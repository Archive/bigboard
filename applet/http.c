/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */

#include "http.h"

static GdkPixbuf*
pixbuf_parse(const char            *data,
             guint                  length,
             GError               **error_p)
{
    GdkPixbufLoader *loader;
    GdkPixbuf *pixbuf;

    loader = gdk_pixbuf_loader_new();

    if (!gdk_pixbuf_loader_write(loader, (guchar*) data, length, error_p))
        goto failed;
    
    if (!gdk_pixbuf_loader_close(loader, error_p))
        goto failed;

    pixbuf = gdk_pixbuf_loader_get_pixbuf(loader);
    if (pixbuf == NULL) {
        g_set_error(error_p,
                    GDK_PIXBUF_ERROR,
                    GDK_PIXBUF_ERROR_FAILED,
                    "Could not load pixbuf");
        goto failed;
    }

    g_object_ref(pixbuf);
    g_object_unref(loader);
    return pixbuf;

  failed:
    g_assert(error_p == NULL || *error_p != NULL);
    
    if (loader)
        g_object_unref(loader);

    return NULL;
}

typedef struct {
    HttpPixbufFunc func;
    void *data;
} GetPixbufClosure;

static void
on_pixbuf_result(SoupSession  *session,
                 SoupMessage  *message,
                 void         *data)
{
    GetPixbufClosure *closure = data;
    GdkPixbuf *new_icon = NULL;
    GError *error;
    
#ifdef VERBOSE_HTTP
    g_debug("Got reply to http GET for pixbuf");
#endif

    if (!SOUP_STATUS_IS_SUCCESSFUL(message->status_code)) {
        g_printerr("Failed to download image: %d\n", message->status_code);
        goto out;
    }
    
    error = NULL;
    new_icon = pixbuf_parse(message->response_body->data, message->response_body->length, &error);
    if (new_icon == NULL) {
        g_printerr("Failed to parse image: %s\n", error->message);
        g_error_free(error);
        goto out;
    }

 out:
    (* closure->func) (new_icon, closure->data);
    
    if (new_icon)
        g_object_unref(new_icon);
}

void
http_get_pixbuf(SoupSession   *session,
                const char     *url,
                HttpPixbufFunc  func,
                void           *data)
{
    SoupMessage *message;
    GetPixbufClosure *closure;

    closure = g_new0(GetPixbufClosure, 1);
    closure->func = func;
    closure->data = data;

    message = soup_message_new("GET", url);
    soup_session_queue_message(session, message, on_pixbuf_result, closure);
}
