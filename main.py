#!/usr/bin/python

import os, sys, getopt, logging, stat, signal

# This line makes jhbuild find the jhbuilt pygtk
import pygtk; pygtk.require ('2.0')
import gobject, gtk, pango, cairo
import gnome.ui, gconf
# We need to import this early before gnome_program_init() is called
import gnomeapplet
import dbus
import dbus.service
import dbus.glib

import hippo

from bigboard.core.panel import Panel

import bigboard
import bigboard.big_widgets
import bigboard.libbig
try:
    import bigboard.bignative as bignative
except:
    import bignative
import bigboard.globals
from bigboard.globals import GCONF_PREFIX, BUS_NAME_STR
import bigboard.google
from bigboard.libbig.gutil import *
import bigboard.libbig.dbusutil
import bigboard.libbig.logutil
import bigboard.libbig.stdout_logger

_logger = logging.getLogger("bigboard.Main")

_logger.debug("starting main")

# TODO: figure out an algorithm for removing pixbufs from the cache
_surfacecache = {}
_emptysurface = None
_mtimecache = {}
def _get_mtime(filename):
    try:
        return os.stat(filename).st_mtime
    except OSError:
        return -1

def load_image_hook(img_name):
    try:
        surface = _surfacecache[img_name]
    except KeyError, e:
        surface = None

    try:
        mtime = _mtimecache[img_name]
        new_mtime = _get_mtime(img_name)
        if mtime != new_mtime:
            surface = None
    except KeyError, e:
        pass
    
    if not surface:
        try:
            if img_name.find(os.sep) >= 0:
                pixbuf = gtk.gdk.pixbuf_new_from_file(img_name)
                _logger.debug("loaded from file '%s': %s", img_name, pixbuf)
                if pixbuf:
                    _mtimecache[img_name] = _get_mtime(img_name)
            else:
                theme = gtk.icon_theme_get_default()
                pixbuf = theme.load_icon(img_name, 60, gtk.ICON_LOOKUP_USE_BUILTIN)
                _logger.debug("loaded from icon theme '%s': %s", img_name,pixbuf)
        except gobject.GError, e:
            _logger.error("Failed to load icon: '%s'", e.message)
            pixbuf = None

        if pixbuf:
            surface = hippo.cairo_surface_from_gdk_pixbuf(pixbuf)
            _surfacecache[img_name] = surface

    if not surface:
        # Returning None will cause a crash, return a 1x1 transparent pixmap
        global _emptysurface
        if not _emptysurface:
            _emptysurface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 1, 1)
            cr = cairo.Context(_emptysurface)
            cr.set_operator(cairo.OPERATOR_CLEAR)
            cr.paint()
        surface = _emptysurface
        # Cache negative result
        _surfacecache[img_name] = surface

    return surface

def on_name_lost(*args):
    name = str(args[0])
    logging.debug("Lost bus name " + name)
    if name == BUS_NAME_STR:
        gtk.main_quit()

def usage():
    print "%s [--debug] [--debug-modules=mod1,mod2...] [--info] [--no-autolaunch] [--stockdirs=dir1:dir2:...] [--help]" % (sys.argv[0])

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hds", ["help", "debug", "na", "no-autolaunch", "info", "replace", "stockdirs=", "debug-modules=", "server=", "dogfood"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    info = False
    debug = False
    replace = False
    stockdirs = []
    debug_modules = []
    for o, v in opts:
        if o in ('-d', '--debug'):
            debug = True
        elif o in ('--dogfood',):
            bigboard.globals.set_server_name('dogfood.mugshot.org:9080')
        elif o in ('--info',):
            info = True
        elif o in ('--replace',):
            replace = True            
        elif o in ('--na', '--no-autolaunch'):
            bigboard.globals.set_do_autolaunch(False)
        elif o in ('--stockdirs',):
            stockdirs = map(os.path.abspath, v.split(':'))
        elif o in ('--debug-modules',):
            debug_modules = v.split(',')
        elif o in ('--server',):
            bigboard.globals.set_server_name(v)
        elif o in ("-h", "--help"):
            usage()
            sys.exit()

    signal.signal(signal.SIGINT, lambda i,frame: sys.stderr.write('Caught SIGINT\n') or os._exit(0))

    if (not os.environ.has_key('OD_SESSION')):
        warn = gconf.client_get_default().get_without_default(GCONF_PREFIX + 'warn_outside_online_desktop')
        if warn == None or warn.get_bool():
            dialog = gtk.MessageDialog(type=gtk.MESSAGE_WARNING, message_format="Online desktop session isn't running")
            dialog.format_secondary_text("You should log into the online desktop session rather than running Big Board directly.")
            dialog.add_buttons("Exit", gtk.RESPONSE_CANCEL, "Continue", gtk.RESPONSE_OK)
            checkbutton = gtk.CheckButton("Don't show this warning again")
            checkbutton.show()
            dialog.vbox.pack_end(checkbutton)
            response = dialog.run()
            if checkbutton.get_active():
                warn = gconf.client_get_default().set_bool(GCONF_PREFIX + 'warn_outside_online_desktop', False)
            if response == gtk.RESPONSE_CANCEL:
                exit(1)

    def logger(domain, priority, msg):
        print msg

    gobject.threads_init()
    dbus.glib.threads_init()    

    gnome.program_init("bigboard", "0.3")
    
    default_log_level = 'ERROR'
    if info:
        default_log_level = 'INFO'
    if debug:
        default_log_level = 'DEBUG'

    bigboard.libbig.logutil.init(default_log_level, debug_modules, '')

    # Redirect sys.stdout to our logging framework
    sys.stdout = bigboard.libbig.stdout_logger.StdoutLogger()
    
    bignative.set_application_name("BigBoard")
    bignative.set_program_name("bigboard")
    bignative.install_focus_docks_hack()
    
    hippo.canvas_set_load_image_hook(load_image_hook)

    bigboard.globals.init()
    
    bus = dbus.SessionBus() 

    if replace:
        try:
            bb = bus.get_object(BUS_NAME_STR, '/bigboard/panel')
            bb.Kill()
        except dbus.DBusException, e:
            pass
        
    bus_name = dbus.service.BusName(BUS_NAME_STR, bus=bus)
    _logger.debug("Requesting D-BUS name")
    try:
        bigboard.libbig.dbusutil.take_name(BUS_NAME_STR, replace, on_name_lost)
    except bigboard.libbig.dbusutil.DBusNameExistsException:
        print "Big Board already running; exiting"
        sys.exit(0)
        
    gconf.client_get_default().add_dir(GCONF_PREFIX[:-1], gconf.CLIENT_PRELOAD_RECURSIVE)

    gtk.rc_parse_string('''
style "bigboard-nopad-button" {
  xthickness = 0
  ythickness = 0
  GtkButton::inner-border = {0,0,0,0}
}
widget "*bigboard-nopad-button" style "bigboard-nopad-button"
''')
    
    _logger.debug("Creating panel")
    panel = Panel(bus_name)

    bigboard.google.init()
        
    _logger.debug("Enter mainloop")
    gtk.main()

    _logger.debug("Exiting BigBoard")
    sys.exit(0)

if __name__ == "__main__":
    main()
