import copy
import gobject
import logging
import re

from ddm import DataModel
from ddm.NotificationSet import NotificationSet
import ddm.Resource
import bigboard.globals
from libbig.singletonmixin import Singleton
import libbig.gutil as gutil

try:
    import bigboard.bignative as bignative
except:
    import bignative

_logger = logging.getLogger("bigboard.PeopleTracker")

def _canonicalize_aim(aim):
    return aim.replace(" ", "").lower()

class Person(gobject.GObject):
    """Wrapper around buddy and user resources

    A Person wraps a buddy resource or user resource and provides convenience APIs for
    things that are shared between the two resource types.
    """
    __gsignals__ = {
        "display-name-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        "icon-url-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        "home-url-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        "aims-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        "aim-buddies-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        "emails-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        "xmpps-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        "xmpp-buddies-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        "local-buddies-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        "online-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        "status-changed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
    }
    
    def __init__(self, resource):
        gobject.GObject.__init__(self)

        self.display_name = None
        self.contact = None
        self.user = None
        self.buddy = None
        self.aims = []
        self.emails = []
        self.xmpps = []
        self.aim_buddies = []
        self.xmpp_buddies = []
        self.local_buddies = []

        self.icon_url = None
        self.home_url = None
        self.online = False
        self.status = STATUS_NOT_A_CONTACT

        self.resource = resource
        
        if resource.class_id == "http://online.gnome.org/p/o/contact":
            self.__set_contact(resource)
        elif resource.class_id == "http://mugshot.org/p/o/user":
            self.__set_user(resource)
        elif resource.class_id == "online-desktop:/p/o/buddy":
            self.__set_buddy(resource)

    def __set_contact(self, contact):
        if self.contact:
            self.contact.disconnect(self.__contact_user_changed)
            self.contact.disconnect(self.__contact_name_changed)
            self.contact.disconnect(self.__contact_status_changed)
            self.contact.disconnect(self.__contact_aims_changed)
            self.contact.disconnect(self.__contact_emails_changed)
            self.contact.disconnect(self.__contact_xmpps_changed)
            self.contact.disconnect(self.__contact_aim_buddies_changed)
            self.contact.disconnect(self.__contact_xmpp_buddies_changed)

        self.contact = contact
        
        if self.contact:
            self.contact.connect(self.__contact_user_changed, "user")
            self.contact.connect(self.__contact_name_changed, "name")
            self.contact.connect(self.__contact_status_changed, "status")
            self.contact.connect(self.__contact_aims_changed, "aims")
            self.contact.connect(self.__contact_emails_changed, "emails")
            self.contact.connect(self.__contact_xmpps_changed, "xmpps")
            self.contact.connect(self.__contact_aim_buddies_changed, "aimBuddies")
            self.contact.connect(self.__contact_xmpp_buddies_changed, "xmppBuddies")
            
        self.__contact_user_changed(contact)
        self.__contact_name_changed(contact)
        self.__contact_status_changed(contact)
        self.__contact_aims_changed(contact)
        self.__contact_emails_changed(contact)
        self.__contact_xmpps_changed(contact)
        self.__contact_aim_buddies_changed(contact)
        self.__contact_xmpp_buddies_changed(contact)

    def __set_user(self, user):
        if self.user:
            self.user.disconnect(self.__user_name_changed)
            self.user.disconnect(self.__user_photo_url_changed)
            self.user.disconnect(self.__user_home_url_changed)
            self.user.disconnect(self.__user_aims_changed)
            self.user.disconnect(self.__user_emails_changed)
            self.user.disconnect(self.__user_xmpps_changed)
            self.user.disconnect(self.__user_aim_buddies_changed)
            self.user.disconnect(self.__user_xmpp_buddies_changed)
            self.user.disconnect(self.__user_local_buddies_changed)

        self.user = user
        
        if self.user:
            self.user.connect(self.__user_name_changed, "name")
            self.user.connect(self.__user_photo_url_changed, "photoUrl")
            self.user.connect(self.__user_home_url_changed, "homeUrl")
            self.user.connect(self.__user_aims_changed, "aims")
            self.user.connect(self.__user_emails_changed, "emails")
            self.user.connect(self.__user_xmpps_changed, "xmpps")
            self.user.connect(self.__user_aim_buddies_changed, "aimBuddies")
            self.user.connect(self.__user_xmpp_buddies_changed, "xmppBuddies")
            self.user.connect(self.__user_local_buddies_changed, "mugshotLocalBuddies")
            
        self.__user_name_changed(user)
        self.__user_aims_changed(user)
        self.__user_emails_changed(user)
        self.__user_xmpps_changed(user)
        self.__user_aim_buddies_changed(user)
        self.__user_xmpp_buddies_changed(user)
        self.__user_photo_url_changed(user)
        self.__user_home_url_changed(user)
        self.__user_local_buddies_changed(user)

    def __set_buddy(self, buddy):
        self.buddy = buddy
        
        self.__update_aim_buddies()
        self.__update_xmpp_buddies()
        self.__update_local_buddies()
        self.__update_aims()
        self.__update_xmpps()
        
    ##############################################################
        
    def __contact_user_changed(self, contact):
        self.__set_user(getattr(contact, 'user', None))
    
    def __contact_name_changed(self, contact):
        self.__update_name()
    
    def __contact_status_changed(self, contact):
        self.__update_status()
    
    def __contact_aims_changed(self, contact):
        self.__update_aims()
    
    def __contact_emails_changed(self, contact):
        self.__update_emails()
    
    def __contact_xmpps_changed(self, contact):
        self.__update_xmpps()
    
    def __contact_aim_buddies_changed(self, contact):
        self.__update_aim_buddies()
    
    def __contact_xmpp_buddies_changed(self, contact):
        self.__update_xmpp_buddies()

    ##############################################################

    def __user_name_changed(self, user):
        self.__update_name()
    
    def __user_photo_url_changed(self, user):
        self.__update_icon_url()
    
    def __user_home_url_changed(self, user):
        self.__update_home_url()
    
    def __user_aims_changed(self, user):
        self.__update_aims()
    
    def __user_emails_changed(self, user):
        self.__update_emails()
    
    def __user_xmpps_changed(self, user):
        self.__update_xmpps()
    
    def __user_aim_buddies_changed(self, user):
        self.__update_aim_buddies()
    
    def __user_xmpp_buddies_changed(self, user):
        self.__update_xmpp_buddies()
    
    def __user_local_buddies_changed(self, user):
        self.__update_local_buddies()

    ##############################################################

    def __buddy_alias_changed(self, buddy):
        self.__update_name()
        
    def __buddy_icon_changed(self, buddy):
        self.__update_icon_url()
        
    def __buddy_is_online_changed(self, buddy):
        self.__update_online()
        
    ##############################################################

    def __update_name(self):
        name = None
        # We prefer the name in the user to the name in the contact, because
        # a lot of our contacts in the database have bad (email-derived)
        # names. If we add the ability to override the name chosen by
        # a Mugshot user, then we'll need to remove those bad contact
        # names, and then reverse this.
        if not name and self.user:
            name = getattr(self.user, 'name', None)
        if not name and self.contact:
            name = getattr(self.contact, 'name', None)
        if not name:
            for buddy in self.aim_buddies:
                name = getattr(buddy, 'alias', None)
                if name:
                    break
        if not name:
            for buddy in self.xmpp_buddies:
                name = getattr(buddy, 'alias', None)
                if name:
                    break
        if not name:
            for buddy in self.local_buddies:
                name = getattr(buddy, 'alias', None)
                if name:
                    break
        if not name:
            for buddy in self.aim_buddies:
                name = getattr(buddy, 'name', None)
                if name:
                    break
        if not name:
            for buddy in self.xmpp_buddies:
                name = getattr(buddy, 'name', None)
                if name:
                    break
        if not name:
            for buddy in self.local_buddies:
                name = getattr(buddy, 'name', None)
                if name:
                    break
        if not name and len(self.emails) > 0:
            name = self.emails[0]

        if name == None:
            name = "NO_NAME"

        if self.display_name != name:
            self.display_name = name
            self.emit('display-name-changed')

    def __update_icon_url(self):
        icon_url = None
        if self.user:
            icon_url = getattr(self.user, 'photoUrl', None)
            
        if not icon_url:
            for buddy in self.aim_buddies:
                icon_url = getattr(buddy, 'icon', None)
                if icon_url:
                    break
        if not icon_url:
            for buddy in self.xmpp_buddies:
                icon_url = getattr(buddy, 'icon', None)
                if icon_url:
                    break
        if not icon_url:
            for buddy in self.local_buddies:
                icon_url = getattr(buddy, 'contact', None)
                if icon_url:
                    break
    
        if self.icon_url != icon_url:
            self.icon_url = icon_url
            self.emit('icon-url-changed')

    def __update_home_url(self):
        home_url = None
        if self.user:
            home_url = getattr(self.user, 'homeUrl', None)
            
        if self.home_url != home_url:
            self.home_url = home_url
            self.emit('home-url-changed')

    def __update_aims(self):
        aims = set()
        if self.contact:
            aims.update(getattr(self.contact, 'aims', []))
        if self.user:
            aims.update(getattr(self.user, 'aims', []))
        if self.buddy and self.buddy.protocol == 'aim':
            aims.add(self.buddy.name)

        aims = list(aims)
        aims.sort()
        
        if self.aims != aims:
            self.aims = aims
            self.emit('aims-changed')
            
    def __update_emails(self):
        emails = set()
        if self.contact:
            emails.update(getattr(self.contact, 'emails', []))
        if self.user:
            emails.update(getattr(self.user, 'emails', []))

        emails = list(emails)
        emails.sort()
        
        if self.emails != emails:
            self.emails = emails
            self.emit('emails-changed')
            
        self.__update_name()
        
    def __update_xmpps(self):
        xmpps = set()
        if self.contact:
            xmpps.update(getattr(self.contact, 'xmpps', []))
        if self.user:
            xmpps.update(getattr(self.user, 'xmpps', []))
        if self.buddy and self.buddy.protocol == 'xmpp':
            xmpps.add(self.buddy.name)

        xmpps = list(xmpps)
        xmpps.sort()
    
        if self.xmpps != xmpps:
            self.xmpps = xmpps
            self.emit('xmpps-changed')

    def __update_buddy_connections(self, old_buddies, new_buddies):
        for buddy in old_buddies:
            if not buddy in new_buddies:
                buddy.disconnect(self.__buddy_alias_changed)
                buddy.disconnect(self.__buddy_icon_changed)
                buddy.disconnect(self.__buddy_is_online_changed)
                
        for buddy in new_buddies:
            if not buddy in old_buddies:
                buddy.connect(self.__buddy_alias_changed, 'alias')
                buddy.connect(self.__buddy_icon_changed, 'icon')
                buddy.connect(self.__buddy_is_online_changed, 'isOnline')
            
    def __update_aim_buddies(self):
        aim_buddies = set()
        if self.contact:
            aim_buddies.update(getattr(self.contact, 'aimBuddies', []))
        if self.user:
            aim_buddies.update(getattr(self.user, 'aimBuddies', []))
        if self.buddy and self.buddy.protocol == 'aim':
            aim_buddies.add(self.buddy)

        aim_buddies = list(aim_buddies)
        aim_buddies.sort()
    
        if self.aim_buddies != aim_buddies:
            self.__update_buddy_connections(self.aim_buddies, aim_buddies)
            self.aim_buddies = aim_buddies
            self.emit('aim-buddies-changed')

        self.__update_name()
        self.__update_icon_url()
        self.__update_online()
            
    def __update_xmpp_buddies(self):
        xmpp_buddies = set()
        if self.contact:
            xmpp_buddies.update(getattr(self.contact, 'xmppBuddies', []))
        if self.user:
            xmpp_buddies.update(getattr(self.user, 'xmppBuddies', []))
        if self.buddy and self.buddy.protocol == 'xmpp':
            xmpp_buddies.add(self.buddy)

        xmpp_buddies = list(xmpp_buddies)
        xmpp_buddies.sort()
    
        if self.xmpp_buddies != xmpp_buddies:
            self.__update_buddy_connections(self.xmpp_buddies, xmpp_buddies)
            self.xmpp_buddies = xmpp_buddies
            self.emit('xmpp-buddies-changed')
            
        self.__update_name()
        self.__update_icon_url()
        self.__update_online()
            
    def __update_local_buddies(self):
        local_buddies = set()
        if self.contact:
            local_buddies.update(getattr(self.contact, 'mugshotLocalBuddies', []))
        if self.user:
            local_buddies.update(getattr(self.user, 'mugshotLocalBuddies', []))
        if self.buddy and self.buddy.protocol == 'mugshot-local':
            local_buddies.add(self.buddy)

        local_buddies = list(local_buddies)
        local_buddies.sort()

        if self.local_buddies != local_buddies:
            self.__update_buddy_connections(self.local_buddies, local_buddies)
            self.local_buddies = local_buddies
            self.emit('local-buddies-changed')
            
        self.__update_name()
        self.__update_icon_url()
        self.__update_online()
            
    def __update_online(self):
        online = False
        if not online:
            for buddy in self.aim_buddies:
                online = buddy.isOnline
                if online:
                    break
        if not online:
            for buddy in self.xmpp_buddies:
                online = buddy.isOnline
                if online:
                    break
        if not online:
            for buddy in self.local_buddies:
                online = buddy.isOnline
                if online:
                    break
    
        if self.online != online:
            self.online = online
            self.emit('online-changed')

    def __update_status(self):
        status = STATUS_NOT_A_CONTACT
        if self.contact:
            status = self.contact.status
            
        if self.status != status:
            self.status = status
            self.emit('status-changed')

    def __hash__(self):
        return hash(self.resource)

    def __eq__(self, other):
        if isinstance(other, Person):
            return self.resource == other.resource
        else:
            return self.resource == other

class PersonSet(gobject.GObject):
    """A list of Person objects with change notification

    The PersonSet object represents an (unordered) set of person objects with change notification
    when person objects are added and removed from it via the 'added' and 'removed' GObject signals.
    The object also supports the standard Python iteration protocol.

    """
    __gsignals__ = {
        "added": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        "removed": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
    }

## PersonSet that wraps a list of Resource objects (each resource is wrapped in a Person object)
class SinglePersonSet(PersonSet):
    def __init__(self):
        PersonSet.__init__(self)

        self.__resources = set()
        self.__resolved = set()

        # This is the set of users we need to watch the 'contact' property on
        self.__watched_users = set()

    def _update(self, resources):
        for resource in self.__resources:
            if resource.class_id == "online-desktop:/p/o/buddy" and resource not in resources:
                resource.disconnect(self.__buddy_user_changed)
                resource.disconnect(self.__buddy_contact_changed)

        for resource in resources:
            if resource.class_id == "online-desktop:/p/o/buddy" and resource not in self.__resources:
                resource.connect(self.__buddy_user_changed, "user")
                resource.connect(self.__buddy_contact_changed, "contact")

        self.__resources = resources
        self.__update_resolved()

    def __update_resolved(self):
        resolved = set()
        watched_users = set()

        for resource in self.__resources:
            if resource.class_id == "online-desktop:/p/o/buddy":
                user = getattr(resource, 'user', None)
                contact = getattr(resource, 'contact', None)

                if user != None:
                    watched_users.add(user)
                    if contact == None:
                        contact = getattr(user, 'contact', None)
                    
                if contact != None:
                    resource = contact
                elif user != None:
                    resource = user

            try:
                person = resource.__person
            except AttributeError:
                person = resource.__person = Person(resource)

            resolved.add(person)

        old_watched_users = self.__watched_users
        self.__watched_users = watched_users
        
        for user in old_watched_users:
            if not user in self.__watched_users:
                user.disconnect(self.__user_contact_changed)
                
        for user in self.__watched_users:
            if not user in old_watched_users:
                user.connect(self.__user_contact_changed, 'contact')

        old_resolved = self.__resolved
        self.__resolved = resolved
        
        for person in old_resolved:
            if person not in self.__resolved:
                self.emit('removed', person)
                
        for person in self.__resolved:
            if person not in old_resolved:
                self.emit('added', person)

    def __buddy_user_changed(self, resource):
        self.__update_resolved()

    def __buddy_contact_changed(self, resource):
        self.__update_resolved()

    def __user_contact_changed(self, resource):
        self.__update_resolved()

    def __str__(self):
        return self.__resolved.__str__()

    def __iter__(self):
        return self.__resolved.__iter__()

## used to merge multiple PersonSet into one
class UnionPersonSet(PersonSet):
    def __init__(self, *args):
        PersonSet.__init__(self)
        self.__items = {}
        for s in args:
            s.connect('added', self.__on_added)
            s.connect('removed', self.__on_removed)

            for item in s:
                self.__on_added(self, s, item)

    def __on_added(self, s, item):
        if item in self.__items:
            self.__items[item] += 1
        else:
            self.__items[item] = 1
            self.emit('added', item)

    def __on_removed(self, s, item):
        self.__items[item] -= 1
        if self.__items[item] == 0:
            del self.__items[item]
            self.emit('removed', item)

    def __str__(self):
        return self.__items.values().__str__()

    def __iter__(self):
        return self.__items.iterkeys()

class PeopleTracker(Singleton):
    """Singleton object for tracking available users and contacts

    The PeopleTracker is responsible for doing bookkeeping associated with figuring
    out who is around.

    Lists of different types of users are available as the contacts, aim_users and local_users
    attributes of the singleton; these are UserList objects, and have signals for tracking
    changes as well as allowing iteration through the users.
    """
    
    def __init__(self):
        self.__model = bigboard.globals.get_data_model()
        self.__model.add_ready_handler(self.__on_ready)

        self.contacts = SinglePersonSet()
        self.aim_people = SinglePersonSet()
        self.xmpp_people = SinglePersonSet()
        self.local_people = SinglePersonSet()
        
        self.people = UnionPersonSet(self.contacts, self.aim_people, self.xmpp_people, self.local_people)

        if self.__model.ready:
            self.__on_ready()
        
    def __on_ready(self):

        # When we disconnect from the server we freeze existing content, then on reconnect
        # we clear everything and start over.

        contact_props = '[+;name;user [+;aims;aimBuddies [+;icon;statusMessage];xmpps;xmppBuddies [+;icon;statusMessage];mugshotLocalBuddies [+;icon;statusMessage];emails];aims;aimBuddies [+;icon;statusMessage];mugshotLocalBuddies [+;icon];xmpps;xmppBuddies [+;icon;statusMessage];emails;status]'
        
        if self.__model.self_resource != None:
            query = self.__model.query_resource(self.__model.self_resource, "contacts %s" % contact_props)
            query.add_handler(self.__on_got_self)
            query.execute()

        # Since we've previously retrieved all our contacts with a bunch of properties, we can just use 'contact +' properties
        query = self.__model.query_resource(self.__model.global_resource,
                                            "aimBuddies [+;icon;statusMessage;contact +;user [+;contact]]; xmppBuddies [+;icon;statusMessage;contact +;user [+;contact]]; mugshotLocalBuddies [+;icon;user [+;contact]]")

        query.add_handler(self.__on_got_global)
        query.execute()

    def __ensure_list_property(self, resource, property):
        # Workaround for lack of schema support currently; if the property doesn't
        # exist, make it empty
        try:
            resource.get(property)
        except AttributeError:
            resource._update_property(property,
                                      ddm.Resource.UPDATE_CLEAR, ddm.Resource.CARDINALITY_N,
                                      None)
        
    def __on_got_self(self, self_resource):
        self.__ensure_list_property(self_resource, ("http://mugshot.org/p/o/user", "contacts"))
        self_resource.connect(self.__on_contacts_changed, "contacts")
        self.__on_contacts_changed(self_resource)

    def __on_contacts_changed(self, self_resource):
        new_contacts = set()
        for contact in self_resource.contacts:
            new_contacts.add(contact)

        self.contacts._update(new_contacts)
        
    def __on_got_global(self, global_resource):
        self.__ensure_list_property(global_resource, ("online-desktop:/p/o/global", "aimBuddies"))
        self.__ensure_list_property(global_resource, ("online-desktop:/p/o/global", "xmppBuddies"))
        self.__ensure_list_property(global_resource, ("online-desktop:/p/o/global", "mugshotLocalBuddies"))
        global_resource.connect(self.__on_aim_buddies_changed, "aimBuddies")
        global_resource.connect(self.__on_local_buddies_changed, "mugshotLocalBuddies")
        global_resource.connect(self.__on_xmpp_buddies_changed, "xmppBuddies")
        
        self.__on_aim_buddies_changed(global_resource)
        self.__on_local_buddies_changed(global_resource)
        self.__on_xmpp_buddies_changed(global_resource)

    def __on_aim_buddies_changed(self, global_resource):
        self.aim_people._update(global_resource.aimBuddies)

    def __on_local_buddies_changed(self, global_resource):
        self.local_people._update(global_resource.mugshotLocalBuddies)

    def __on_xmpp_buddies_changed(self, global_resource):
        self.xmpp_people._update(global_resource.xmppBuddies)

## status on a contact is:
## 0 - not a contact
## 1 - blocked
## 2 - cold (never in sidebar)
## 3 - medium (auto-choose sidebar)
## 4 - hot (always in sidebar)

STATUS_NOT_A_CONTACT = 0
STATUS_BLOCKED = 1
STATUS_COLD = 2
STATUS_MEDIUM = 3
STATUS_HOT = 4

## for sorting, we classify with the following 
## ranks (higher is higher in the sidebar).
## The ranks have a gap since not being a user counts 
## as -1 for contacts
RANK_NO_DISPLAY = -2
RANK_COLD = 0
RANK_BUDDY_OFFLINE = 2
RANK_MEDIUM_OFFLINE = 4
RANK_BUDDY_ONLINE = 6
RANK_MEDIUM_ONLINE = 8
RANK_HOT_OFFLINE = 10
RANK_HOT_ONLINE = 12

def __get_raw_contact_rank(contact):
    if contact.status == STATUS_NOT_A_CONTACT:
        return RANK_COLD
    elif contact.status == STATUS_BLOCKED:
        return RANK_NO_DISPLAY

    if contact.online:
        if contact.status == STATUS_MEDIUM:
            return RANK_MEDIUM_ONLINE
        elif contact.status == STATUS_HOT:
            return RANK_HOT_ONLINE
    else:
        if contact.status == STATUS_MEDIUM:
            return RANK_MEDIUM_OFFLINE
        elif contact.status == STATUS_HOT:
            return RANK_HOT_OFFLINE

    ## I believe this is not reached
    return RANK_COLD

def __get_contact_rank(contact):
    rank = __get_raw_contact_rank(contact)
    ## subtract 1 if not a user
    if not contact.user:
        rank -= 1

    return rank
        
def __get_buddy_rank(buddy):
    if buddy.online:
        return RANK_BUDDY_ONLINE
    else:
        return RANK_BUDDY_OFFLINE

# import libbig.gutil
# rank_changed_list = []
# def rank_changed_idle():
#     global rank_changed_list
#     for person in rank_changed_list:
#         person.emit("display-name-changed")
#     rank_changed_list = []

# def debug_change_rank(person, rank):
#     global rank_changed_list
#     if rank != person._debug_rank:
#         person._debug_rank = rank
#         rank_changed_list.append(person)
#         libbig.gutil.call_idle_once(rank_changed_idle)

def sort_people(a, b):

    rankA = 0
    rankB = 0

    if a.contact:
        rankA = __get_contact_rank(a)
    else:
        rankA = __get_buddy_rank(a)

    if b.contact:
        rankB = __get_contact_rank(b)
    else:
        rankB = __get_buddy_rank(b)

    #    debug_change_rank(a, rankA)
    #    debug_change_rank(b, rankB)

    if rankA != rankB:
        return rankB - rankA

    return bignative.utf8_collate(a.display_name, b.display_name)

