import logging, dbus, gtk
import hippo
import bigboard.google as google
import bigboard.libbig.gutil as gutil
from bigboard.libbig.logutil import log_except

_logger = logging.getLogger("bigboard.Google")

## this base class is messed up, because the polling and account data
## tracking should be in a model object, not in the view (stock).
## Some stuff in here is view-specific though, like the _create_login_button
## method.

FAILED_TO_LOGIN_STRING = "Failed to login."
FAILED_TO_CONNECT_STRING = "Cannot connect to Google."
LOGIN_TO_GOOGLE_STRING = "Login to Google"
EDIT_GOOGLE_ACCOUNTS_STRING = "Edit Google Accounts"
CHECKING_LOGIN_STRING = "Checking Login..."

class GoogleStock(object):
    def __init__(self, action_id, **kwargs):
        super(GoogleStock, self).__init__(**kwargs)
        _logger.debug("in google stock init")
        # a set of enabled google accounts to be used in the stock
        self.googles = set()
        self.__googles_by_account = {} ## maps OnlineAccount ObjectPath => google.Google

        self.__onlineaccounts_proxy = None

        _logger.debug("in google stock init action_id is %s", str(action_id))         
        self.__action_id = action_id

        self.__connections = gutil.DisconnectSet()

        self._login_button = hippo.CanvasButton(text=LOGIN_TO_GOOGLE_STRING)
        self._login_button.connect('activated', lambda button: self.__open_login_dialog())
        _logger.debug("done with google stock init")     

    def _post_init(self):
        try:
            self.__onlineaccounts_proxy = dbus.SessionBus().get_object('org.gnome.OnlineAccounts', '/onlineaccounts')
        except dbus.DBusException, e:
            _logger.error("onlineaccounts DBus service not available, can't get google accounts")
            return

        try: 
            self.__onlineaccounts_proxy.EnsureAccountType("google", "Google", "Gmail or Google Apps for Your Domain email", "http://gmail.com")
        except dbus.DBusException, e:
            _logger.error(e.message)
            return

        all_google_account_paths = self.__onlineaccounts_proxy.GetEnabledAccountsWithTypes(["google"])
        for a_path in all_google_account_paths:
            self.__on_account_added(a_path)

        id = self.__onlineaccounts_proxy.connect_to_signal('AccountEnabled', self.__on_account_added)
        self.__connections.add(self.__onlineaccounts_proxy, id)
        id = self.__onlineaccounts_proxy.connect_to_signal('AccountDisabled', self.__on_account_removed)
        self.__connections.add(self.__onlineaccounts_proxy, id)

    ## we can't just override _on_delisted() because of multiple inheritance,
    ## so our subclasses have to override it then call this
    def _delist_google(self):
        self.__connections.disconnect_all()

        ## detach from all the accounts
        acct_paths = self.__googles_by_account.keys()
        for a_path in acct_paths:
            self.__on_account_removed(a_path)

    @log_except(_logger)
    def __on_account_added(self, acct_path):
        try:
            _logger.debug("acct_path is %s" % acct_path)
            acct = dbus.SessionBus().get_object('org.gnome.OnlineAccounts', acct_path)
        except dbus.DBusException, e:
            _logger.error("onlineaccount for path %s was not found" % acct_path)
            return

        if acct.GetType() != "google":
            return
       
        _logger.debug("in __on_account_added for %s", str(acct))         
        gobj = google.get_google_for_account(acct)
        gobj.add_poll_action_func(self.__action_id, lambda gobj: self.update_google_data(gobj))
        self.googles.add(gobj)
        self.__googles_by_account[acct_path] = gobj
        gobj.connect("checking-auth", self._checking_google_auth)
        ## update_google_data() should be called in the poll action
    
    def __on_account_removed(self, acct_path):
        try:
            acct = dbus.SessionBus().get_object('org.gnome.OnlineAccounts', acct_path)
        except dbus.DBusException, e:
            _logger.error("onlineaccount for path %s was not found" % acct_path)
            return

        if acct.GetType() != "google":
            return

        _logger.debug("in __on_account_removed for %s", str(acct))  
        ## we keep our own __googles_by_account because google.get_google_for_account()
        ## will have dropped the Google before this point
        gobj = self.__googles_by_account[acct_path]
        gobj.remove_poll_action(self.__action_id)
        self.googles.remove(gobj)
        del self.__googles_by_account[acct_path]

        ## hook for derived classes
        self.remove_google_data(gobj)

    def have_one_good_google(self):
        for g in self.googles:
            if not g.get_current_auth_credentials_known_bad():
                return True

        return False

    def __open_login_dialog(self):
        if self.__onlineaccounts_proxy:
            self.__onlineaccounts_proxy.OpenAccountsDialogWithTypes(["google"], gtk.get_current_event_time())
        else: 
            _logger.error("onlineaccounts proxy is not initialized, can't open the dialog for google accounts")    

    def _checking_google_auth(self, gobj):
        self._login_button.set_property("text", CHECKING_LOGIN_STRING)

    def update_google_data(self, gobj=None):
        pass

    def remove_google_data(self, gobj):
        pass
    
