import logging
import math

import cairo
import gobject
import gtk
import hippo

from bigboard.big_widgets import Arrow, Header
from bigboard.expand_box import ExpandBox
from bigboard.stock import Stock
from bigboard.theme_manager import ThemeManager

_logger = logging.getLogger("bigboard.StockHolder")

class GoogleGadgetContainer(hippo.CanvasWidget):
    def __init__(self, metainfo, env):
        super(GoogleGadgetContainer, self).__init__()
        from pyonlinedesktop import ggadget        
        self.widget = ggadget.Gadget(metainfo, env)
        self.widget.show_all() 
        self.set_property('widget', self.widget)

class Separator(hippo.CanvasBox):
    def __init__(self):
        hippo.CanvasBox.__init__(self, border_top=1, border_color=0x999999FF)
        
class RotatedText(hippo.CanvasBox):
    text = gobject.property(type=str)

    def __create_layout(self, cr=None):
        layout =  self.create_layout()
        layout.set_text(self.text)
        layout.set_font_description(self.get_style().get_font())
        if not cr:
            surface = self.create_surface(cairo.CONTENT_COLOR_ALPHA, 1, 1)
            cr = gtk.gdk.CairoContext(cairo.Context(surface))
            cr.rotate(- math.pi / 2)

        cr.update_layout(layout)
        
        return layout

    def do_paint_below_children(self, cr, rect):
        cr2 = gtk.gdk.CairoContext(cr)
        layout = self.__create_layout(cr2)
        style = self.get_style()
        hippo.cairo_set_source_rgba32(cr2, style.get_foreground_color())

        width, height = layout.get_pixel_size()
        cr2.translate(0, width)
        cr2.rotate(- math.pi / 2)
        
        cr2.move_to(0, 0)
        cr2.show_layout(layout)

    def do_get_content_width_request(self):
        width, height = self.__create_layout().get_pixel_size()
        return height, height
    
    def do_get_content_height_request(self, width):
        width, height = self.__create_layout().get_pixel_size()
        return width, width

class StackLayout(gobject.GObject,hippo.CanvasLayout):
    def do_set_box(self, box):
        self.__box = box
        
    def do_get_width_request(self):
        max_item_min_width = 0
        max_item_natural_width = 0

        if self.__box.active:
            active_child = self.__box.find_box_child(self.__box.active)

        for child in self.__box.get_layout_children():
            (min_width, natural_width) = child.get_width_request()
            if child == active_child:
                max_item_min_width = max(max_item_min_width, min_width)
                max_item_natural_width = max(max_item_natural_width, natural_width)

        return max_item_min_width, max_item_natural_width

    def do_get_height_request(self, width):
        max_item_min_height = 0
        max_item_natural_height = 0

        if self.__box.active:
            active_child = self.__box.find_box_child(self.__box.active)

        for child in self.__box.get_layout_children():
            if child == active_child:
                (min_height, natural_height) = child.get_height_request(width)
            else:
                if not self.__box.use_max_height:
                    continue
                (_, natural_width) = child.get_width_request()
                (min_height, natural_height) = child.get_height_request(natural_width)
            
            max_item_min_height = max(max_item_min_height, min_height)
            max_item_natural_height = max(max_item_natural_height, natural_height)

        return max_item_min_height, max_item_natural_height

    def do_allocate(self, x, y, width, height, requested_width, requested_height, origin_changed):
        if self.__box.active:
            active_child = self.__box.find_box_child(self.__box.active)

        for child in self.__box.get_layout_children():
            if child == active_child:
                child.allocate(x, y, width, height, origin_changed)
            else:
                child.allocate(0, 0, 0, 0, origin_changed)
    
gobject.type_register(StackLayout)

class Stack(hippo.CanvasBox, hippo.CanvasItem):
    __gtype_name__ = 'Stack'
    
    def __init__(self, *args, **kwargs):
        hippo.CanvasBox.__init__(self, *args, **kwargs)
        self.set_layout(StackLayout())
        self.active = None
        self.use_max_height = True

    def add(self, child):
        self.append(child, hippo.PACK_EXPAND)
        if not self.active:
            self.set_active(child)

    def set_active(self, child):
        self.active = child
        self.emit_request_changed()
        self.emit_paint_needed(0, 0, -1, -1)        

    def set_use_max_height(self, use_max_height):
        self.use_max_height = use_max_height
        self.emit_request_changed()
        self.emit_paint_needed(0, 0, -1, -1)       

class StockHolder(ExpandBox):
    """A renderer for stocks."""
    
    def __init__(self, metainfo, env, pymodule=None, is_notitle=False, panel=None, stylesheet=None, hidden=False):
        super(StockHolder, self).__init__(orientation=hippo.ORIENTATION_HORIZONTAL,
                                          classes='stock')
        
        self.hidden = hidden
        self.__mouseover = False
        self.__appears_hidden = False
        
        self.__metainfo = metainfo
        self.__env = env
        self.__pymodule = pymodule
        self.__panel = panel
        self.__stylesheet = stylesheet
        self.__content = None
        self.__ticker_text = None
        self.__ticker_container = None

        self.__stack = Stack()
        self.__stack.set_use_max_height(False)
        self.append(self.__stack)
        self.__stack.connect('motion-notify-event', self.__on_stack_motion_notify)
        self.__visible_contents = hippo.CanvasBox(orientation=hippo.ORIENTATION_VERTICAL,
                                                  box_width=Stock.SIZE_BULL_CONTENT_PX)
        
        self.__stack.add(self.__visible_contents)
        
        self.__expanded = True
        if not is_notitle:
            self.__ticker_container = hippo.CanvasBox(orientation=hippo.ORIENTATION_HORIZONTAL)
            
            header_left = hippo.CanvasBox(orientation=hippo.ORIENTATION_HORIZONTAL, classes='stock-header-left')
            self.__ticker_container.append(header_left, hippo.PACK_EXPAND)
            
            self.__ticker_text = hippo.CanvasText(text=metainfo.title, xalign=hippo.ALIGNMENT_START)
            self.__ticker_text.set_clickable(True)
            self.__ticker_text.connect("activated", lambda text: self.__toggle_expanded())  
            header_left.append(self.__ticker_text, hippo.PACK_EXPAND)

            self.__ticker_arrow = Arrow(Arrow.LEFT, padding=4)
            self.__ticker_arrow.connect("activated", lambda text: self.__toggle_expanded())
            header_left.append(self.__ticker_arrow)
            
            if pymodule and pymodule.has_more_button():
                more_button = hippo.CanvasBox(classes='stock-header-right')
                more_button.set_clickable(True)
                more_button_label = hippo.CanvasText()
                more_button.append(more_button_label)
                
                def update_more_button_text(pymodule, *args):
                    more_button_label.props.text = pymodule.more_button_text

                pymodule.connect('notify::more-button-text', update_more_button_text)
                update_more_button_text(pymodule)
                
                more_button.connect("activated", lambda l: pymodule.on_more_clicked())
                self.__ticker_container.append(more_button)

            self.__visible_contents.append(self.__ticker_container)

        self.__theme_mgr = ThemeManager.getInstance()
        self.__theme_mgr.connect('theme-changed', self.__sync_theme)

        self.__stockbox = hippo.CanvasBox(classes='stock-main')
        self.__visible_contents.append(self.__stockbox)
        if pymodule:
            pymodule.connect('visible', self.__render_pymodule)
            self.__render_pymodule()
        else:
            self.__render_google_gadget()

        self.__bottom = hippo.CanvasBox(classes='stock-bottom', orientation=hippo.ORIENTATION_HORIZONTAL, xalign=hippo.ALIGNMENT_CENTER)
        self.__visible_contents.append(self.__bottom)
            
        grippy = hippo.CanvasBox(classes='stock-grippy', box_width=20, box_height=8)
        self.__bottom.append(grippy)

        self.__hidden_title = hippo.CanvasBox(classes='stock-hidden')
        self.__stack.add(self.__hidden_title)

        hidden_text = RotatedText(text=metainfo.title, xalign=hippo.ALIGNMENT_START)
        self.__hidden_title.append(hidden_text)

        self.__update_appears_hidden()
        self.__sync_visibility()

    def __on_stack_motion_notify(self, item, event):
        if event.detail == hippo.MOTION_DETAIL_ENTER:
            self.__mouseover = True
            self.__update_appears_hidden()
        elif event.detail == hippo.MOTION_DETAIL_LEAVE:
            self.__mouseover = False
            self.__update_appears_hidden()

    def __sync_theme(self, *args):
        if self.__content:
            if self.__stylesheet:
                theme = self.__theme_mgr.make_stock_theme(self.__stylesheet)
                self.__content.set_theme(theme)

    def on_delisted(self):
        _logger.debug("on_delisted")
        self.__unrender_pymodule()

    def on_popped_out_changed(self, popped_out):
        self.__pymodule.on_popped_out_changed(popped_out)

    def get_hidden_width(self):
        box_child = self.__hidden_title.get_parent().find_box_child(self.__hidden_title)
        minimum, natural =  box_child.get_width_request()
        
        return natural

    def get_visible_width(self):
        box_child = self.__visible_contents.get_parent().find_box_child(self.__visible_contents)
        minimum, natural =  box_child.get_width_request()
        
        return natural

    def get_current_size(self):
        if self.__appears_hidden:
            return self.__hidden_title.get_allocation()
        else:
            width, height = self.__visible_contents.get_allocation()
            if not self.__expanded:
                # Bit of a hack ... the box __visible_contents is allocated full-height
                # but it's not filled up
                _, h1 = self.__ticker_container.get_allocation()
                _, h2 = self.__bottom.get_allocation()
                height = h1 + h2
            return width, height
    
    def __update_appears_hidden(self):
        appears_hidden = self.hidden and not self.__mouseover and (not self.__pymodule or
                                                                   not self.__pymodule.active)
        if appears_hidden == self.__appears_hidden:
            return

        self.__appears_hidden = appears_hidden

        if self.get_context():
            self.animate(0.3333)

        if self.__appears_hidden:
            self.__stack.set_active(self.__hidden_title)
        else:
            self.__stack.set_active(self.__visible_contents)

    def set_hidden(self, hidden):
        self.hidden = hidden
        self.__update_appears_hidden()
        self.__stack.set_use_max_height(hidden)

    def __sync_visibility(self):
        if self.get_context():
            self.animate(0.3333)
        self.__stockbox.set_visible(self.__expanded)
        if self.__ticker_container:
            self.__ticker_arrow.set_visible(not self.__expanded)

    def __toggle_expanded(self):
        self.__expanded = not self.__expanded
        self.__sync_visibility()
    
    def get_metainfo(self):
        return self.__metainfo
    
    def get_pymodule(self):
        return self.__pymodule
    
    def __render_google_gadget(self):
        rendered = GoogleGadgetContainer(self.__metainfo, self.__env)
        self.__stockbox.append(rendered)

    def __on_stock_active_changed(self, pymodule):
        self.__update_appears_hidden()
        
    def __render_pymodule(self, *args):
        self.__stockbox.remove_all()
        self.__pymodule.set_size(Stock.SIZE_BULL)

        self.__active_changed_handler = self.__pymodule.connect('active-changed', self.__on_stock_active_changed)

        self.__content = self.__pymodule.get_content(Stock.SIZE_BULL) 
        self.__sync_theme()

        if self.__ticker_container:
            self.__ticker_container.set_visible(not not self.__content)
        self.__stockbox.set_visible(not not self.__content)
        if not self.__content:
            _logger.debug("no content for stock %s", self.__pymodule)
            return
        self.__stockbox.append(self.__content)
        padding = 4
        self.__stockbox.set_property("padding_left", padding)
        self.__stockbox.set_property("padding_right", padding)

    def __unrender_pymodule(self):
        if not self.__pymodule:
            _logger.debug("No pymodule")
            return
        
        self.__pymodule.disconnect(self.__active_changed_handler)

        _logger.debug("delisting pymodule %s", self.__pymodule)
        self.__pymodule.on_delisted()
        self.__pymodule = None

