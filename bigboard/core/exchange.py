import logging
import os
import sys
import urllib2
import urlparse

import gconf
import gobject
import pyonlinedesktop
import pyonlinedesktop.widget

import bigboard
from bigboard.globals import GCONF_PREFIX
from bigboard.libbig.gutil import *

from stock_holder import StockHolder

_logger = logging.getLogger("bigboard.Exchange")

class Exchange(gobject.GObject):
    __gsignals__ = {
        "listings-changed" : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, []),
    }    
    def __init__(self, hardcoded_urls):
        super(Exchange, self).__init__()
        self.__stockdir = os.path.join(os.path.dirname(bigboard.__file__), 'stocks')
        self.__widget_environ = widget_environ = pyonlinedesktop.widget.WidgetEnvironment()
        widget_environ['google_apps_auth_path'] = ''
        self.__listing_key = GCONF_PREFIX + 'url_listings'
        gconf.client_get_default().notify_add(self.__listing_key, self.__on_listings_change)
        self.__metainfo_cache = {}   
        self.__hardcoded_urls = hardcoded_urls                        

    def set_listed(self, url, dolist):
        curlist = list(self.get_listed_urls())
        if (url in curlist) and dolist:
            _logger.debug("attempting to list currently listed stock %s", url)
            return
        elif (url not in curlist) and (not dolist):
            _logger.debug("attempting to delist currently unlisted stock %s", url)            
            return
        elif dolist:
            _logger.debug("listing %s", url)              
            curlist.append(url)
        elif not dolist:
            _logger.debug("delisting %s", url)               
            curlist.remove(url)
        gconf.client_get_default().set_list(self.__listing_key, gconf.VALUE_STRING, curlist)                        

    def move_listing(self, url, isup):
        curlist = list(self.get_listed_urls())
        curlen = len(curlist)
        pos = curlist.index(url)
        if pos < 0:
            _logger.debug("couldn't find url in listings: %s", url)
            return
        if isup and pos == 0:
            return
        elif (not isup) and pos == (curlen-1):
            return
        del curlist[pos]        
        pos += (isup and -1 or 1)
        curlist.insert(pos, url)
        gconf.client_get_default().set_list(self.__listing_key, gconf.VALUE_STRING, curlist)        

    def get_all_builtin_urls(self):
        for fname in os.listdir(self.__stockdir):
            fpath = os.path.join(self.__stockdir, fname)
            if fpath.endswith('.xml'):
                url = 'builtin://' + fname
                if url not in self.__hardcoded_urls:
                    yield url
            
    def get_hardcoded_urls(self):
        return self.__hardcoded_urls
            
    def get_all_builtin_metadata(self):
        for url in self.get_all_builtin_urls():
            yield self.load_metainfo(url)
            
    def get_listed_urls(self):
        for url in gconf.client_get_default().get_list(self.__listing_key, gconf.VALUE_STRING):
            if url not in self.__hardcoded_urls:
                yield url
    
    def get_listed(self):
        for url in self.get_listed_urls():
            yield self.load_metainfo(url)        

    def load_metainfo(self, url):
        try:
            return self.__metainfo_cache[url]
        except KeyError, e:
            pass        
        _logger.debug("loading stock url %s", url)
        builtin_scheme = 'builtin://'
        srcurl = url
        if url.startswith(builtin_scheme):
            srcurl = 'file://' + os.path.join(self.__stockdir, url[len(builtin_scheme):])
            baseurl = 'file://' + self.__get_moddir_for_builtin(url)
        else:
            baseurl = os.path.dirname(url)
        try:
            metainfo = pyonlinedesktop.widget.WidgetParser(url, urllib2.urlopen(srcurl), self.__widget_environ, baseurl=baseurl)
            ## FIXME this is a hack - we need to move async processing into StockHolder probably
            url_contents = {}
            for url in metainfo.get_required_urls():
                url_contents[url] = urllib2.urlopen(url).read()
            metainfo.process_urls(url_contents)
        except urllib2.HTTPError, e:
            _logger.warn("Failed to load %s", url, exc_info=True)
            
        self.__metainfo_cache[url] = metainfo
        return metainfo 
    
    def render(self, module, **kwargs):
        (content_type, content_data) = module.content
        pymodule = None
        if content_type == 'online-desktop-builtin':
            pymodule = self.__load_builtin(module, **kwargs)
            if not pymodule: 
                return None

        stylesheet = os.path.join(self.__get_moddir_for_builtin(module.srcurl), 'stock.css')
        if not os.path.exists(stylesheet):
            stylesheet = None

        return StockHolder(module, self.__widget_environ, pymodule=pymodule,
                           panel=kwargs['panel'], stylesheet=stylesheet)
        
    def render_url(self, url, **kwargs):
        return self.render(self.load_metainfo(url), **kwargs)
        
    def __get_moddir_for_builtin(self, url):
        modpath = urlparse.urlparse(url).path
        modfile = os.path.basename(modpath)
        dirname = modfile[:modfile.rfind('.')]
        return os.path.join(self.__stockdir, dirname) + "/"
                
    def __load_builtin(self, metainfo, notitle=False, panel=None):
        dirpath = self.__get_moddir_for_builtin(metainfo.srcurl)
        modpath = urlparse.urlparse(metainfo.srcurl).path
        modfile = os.path.basename(modpath)
        dirname = modfile[:modfile.rfind('.')]        
        _logger.debug("appending to path: %s", dirpath)
        sys.path.append(dirpath)
        pfxidx = modfile.find('_')
        if pfxidx >= 0:
            classname = dirname[pfxidx+1:]
        else:
            classname = dirname
        classname = classname[0].upper() + classname[1:] + 'Stock'
        try:
            _logger.info("importing module %s (title: %s) from dir %s", classname, metainfo.title, dirpath)
            pymodule = __import__(classname)
            class_constructor = getattr(pymodule, classname)
            _logger.debug("got constructor %s", class_constructor)
            if notitle:
                title = ''
            else:
                title = metainfo.title
            stock = class_constructor(metainfo, title=title, panel=panel)

            return stock                  
        except:
            _logger.exception("failed to add stock %s", classname)
            return None
                
    @defer_idle_func(timeout=100)     
    def __on_listings_change(self, *args):
        _logger.debug("processing listings change")
        self.emit("listings-changed")
