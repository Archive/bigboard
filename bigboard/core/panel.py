import logging
import os

import dbus
import gconf
import gobject
import gtk
import hippo

from bigboard.big_widgets import Button, Header, Sidebar
from bigboard.dock_window import DockWindow
from bigboard.globals import BUS_NAME_STR, GCONF_PREFIX
from bigboard.libbig.gutil import *
import bigboard.keybinder
from bigboard.libbig.logutil import log_except
from bigboard.stock import Stock
from bigboard.theme_manager import ThemeManager

try:
    import bigboard.bignative as bignative
except:
    import bignative
    
from exchange import Exchange

_logger = logging.getLogger("bigboard.Panel")

BUS_IFACE=BUS_NAME_STR
BUS_IFACE_PANEL=BUS_IFACE + ".Panel"

class FirstTimeMinimizeDialog(gtk.Dialog):
    __gsignals__ = { 'response' : 'override' }

    def __init__(self, show_windows):
        super(FirstTimeMinimizeDialog, self).__init__("Minimizing Sidebar",
                                                      None,
                                                      gtk.DIALOG_MODAL,
                                                      ('Undo', gtk.RESPONSE_CANCEL,
                                                       gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
        self.set_has_separator(False)
        hbox = gtk.HBox(spacing=8)
        self.vbox.add(hbox)
        if show_windows:
            img_filename = 'windows_key.png'
        else:
            img_filename = 'ctrl_esc_keys.png'
        img_filename = bigboard.globals.find_in_datadir(img_filename)
        _logger.debug("using img %s", img_filename)            
        img = gtk.Image()
        img.set_from_file(img_filename)
        hbox.add(img)
        hbox.add(gtk.Label('''The sidebar is now hidden; press the key shown on the left or the panel button to pop it
back up temporarily.'''))
        
    def do_response(self, id):
        if id != gtk.RESPONSE_CANCEL:
            gconf.client_get_default().set_bool(GCONF_PREFIX + 'first_time_minimize_seen', True)
        else:
            # Avoid set of same value on notify receipt
            gobject.timeout_add(100, self.__idle_undo_visible)
        self.destroy()
        
    def __idle_undo_visible(self):
        gconf.client_get_default().set_bool(GCONF_PREFIX + 'visible', True)
        return False        

class StocksBox(hippo.CanvasBox, hippo.CanvasItem):
    def __init__(self, *args, **kwargs):
        hippo.CanvasBox.__init__(self, *args, **kwargs)
        self.__hidden_width = 0
        self.__visible_width = 0
    
    def do_get_width_request(self):
        minimum, natural = hippo.CanvasBox.do_get_width_request(self)

        max_hidden_width = 0
        max_visible_width = 0

        for child in self.get_children():
            hidden_width = child.get_hidden_width()
            visible_width = child.get_visible_width()
            max_hidden_width = max(max_hidden_width, hidden_width)
            max_visible_width = max(max_visible_width, visible_width)

        if max_hidden_width != self.__hidden_width:
            self.__hidden_width = max_hidden_width
            self.notify('hidden-width')
            
        if max_visible_width != self.__visible_width:
            self.__visible_width = max_visible_width
            self.notify('visible-width')
            
        return minimum, natural

    def get_hidden_width(self):
        return self.__hidden_width
    
    def get_visible_width(self):
        return self.__visible_width

    hidden_width = gobject.property(type=int, getter=get_hidden_width)
    visible_width = gobject.property(type=int, getter=get_visible_width)

gobject.type_register(StocksBox)
    
class Panel(dbus.service.Object):
    def __init__(self, bus_name):
        dbus.service.Object.__init__(self, bus_name, '/bigboard/panel')
          
        _logger.info("constructing")

        self.__visible = False
        self.__popped_out = False
        self.__shell = None
        
        gconf_client = gconf.client_get_default()
        
        self._dw = DockWindow()
        self._dw.connect_after('size-allocate', self.__on_dock_window_size_allocate)
        self._dw.connect_after('realize', self.__on_dock_window_realize)
        
        self.__theme_mgr = ThemeManager.getInstance()
        self.__theme_mgr.connect('theme-changed', self.__sync_theme)
        self.__sync_theme()

        self._dw.set_composited(self.__theme_mgr.composited)

        gconf_client.notify_add(GCONF_PREFIX + 'orientation', self.__sync_orient)
        self.__sync_orient()            

        self.__keybinding = gconf_client.get_string('/apps/bigboard/focus_key')
        if self.__keybinding:
            bigboard.keybinder.tomboy_keybinder_bind(self.__keybinding, self.__on_focus)
    
        self._holders = {} ## metainfo.srcurl to StockHolder

        self._main_box = hippo.CanvasBox()

        self._dw.set_root(self._main_box)

        self._title = hippo.CanvasText(classes='header', text="My Desktop", font="Bold", xalign=hippo.ALIGNMENT_START, padding_left=8)
     
        self._size_button = None
        
        self._stocks_box = StocksBox(spacing=4)
        self._stocks_box.connect('notify::hidden-width', self.__on_hidden_width_changed)
        self._stocks_box.connect('notify::visible-width', self.__on_visible_width_changed)
        
        self._main_box.append(self._stocks_box)
        
        self.__exchange = Exchange(['builtin://self.xml'])
        self.__exchange.connect("listings-changed", lambda *args: self.__sync_listing())
        
        # These are hardcoded as it isn't really sensible to remove them
        self.__hardcoded_stocks = self.__exchange.get_hardcoded_urls()
        hardcoded_metas = map(lambda url: self.__exchange.load_metainfo(url), self.__hardcoded_stocks)
        for metainfo in hardcoded_metas:
            self.__append_metainfo(metainfo, notitle=True)    
        self.__self_stock = self._holders[self.__hardcoded_stocks[0]].get_pymodule()
        self.__self_stock.set_panel_visible(self.__visible)
        self.__self_stock.connect('toggle_visible', self.__on_toggle_visible)
        gobject.idle_add(self.__sync_listing)

        ## visible=True means we never hide, visible=False means we "autohide" and popout
        ## when the hotkey or applet is used
        gconf_client.notify_add(GCONF_PREFIX + 'visible', self.__sync_visible_mode)
        self.__sync_visible_mode()
        
        if not self.__self_stock.info_loaded:
            self.__self_stock.connect('info-loaded', self.__on_info_loaded)
        else:
            self._dw.show()

    @log_except(_logger)
    def __on_info_loaded(self, stock):
        ## This function is where we show the canvas internally; we only want this to 
        ## happen after we've loaded information intially to avoid showing a partially-loaded
        ## state.
        self._dw.show()

    def __on_toggle_visible(self, stock):
        self.__set_visible_mode(not self.__visible)
        
    def __on_header_buttonpress(self, box, e):
        _logger.debug("got shell header click: %s %s %s", e, e.button, e.modifiers)
        if e.button == 2:
            self.Shell()

    @log_except()
    def __idle_show_we_exist(self):
        _logger.debug("showing we exist")
        self.__enter_popped_out_state()
        self.__leave_popped_out_state()

    @log_except()
    def __on_focus(self):
        _logger.debug("got focus keypress")
        vis = gconf.client_get_default().get_bool(GCONF_PREFIX + 'visible')
        ts = bigboard.keybinder.tomboy_keybinder_get_current_event_time()
        if vis:
            self.__do_focus_search(ts)
        else:
            self.toggle_popout(ts)

    def __append_metainfo(self, metainfo, **kwargs):
        try:
            holder = self._holders[metainfo.srcurl]
        except KeyError, e:
            holder = self.__exchange.render(metainfo, panel=self, **kwargs)
            _logger.debug("rendered %s: %s", metainfo.srcurl, holder)
            if holder:
                self._holders[metainfo.srcurl] = holder
        if not holder:
            _logger.debug("failed to load stock from %s", metainfo.srcurl)
            return
        _logger.debug("adding stock %s", holder)
        holder.set_hidden(not self.__visible)
        self._stocks_box.append(holder)
        
    @log_except(_logger)
    def __sync_listing(self):
        _logger.debug("doing stock listing sync")
        new_listed = list(self.__exchange.get_listed())
        new_listed_srcurls = map(lambda mi: mi.srcurl, new_listed)
        for holder in list(self._stocks_box.get_children()):
            if holder.get_metainfo().srcurl in self.__hardcoded_stocks:
                continue

            _logger.debug("unrendering %s", holder)
            
            self._stocks_box.remove(holder)

            if holder.get_metainfo().srcurl not in new_listed_srcurls:
                _logger.debug("removing %s", holder)                
                del self._holders[holder.get_metainfo().srcurl]
                holder.on_delisted()
            
        for metainfo in new_listed:
            self.__append_metainfo(metainfo)
        _logger.debug("done with stock load")            
        
    def get_exchange(self):
        return self.__exchange
        
    def __get_size(self):
        return Stock.SIZE_BULL

    ## If the user performs an action such as launching an app,
    ## that should close a popped-out sidebar, call this
    def action_taken(self):
        _logger.debug("action taken")
        self.__leave_popped_out_state(immediate=True)

    def __on_hidden_width_changed(self, stocks_box, paramspec):
        _logger.debug("Hidden width is now %s", stocks_box.hidden_width)
        if not self.__visible:
            _logger.debug("Setting strut size to %s", stocks_box.hidden_width)
            self._dw.strut_size = stocks_box.hidden_width
        
    def __on_visible_width_changed(self, stocks_box, paramspec):
        _logger.debug("Visible width is now %s", stocks_box.visible_width)
        if self.__visible:
            _logger.debug("Setting strut size to %s", stocks_box.visible_width)
            self._dw.strut_size = stocks_box.visible_width
        
    @log_except()
    def __sync_orient(self, *args):
        orient = gconf.client_get_default().get_string(GCONF_PREFIX + 'orientation')
        if not orient:
            orient = 'west'
        if orient.lower() == 'west':
            gravity = gtk.gdk.GRAVITY_WEST
        else:
            gravity = gtk.gdk.GRAVITY_EAST
        self._dw.set_gravity(gravity)
        
    def __sync_theme(self, *args):
        theme = self.__theme_mgr.get_theme()
        self._dw.set_theme(theme)

    ## There are two aspects to the sidebar state:
    ## the "visible" gconf key is like the old gnome-panel "autohide"
    ## preference. i.e. if !visible, the sidebar is normally collapsed
    ## and you have to use a hotkey or the applet to pop it out.
    ## So the second piece of state is self.__popped_out, which is whether
    ## the sidebar is currently popped out. If visible=True, the sidebar
    ## is always popped out, i.e. self.__popped_out should be True always.

    def __notify_stocks_of_popped_out(self):
        for e in self._holders.values():
            e.set_hidden(not self.__popped_out)
            e.on_popped_out_changed(self.__popped_out)

    ## Shows the sidebar
    def __enter_popped_out_state(self):
        if not self.__popped_out:
            _logger.debug("popping out")

            # we would prefer to need this, if iconify() worked on dock windows
            #self._dw.deiconify()
            self.__popped_out = True

            self.__notify_stocks_of_popped_out()

            self.EmitPoppedOutChanged()

    ## Hides the sidebar, possibly after a delay, only if visible mode is False
    def __leave_popped_out_state(self, immediate=False):
        vis = gconf.client_get_default().get_bool(GCONF_PREFIX + 'visible')
        if self.__popped_out and not vis:
            _logger.debug("unpopping out")
            
            self.__popped_out = False
            
            self.__notify_stocks_of_popped_out()        

            self.EmitPoppedOutChanged()

    def __sync_popped_out(self):
        if self.__visible and not self.__popped_out:
            self.__enter_popped_out_state()
        elif not self.__visible and self.__popped_out:
            self.__leave_popped_out_state()
        
    ## syncs our current state to a change in the gconf setting for visible mode
    @log_except()
    def __sync_visible_mode(self, *args):
        visible = gconf.client_get_default().get_bool(GCONF_PREFIX + 'visible')

        if visible != self.__visible:
            self.__visible = visible
            self.__self_stock.set_panel_visible(visible)

            if self.__visible:
                self._dw.strut_size = self._stocks_box.visible_width
            else:
                self._dw.strut_size = self._stocks_box.hidden_width

            if not visible:
                if not gconf.client_get_default().get_bool(GCONF_PREFIX + 'first_time_minimize_seen'):
                    dialog = FirstTimeMinimizeDialog(True)
                    dialog.show_all()

            self.__sync_popped_out()
        
    ## Pops out the sidebar, and focuses it (if the sidebar is in visible mode, only has to focus)
    def __do_popout(self, xtimestamp):
        if not self.__popped_out:
            _logger.debug("popout requested")
            self.__enter_popped_out_state()
        self.__do_focus_search(xtimestamp)
            
    def __do_focus_search(self, xtimestamp):
        ## focus even if we were already shown
        _logger.debug("presenting with ts %s", xtimestamp)
        self._dw.present_with_time(xtimestamp)
        self.__self_stock.focus_search()

    ## Hides the sidebar, only if not in visible mode
    def __do_unpopout(self):
        if self.__popped_out:
            _logger.debug("unpopout requested")
            self.__leave_popped_out_state(True)

    def toggle_popout(self, xtimestamp):
        if self.__popped_out:
            self.__do_unpopout()
        else:
            self.__do_popout(xtimestamp)

    def __set_visible_mode(self, setting):
        vis = gconf.client_get_default().get_bool(GCONF_PREFIX + 'visible')
        if setting != vis:
            gconf.client_get_default().set_bool(GCONF_PREFIX + 'visible', setting)

    def __set_dock_window_shape(self):
        if self._dw.composited:
            return
        
        region = gtk.gdk.Region()
        
        for holder in self._stocks_box.get_children():
            x, y = holder.get_context().translate_to_widget(holder)
            width, height = holder.get_current_size()
            region.union_with_rect(gtk.gdk.Rectangle(x, y, width, height))

        bignative.window_shape_set_region(self._dw.window, region)

    def __on_dock_window_size_allocate(self, dock_window, allocation):
        if dock_window.window:
            self.__set_dock_window_shape()
            
    def __on_dock_window_realize(self, dock_window):
        self.__set_dock_window_shape()

    @dbus.service.method(BUS_IFACE_PANEL)
    def EmitPoppedOutChanged(self):
        _logger.debug("got emitPoppedOutChanged method call")        
        self.PoppedOutChanged(self.__popped_out)
        
    @dbus.service.method(BUS_IFACE_PANEL)
    def Popout(self, xtimestamp):
        _logger.debug("got popout method call")
        return self.__do_popout(xtimestamp)

    @dbus.service.method(BUS_IFACE_PANEL)
    def Unpopout(self):
        _logger.debug("got unpopout method call")

        ## force us into autohide mode, since otherwise unpopout would not make sense
        self.__set_visible_mode(False)

        return self.__do_unpopout()

    @dbus.service.method(BUS_IFACE_PANEL)
    def Reboot(self):
        import subprocess

        reexec_cmd = os.getenv('BB_REEXEC') or '/usr/bin/bigboard'
        reexec_cmd = os.path.abspath(REEXEC_CMD)
        
        args = [reexec_cmd]
        args.extend(sys.argv[1:])
        if not '--replace' in args:
            args.append('--replace')
        _logger.debug("Got Reboot, executing %s", args)
        subprocess.Popen(args)

    def __create_scratch_window(self):
        w = hippo.CanvasWindow(gtk.WINDOW_TOPLEVEL)
        w.modify_bg(gtk.STATE_NORMAL, gtk.gdk.Color(65535,65535,65535))
        w.set_title('Scratch Window')
        box = CanvasVBox()
        w.set_root(box)
        w.connect('delete-event', lambda *args: w.destroy())
        w.show_all()
        w.present_with_time(gtk.get_current_event_time())
        return box

    @dbus.service.method(BUS_IFACE_PANEL)
    def Shell(self):
        if self.__shell:
            self.__shell.destroy()
        import bigboard.pyshell
        self.__shell = bigboard.pyshell.CommandShell({'panel': self,
                                     '               scratch_window': self.__create_scratch_window},
                                                     savepath=os.path.expanduser('~/.bigboard/pyshell.py'))
        self.__shell.show_all()
        self.__shell.present_with_time(gtk.get_current_event_time())
        
    @dbus.service.method(BUS_IFACE_PANEL)
    def Kill(self):
        try:
            bigboard.keybinder.tomboy_keybinder_unbind(self.__keybinding)
        except KeyError, e:
            pass   
        # This is a timeout so we reply to the method call
        gobject.timeout_add(100, gtk.main_quit)
        
    @dbus.service.method(BUS_IFACE_PANEL)
    def Exit(self):
        gtk.main_quit()

    @dbus.service.signal(BUS_IFACE_PANEL,
                         signature='b')
    def PoppedOutChanged(self, is_popped_out):
        pass

