/* -*- mode: C; c-basic-offset: 4; indent-tabs-mode: nil; -*- */
#include "bigboard-native.h"

#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <glib.h>

#include <X11/extensions/shape.h>

/*  NO_IMPORT declares _PyGObject_Functions extern instead of definint them, which happens in bignative.c */
#define NO_IMPORT_PYGOBJECT
#include <pygobject.h>

#include <gtk/gtk.h>
#include <gdk/gdkx.h>

static PyObject *logging_cb = NULL;

static gboolean initialized_loghandler = FALSE;
static void 
log_handler(const char    *log_domain,
            GLogLevelFlags log_level,
            const char    *message,
            void          *user_data)
{
    PyObject *arglist = NULL;
    PyObject *result;
    
    if (!initialized_loghandler)
        return;
    
    arglist = Py_BuildValue("(sis)", log_domain, log_level, message);
    result = PyEval_CallObject(logging_cb, arglist);
    Py_DECREF(arglist);
    if (result == NULL)
        return;
    Py_DECREF(result);	
}

PyObject *
bigboard_set_log_handler(PyObject *self, PyObject *args)
{
    PyObject *result = NULL;
    PyObject *temp;

    if (PyArg_ParseTuple(args, "O:bigboard_set_log_handler", &temp)) {
        if (!PyCallable_Check(temp)) {
            PyErr_SetString(PyExc_TypeError, "parameter must be callable");
            return NULL;
        }
        Py_XINCREF(temp);
        Py_XDECREF(logging_cb);
      	logging_cb = temp;
      	if (!initialized_loghandler) {
    		g_log_set_handler(NULL,
            		          (GLogLevelFlags) (G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION),
                    	      log_handler, NULL);      		
      		initialized_loghandler = TRUE;
      	}
        Py_INCREF(Py_None);
        result = Py_None;
    }
    return result;
}

PyObject*
bigboard_set_application_name(PyObject *self, PyObject *args)
{
    PyObject *result = NULL;
    char *s;

    if (PyArg_ParseTuple(args, "s:bigboard_set_application_name", &s)) {
        /* my impression from the python docs is that "s" is not owned by us so not freed */
        g_set_application_name(s);
        
        Py_INCREF(Py_None);
        result = Py_None;
    }
    return result;
}

PyObject*
bigboard_set_program_name(PyObject *self, PyObject *args)
{
    PyObject *result = NULL;
    char *s;

    if (PyArg_ParseTuple(args, "s:bigboard_set_program_name", &s)) {
        /* my impression from the python docs is that "s" is not owned by us so not freed */
        g_set_prgname(s);
        
        Py_INCREF(Py_None);
        result = Py_None;
    }
    return result;
}

static gboolean
button_press_event_hook(GSignalInvocationHint  *ihint,
                        guint			n_param_values,
                        const GValue	       *param_values,
                        gpointer		data)
{
    GObject *object;
    GtkWidget *toplevel;

    object = g_value_get_object(param_values);
    
    /* g_printerr("Button press on %s\n", g_type_name_from_instance(object)); */
    
    if (!(GTK_IS_ENTRY(object) || GTK_IS_TEXT_VIEW(object)))
        return TRUE;

    toplevel = gtk_widget_get_toplevel(GTK_WIDGET(object));
    if (!toplevel)
        return TRUE;

    if (!GTK_IS_WINDOW(toplevel))
        return TRUE;

    if (gtk_window_get_type_hint(GTK_WINDOW(toplevel)) != GDK_WINDOW_TYPE_HINT_DOCK)
        return TRUE;

    gtk_window_present_with_time(GTK_WINDOW(toplevel), gtk_get_current_event_time());
    
    return TRUE;
}

PyObject*
bigboard_install_focus_docks_hack(PyObject *self, PyObject *args)
{
    PyObject *result = NULL;

    g_signal_add_emission_hook(g_signal_lookup("button-press-event",
                                               GTK_TYPE_WIDGET),
                               0, button_press_event_hook,
                               NULL, NULL);
    
    Py_INCREF(Py_None);
    result = Py_None;
    return result;
}

PyObject*
bigboard_utf8_collate(PyObject *self, PyObject *args)
{
    PyObject *result = NULL;
    char *a, *b;

    if (PyArg_ParseTuple(args, "ss:bigboard_utf8_collate", &a, &b)) {
        result = PyInt_FromLong(g_utf8_collate(a, b));
    }
    
    return result;
}

PyObject*
bigboard_get_desktop_dir(PyObject *self, PyObject *args)
{
    PyObject *result = NULL;
    
    if (PyArg_ParseTuple(args, ":bigboard_get_desktop_dir")) {
        const char *desktop_dir = g_get_user_special_dir(G_USER_DIRECTORY_DESKTOP);

        result = PyString_FromString(desktop_dir);
    }
    
    return result;
}

PyObject *
bigboard_window_shape_set_region(PyObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "window", "region", "kind", NULL };
    PyGObject *py_window;
    PyObject *py_region;
    GdkWindow *window;
    GdkRegion *region;
    int kind = ShapeBounding;
    GdkRectangle *rectangles;
    XRectangle *xrectangles;
    int n_rectangles;
    int i;
    /* Used to clamp to limits of XRectangle */
    GdkRectangle short_rect = { -0x8000, -0x8000, 0xffff, 0xffff };
    
    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OO|i:bigboard_window_shape_set_region", kwlist, &py_window, &py_region, kind))
        return NULL;

    if (!GDK_IS_WINDOW(py_window->obj)) {
        PyErr_SetString(PyExc_TypeError, "window should be a GdkWindow");
        return NULL;
    }

    window = GDK_WINDOW(py_window->obj);
        
    if (pyg_boxed_check(py_region, g_type_from_name("GdkRegion")))
        region = pyg_boxed_get(py_region, GdkRegion);
    else {
        PyErr_SetString(PyExc_TypeError, "region should be a GdkRegion");
        return NULL;
    }

    if (kind != ShapeBounding && kind != ShapeInput) {
        PyErr_SetString(PyExc_TypeError, "type should be 0 (ShapeBounding) or 2 (ShapeInput)");
        return NULL;
    }

    gdk_region_get_rectangles (region, &rectangles, &n_rectangles);

    xrectangles = g_new(XRectangle, n_rectangles);
    for (i = 0; i < n_rectangles; i++) {
        /* Clamp to the short-int limits of XRectangle */
        gdk_rectangle_intersect(&rectangles[i], &short_rect, &rectangles[i]);
        xrectangles[i].x = rectangles[i].x;
        xrectangles[i].y = rectangles[i].y;
        xrectangles[i].width = rectangles[i].width;
        xrectangles[i].height = rectangles[i].height;
    }

    XShapeCombineRectangles(GDK_WINDOW_XDISPLAY(window), GDK_WINDOW_XWINDOW(window),
                            kind, 
                            0, 0,
                            xrectangles, n_rectangles,
                            ShapeSet, YXBanded);

    g_free(rectangles);
    g_free(xrectangles);
    
    Py_INCREF(Py_None);
    return Py_None;
}
