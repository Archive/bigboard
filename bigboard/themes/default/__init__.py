import sys

import hippo, gtk, gobject, cairo, pangocairo, pango

from bigboard.libbig.singletonmixin import Singleton

class DefaultThemeEngine(gobject.GObject, hippo.CanvasThemeEngine):
    def __init__(self):
        super(DefaultThemeEngine, self).__init__()
        
        self.header_top = self._rgb_to_cairo(0x9EA3A5)
        self.header_top2 = self._rgb_to_cairo(0xFFFFFF)
        self.header_start = self._rgb_to_cairo(0xdEE6EB)        
        self.header_end = self._rgb_to_cairo(0xFFFFFF)
        self.more_1 = self._rgb_to_cairo(0xFFFFFF)
        self.more_2 = self._rgba_to_cairo(0xBBBFC299)
        
    def _rgba_to_cairo(self, color):
        return map(lambda c: c/255.0,
                   ((color & 0xFF000000) >> 24,
                    (color & 0x00FF0000) >> 16,
                    (color & 0x0000FF00) >> 8,                                        
                    (color & 0x000000FF) >> 0))
        
    def _rgb_to_cairo(self, color):
        return map(lambda c: c/255.0,
                   ((color & 0x00FF0000) >> 16,
                    (color & 0x0000FF00) >> 8,                                        
                    (color & 0x000000FF) >> 0))

    def do_paint(self, style, cr, name, x, y, width, height):
        if name == 'header':
            self.paint_header(style, cr, x, y, width, height)
            return True
        elif name == 'header-no-top-border':
            self.paint_header(style, cr, x, y, width, height, topborder=False)
            return True
        elif name == 'more-button':
            self.paint_more_button(style, cr, x, y, width, height)
            return True
        else:
            return False
        
    def paint_header(self, style, cr, x, y, width, height, topborder=True):
        if topborder:
            cr.set_source_rgba(*self.header_top)
            cr.rectangle(x, y, width, 1)
            cr.fill()
        yoff = topborder and 1 or 0
        cr.set_source_rgba(*self.header_top2)
        cr.rectangle(x, y+yoff, width, 1)
        cr.fill()        
        gradient_y_start = y+1+yoff
        gradient_y_height = gradient_y_start+height-1
        pat = cairo.LinearGradient(x, gradient_y_start,
                                   x, gradient_y_height)
        pat.add_color_stop_rgb(0.0, *self.header_start)
        pat.add_color_stop_rgb(1.0, *self.header_end)
        cr.set_source(pat)
        cr.rectangle(x, gradient_y_start, width, gradient_y_height)
        cr.fill()
        
    def paint_more_button(self, style, cr, x, y, width, height):
        inner_y = y + 2
        inner_height = height-2
        cr.set_source_rgb(*self.more_1)
        cr.rectangle(x, inner_y, 1, inner_height)
        cr.fill()
        cr.set_source_rgba(*self.more_2)
        cr.rectangle(x+1, y, 1, height)
        cr.fill()
        cr.set_source_rgb(0.0, 0.0, 0.0)
        ctx = pangocairo.CairoContext(cr)
        layout = ctx.create_layout()
        layout.set_text('More')
        layout.set_font_description(style.get_font()) 
        (more_w, more_h) = map(lambda x: x/pango.SCALE, layout.get_size())
        more_xspace = width - more_w
        if more_xspace < 0: more_xspace = 0
        more_yspace = height - more_h
        if more_yspace < 0: more_yspace = 0
        cr.translate(more_xspace/2 + 1, more_yspace/2)
        cr.set_source_rgba(*self._rgb_to_cairo(style.get_color('color', True)))
        ctx.update_layout(layout)
        ctx.show_layout(layout)
        
gobject.type_register(DefaultThemeEngine)

_instance = None
def getInstance():
    global _instance
    if _instance is None:
        _instance = DefaultThemeEngine()
    return _instance
