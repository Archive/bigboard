import os,sys

import cairo, gobject, hippo

from bigboard.themes.default import DefaultThemeEngine

class FedoraThemeEngine(DefaultThemeEngine):
    def __init__(self):
        super(FedoraThemeEngine, self).__init__()
        
        self.header_top    = self._rgba_to_cairo(0xCBD5DCFF)        
        self.header_start  = self._rgb_to_cairo(0x436A85)
        self.header_end    = self._rgb_to_cairo(0x59809C)
        self.header_bottom = self._rgba_to_cairo(0x244155FF)
        self.prelight = 0x
                
        self.more_1 = self._rgb_to_cairo(0x496D87)
        self.more_2 = self._rgba_to_cairo(0xA9BCCA99)
        self.more_start = self._rgb_to_cairo(0x66859C)
        self.more_end = self._rgb_to_cairo(0x8BA6BA)
        
    def do_paint(self, style, cr, name, x, y, width, height):
        if name == 'header':
            self.paint_header(style, cr, x, y, width, height)
            return True
        elif name == 'header-no-top-border':
            self.paint_header(style, cr, x, y, width, height, topborder=False)
            return True
        elif name == 'more-button':
            self.paint_more_button(style, cr, x, y, width, height)
            return True
        else:
            return False
        
    def paint_header(self, style, cr, x, y, width, height, topborder=True):
        if topborder:
            cr.set_source_rgba(*self.header_top)
            cr.rectangle(x, y, width, 1)
            cr.fill()
        yoff = topborder and 1 or 0
        gradient_y_start = y+yoff
        gradient_y_height = gradient_y_start+height-1
        pat = cairo.LinearGradient(x, gradient_y_start,
                                   x, gradient_y_height)
        pat.add_color_stop_rgb(0.0, *self.header_start)
        pat.add_color_stop_rgb(1.0, *self.header_end)
        cr.set_source(pat)
        cr.rectangle(x, gradient_y_start, width, gradient_y_height)
        cr.fill()
        cr.set_source_rgba(*self.header_bottom)
        cr.rectangle(x, gradient_y_height, width, 1)
        cr.fill()
        
    def paint_more_button(self, style, cr, x, y, width, height):
        gradient_y_start = y+1
        gradient_y_height = gradient_y_start+height-2
        pat = cairo.LinearGradient(x, gradient_y_start,
                                   x, gradient_y_height)
        pat.add_color_stop_rgb(0.0, *self.more_start)
        pat.add_color_stop_rgb(1.0, *self.more_end)
        cr.set_source(pat)
        cr.rectangle(x, gradient_y_start, width, gradient_y_height)
        cr.fill()
        super(FedoraThemeEngine, self).paint_more_button(style, cr, x, y, width, height)

gobject.type_register(FedoraThemeEngine)

_instance = None
def getInstance():
    global _instance
    if _instance is None:
        _instance = FedoraThemeEngine()
    return _instance
