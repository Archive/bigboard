import logging
import os
import re
import weakref

import gconf
import gobject
import gtk
import hippo

from bigboard.globals import GCONF_PREFIX
from bigboard.libbig.logutil import log_except

_logger = logging.getLogger("bigboard.ThemeManager")

class ThemeManager(gobject.GObject):
    __gsignals__ = {
                    'theme-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
                   }
    def __init__(self):
        super(ThemeManager, self).__init__()
        self.__class__.instance = weakref.ref(self)
        screen = gtk.gdk.screen_get_default()
        self.composited = screen.is_composited() and screen.get_rgba_colormap()
        self.__default_theme = None
        self.__theme = None
        gconf.client_get_default().notify_add(GCONF_PREFIX + 'theme', self.__sync_theme)
        self.__sync_theme()
        
    @staticmethod
    def getInstance():
        needinst = False
        if not hasattr(ThemeManager, 'instance'):
            instvalue = None
        else:
            instref = getattr(ThemeManager, 'instance')
            instvalue = instref()
            needinst = instvalue is None
        if instvalue is None:
            inst = ThemeManager()
        else:
            inst = instvalue
        return inst

    def __make_theme(self, themename):
        is_default = False
        if themename == 'Fedora':
            import bigboard.themes.fedora as package
        else:
            if self.__default_theme != None:
                return self.__default_theme
            
            import bigboard.themes.default as package
            is_default = True

        theme_engine = package.getInstance()

        if self.composited:
            stylesheet_name = re.sub(r"__init__.pyc?$", "theme-alpha.css", package.__file__)
        else:
            stylesheet_name = re.sub(r"__init__.pyc?$", "theme-solid.css", package.__file__)

        if not os.path.exists(stylesheet_name):
            stylesheet_name = re.sub(r"__init__.pyc?$", "theme.css", package.__file__)

        theme = hippo.CanvasTheme(theme_engine = theme_engine, theme_stylesheet = stylesheet_name)
        if is_default and self.__default_theme == None:
            self.__default_theme = theme

        return theme

    def make_stock_theme(self, application_stylesheet, default=False):
        if default:
            theme = self.get_default_theme()
        else:
            theme = self.get_theme()

        return hippo.CanvasTheme(theme_engine = theme.props.theme_engine,
                                 theme_stylesheet = theme.props.theme_stylesheet,
                                 application_stylesheet = application_stylesheet)
        
    def get_theme(self):
        return self.__theme

    def get_default_theme(self):
        return self.__make_theme('default')
        
    @log_except(_logger)
    def __sync_theme(self, *args):
        themename = gconf.client_get_default().get_string(GCONF_PREFIX + 'theme')
        _logger.debug("doing theme sync: <THEMENAME>%r</THEMENAME>", themename)
        
        self.__theme = self.__make_theme(themename)
        self.emit('theme-changed')            
         
