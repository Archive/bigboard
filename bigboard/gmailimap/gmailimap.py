# -*- coding: utf-8 -*-

# Copyright (c) 2008 Stas Zykiewicz <stas.zytkiewicz@gmail.com>
#
#           gmailimap.py
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public License
# as published by the Free Software Foundation. A copy of this license should
# be included in the file GPL-3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


import imaplib, string, sys, os, types, ConfigParser

# Python version check
major, minor = sys.version_info[0],sys.version_info[1]
if not (1 < major < 3 or minor < 4):
    print >> sys.stderr, "Found Python version: %s.%s" % (major, minor)
    print >> sys.stderr, "ERROR, GmailImap needs Python >= 2.4 and > 3.0"
    sys.exit(1)

# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText
import email

import Logging
Logging.set_level('debug')
Logging.start()

import logging# configuration was done by the Logging part above

class NoLoginError(Exception):
    pass
class ServerError(Exception):
    pass
class EmailParseError(Exception):
    pass

class ImapAccount:
    def __init__(self,imap_options={}):
        """Class to connect to a Imap account.
        @imap_options can be used to override the ones from the ~/.imap file
        and must be a dictionary providing *all* the options listed in ~/.imap.
        This is can be used in situations where a script is querying different
        accounts.  
        """
        self.logger = logging.getLogger("gmailimap.ImapAccount")
        if imap_options:
            self.logger.debug("Using arguments as imap options")
            for option,val in imap_options.items():
                if option in ('imap_port','smtp_port'):
                    val = int(val)
                setattr(self,option,val)                
        else:
            self.logger.debug("Parsing imap file")
            try:
                self._parse_imapfile(os.path.expanduser('~/.imap'))
            except (ValueError,IOError),e:
                self.logger.error('Invalid data in ~/.imap')
                self.logger.error(e)
                sys.exit(1)
    
    def _parse_imapfile(self,path):
        config = ConfigParser.ConfigParser()
        config.read(path)
        musthaves = ['imap_server','imap_port','imap_user','imap_pass',\
                    'smtp_server','smtp_port']
        for section in ('imap','smtp'):
            for option in config.options(section):
                self.logger.debug("Found option: %s=%s" % (option,config.get(section, option)))
                if option in musthaves:
                    val = config.get(section, option)
                    if option in ('imap_port','smtp_port'):
                        val = int(val)
                    setattr(self,option,val)
                    musthaves.remove(option)
                else:
                    raise ValueError, "%s option not valid, check ~/.imap" % option
        if musthaves:
            raise ValueError, "Not all the options are found in ~/.imap, %s are missing." % musthaves

    def _parse_message_list(self,mlist):
        # parse messages and cleanup the list
        #self.logger.debug("Parsing message list: %s" % mlist)
        msg_objects_list = []
        proper_msg_list = []
        for i in range(0,len(mlist),2): # even parts of the list are flags which we process together with the corresponding messages
            try: 
                id = int(mlist[i][0].split('(RFC822')[0])
            except (ValueError,AttributeError),e:
                EmailParseError,"Message # %s parse error: %s" % (i,e) 
            seenFlag = False
            if len(mlist) > i and mlist[i+1].find("Seen") >= 0:
                seenFlag = True    
            proper_msg_list.append((id, mlist[i][1], seenFlag))
     
            #self.logger.debug("message %s" % mlist[i][1])
            #if len(mlist[i]) > 2:
            #    self.logger.debug("flags %s" % mlist[i][2])

        proper_msg_list.sort(reverse=True)# we use tuples with the gmail id as the first member    
        for item in proper_msg_list:
            try:
                msgs = email.message_from_string(item[1])
            except email.Errors.MessageError, e:
                self.logger.error("Message %s parse error: %s" % (item[1],e))
                raise EmailParseError,"Message %s parse error: %s" % (item[1],e) 
            msg_objects_list.append((item[0],msgs,item[2]))
        return msg_objects_list

    def _make_message_threads(self,mlist):
        # Group messages in threads by using the jwzthreading-gi module.
        # We do this as the THREAD command is unknown on Gmail imap server and
        # perhaps others. See threaded_search as this should also be tested on
        # various imap servers.
        # The threading is based on Jamie Zawinski's algorithm which was used
        # in "Netscape Mail and News" versions 2.0 through 3.0.  
        # For details, see http://www.jwz.org/doc/threading.html.
        id_table = {}
        mlist.sort()# re-reverse it
        import jwzthreading_gi as jwz
        jwz.main(mlist)

    def login_ssl(self):
        """Log into a Gmail Imap (SSL) server.
        Returns True on succes, None on failure.
        """
        try:
            self.AC = imaplib.IMAP4_SSL(self.imap_server,self.imap_port)
            self.AC.login(self.imap_user, self.imap_pass)
            self.logger.debug("Capabilities %s" % str(self.AC.capabilities))
        except imaplib.IMAP4.error,e:
            self.logger.error("imap server login raised an exception, check your ~/.imap file")
            self.logger.error("reason for the exception: %s" % e)
            raise NoLoginError, e
        else:
            self.logger.debug("Logged into Imap_ssl server")
            return True
    
    def login(self):
        """Log into a Gmail Imap server.
        Returns True on succes, None on failure.
        """
        try:
            self.GM = imaplib.IMAP4(self.imap_server,self.imap_port)
            self.GM.login(self.imap_user, self.imap_pass)
        except imaplib.IMAP4.error,e:
            self.logger.error("imap server login raised an exception, check your ~/.imap file")
            self.logger.error("reason for the exception: %s" % e)
            raise GmailError, 'Failed to parse data returned from gmail.'
        else:
            self.logger.debug("Logged into Imap server")
            return True
    
    def logout(self):
        """Log out of the account and close the connection.
        Returns True on succes, None on failure.
        It will check if there's an account or connection opened before trying
        to close it.
        """
        self.logger.debug("Done, closing connection")
        if self.AC:
            try:
                self.AC.logout()
            except Exception:
                self.logger.exception("Error while closing connection")
    
    def get_unread_message_id_list(self):
        """Get a list of unread message ids from the inbox.
        Return a list with ids (integers)
        """
        return self.get_message_ids_by_folder('INBOX', '(UNSEEN UNDELETED)')
    
    def get_all_messages(self):
        """Get all messages from inbox.
        Returns a list with email.message.Message objects or an empty list if
        there are no unread messages. The message objects are message objects from the 
        Python email module.
        Message objects can have multiple messages representing message threads."""
        return self.get_messages_by_folder('INBOX')

    def get_unread_messages(self):
        """Get unread messages from the inbox.
        Returns a list with email.message.Message objects or an empty list if
        there are no unread messages. The message objects are message objects from the 
        Python email module.
        Message objects can have multiple messages representing message threads.
        """
        return self.get_messages_by_folder("INBOX", '(UNSEEN UNDELETED)')

    def get_messages_by_folder(self,folder,criterion=None,max_count=None):
        """Get contents of the @folder.
        Returns a list of email.message.Message objects.
        """
        msg_id_list = self.get_message_ids_by_folder(folder, criterion)
        if max_count and max_count < len(msg_id_list):
            msg_id_list = msg_id_list[-max_count:]
        comma_separated_msg_id_list = ','.join([m for m in msg_id_list])
        msg_list = self.AC.fetch(comma_separated_msg_id_list,'(RFC822 FLAGS)')[1]
        return self._parse_message_list(msg_list)

    def get_message_ids_by_folder(self,folder,criterion=None):
        """Get the ids of messages in the @folder.
        Suitable for getting the count of messages of a certain type in the folder.
        """
        result, message = self.AC.select(folder,readonly=1)
        if result != 'OK':
            raise ServerError,message
        self.logger.debug("Getting message ids from %s" % folder)
        if criterion:
            result, data = self.AC.search(None, criterion)
        else:
            result, data = self.AC.search(None,'ALL')
        if result != 'OK':
            raise ServerError,message
        msg_id_list = data[0].split()
        return msg_id_list

    def get_folder_names(self):
        """List all the Gmail mailfolders.
        Return a list with the all the raw mailfolder names.
        The list should be parsed by the caller to get the actual names.
        It raises an ServerError when it fails.
        """
        result,data = self.AC.list(directory='',pattern='*')
        if result != 'OK':
            raise ServerError,data 
        return data

    def get_folder_unread_counts(self):
        """ this includes INBOX """
        data = self.get_folder_names()
        labels = {}
        # parse the list with strings to get the names
        for item in data:
            # self.logger.debug("item is %s" % (item))
            line = item.split('"/"')[1].split('/')
            if '[Gmail]' in item and len(line) > 1:
                # since Gmail specific names such as "Spam" or "Trash" are not allowed as Gmail labels,
                # there should not be any danger of duplicate keys here        
                labels["[Gmail]/" + line[1][0:-1]] = len(self.get_message_ids_by_folder("[Gmail]/" + line[1][0:-1],'(UNSEEN UNDELETED)'))   
            elif '[Gmail]' not in item:
                labels[line[0][2:-1]] = len(self.get_message_ids_by_folder(line[0][2:-1],'(UNSEEN UNDELETED)'))
        return labels

    def get_folder_unread_count(self, folder):
        return len(self.get_message_ids_by_folder(folder,'(UNSEEN UNDELETED)'))

    def search(self,folder,pattern):
        """Search for messages in @folder which match @pattern.
        Returns a list with email.message.Message objects or an empty list
        no messages were found.
        @pattern must be a search pattern as mentioned in RFC2060 section 6.4.4 
        """
        result, message = self.AC.select(folder,readonly=1)
        if result != 'OK':
            raise ServerError,message
        self.logger.debug("Searching for messages in %s with pattern %s" % (folder,pattern))
        result, data = self.AC.search(None,pattern)
        if result != 'OK':
            raise ServerError,data
        msg_id_list = ','.join([m for m in data[0].split()])
        msg_list = self.AC.fetch(msg_id_list,'(RFC822 FLAGS)')[1]
        return self._parse_message_list(msg_list)    

##      THIS DON'T WORK ON GMAIL IMAP, seems that THREAD is a unkown command ???
    def threaded_search(self,algorithm='REFERENCES',charset='UTF-8',pattern=''):
        """Search and thread the found messages.
        
        THIS DON'T WORK ON GMAIL IMAP, seems that THREAD is a unkown command ???
        
        Returns a list with email.message.Message objects or an empty list
        no messages were found.
        @algorithm and @charset must be conform to the rules set in RFC draft
        http://tools.ietf.org/html/draft-ietf-imapext-thread-12.
        Most of the times you just want to use the defaults values.
        """
        self.logger.debug("Thread messages")
        result, data = self.AC.thread(algorithm,charset,pattern)
        if result != 'OK':
            raise ServerError,data
        return data

    def delete_message(self,id):
        """Delete message(s).
        This will completely delete the message(s)
        @id is a message id string or list of id strings.
        Returns True on succes, raises an ServerError exception on failure.
        """
        result, message = self.AC.store(id, '+FLAGS', '\\Deleted')
        if result != 'OK':
            raise ServerError,message
        self.AC.expunge()
        # close must be called after deleting messages
        self.AC.close()
        return True
    
    def delete_message_from_folder(self,id,folder):
        """Removes a message from the folder.
        This differs from delete_message which will delete the message itself.
        When a message is only in one folder it will be deleted completly.
        @folder is a string.
        @id is a message id.
        Returns True on succes, raises an ServerError exception on failure.
        """
        # we first select the folder to prevent complete deletion
        result, message = self.AC.select(folder,readonly=0)
        if result != 'OK':
            raise ServerError,message
        return self.delete_message(id)
    
    def copy_message(self,id,destfolder):
        """Copy a message to a folder.
        @label is a string.
        @id is a message id string or list of id strings.
        Returns True on succes,raises an ServerError exception on failure.
        """
        result, message = self.AC.select(readonly=1)
        if result != 'OK':
            raise ServerError,message
        result, message = self.AC.copy(id,destfolder)
        if result != 'OK':
            raise ServerError,message
        else:
            return True

    def get_message(self,id):
        """Get one message with @id.
        """
        result, message = self.AC.select(readonly=1)
        if result != 'OK':
            raise ServerError,message
        msg = self.AC.fetch(id,'(RFC822)')[1][0][1]
        return msg

    def send_message(self,mesg):
        """Send a email message
        @mesg must be a TextMessage object.
        return True on succes, None on failure.
        """        
        server = smtplib.SMTP(self.smtp_server,self.smtp_port)
        self.logger.debug("Start a smtp-ssl connection")
        try:
            server.ehlo()
            server.starttls()
            server.ehlo()
            server.login(self.imap_user,self.imap_pass)
            server.sendmail(mesg.sender, [mesg.recipient], mesg.msg.as_string())
        except (smtplib.SMTPHeloError,\
                smtplib.SMTPAuthenticationError,\
                smtplib.SMTPException), e:
            self.logger.error("Failed to log into smtp Gmail server: %s" % e)
            result = None
        else:
            self.logger.debug("Message send")
            result = True
        server.quit()
        return result


class Container:
    """Object used to sort messages in threads"""
    def __init__(self):
        self.message = None
        self.parent = None
        self.child = None
        self.next = None

class TextMessage:
    def __init__(self,sender,recipient,subject,body):
        """Construct a email message
        @sender: string containing the sender address
        @recipient: string containing the recipient address
        @subject: string with the subject
        @body: string containing the message in plain text.
        For mails with attachments use ComposedMessage 
        """
        self.sender = sender
        self.recipient = recipient
        self.msg = MIMEText(body)
        self.msg['Subject'] = subject
        self.msg['From'] = sender
        self.msg['To'] = recipient




if __name__ == '__main__':
    import pprint
    
    ac = ImapAccount()
    ac.login_ssl()
    
    print "number of unread messages"
    print len(ac.get_unread_message_id_list())
    
    print "unread messages from inbox"
    mgs = ac.get_unread_messages()
    pprint.pprint(ac.get_unread_messages())
    
    print "Thread messages"
    import jwzthreading_gi as jwz
    jwz.main(mgs)
    
    print "get message 1"
    print ac.get_message(1)
    
##    print "all INBOX folder messages:"
##    # same as get_unread_messages but now also the read messages
##    pprint.pprint(ac.get_all_messages())
    
##    print "folders:",ac.get_folder_names()
    
    #print "deleting message 1"
    #print ac.delete_message('1')
    
##    print "move message 1 to (Gmail)trash"
##    ac.copy_message('1', '[Gmail]/Trash')
        
    ac.logout()
