# -*- coding: utf-8 -*-

# Copyright (c) 2008 Stas Zykiewicz <stas.zytkiewicz@gmail.com>
#
#           libgmail-imap.py
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public License
# as published by the Free Software Foundation. A copy of this license should
# be included in the file GPL-3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import gmailimap

class GmailAccount(gmailimap.ImapAccount):
    """Wrapper class to be used as an imap based ligmail replacement."""
    def __init__(self):
        gmailimap.ImapAccount.__init__(self)
    
#########################################################
######### libgmail methods #########################
    
    def getMessagesByFolder(self, folderName, allPages = False):
        """

        Folders contain conversation/message threads.

          `folderName` -- As set in Gmail interface.

        libgmail: Returns a `GmailSearchResult` instance."""
        # TODO: convert allPages to criterion
        result = self.get_messages_by_folder(self,folderName)
        
        
    def getMessagesByQuery(self, query,  allPages = False):
        """

        libgmail: Returns a `GmailSearchResult` instance.
        """
        result = search(self,folder=None,pattern=query)
        
    def getQuotaInfo(self, refresh = False):
        """

        Return MB used, Total MB and percentage used.
        """
        pass
        
    def getRawMessage(self, msgId):
        """returns a string with the complete message
        """
        return self.get_message(msgId)
        
    def getUnreadMessages(self):
        """returns GmailSearchResult
        """
        result = self.get_unread_messages()
        
    def getUnreadMsgCount(self):
        """returns an integer
        """
    def sendMessage(self, msg, asDraft = False, _extraParams = None):
        """returns a GmailMessage object ?
        """
        
    def trashMessage(self, msg):
        """returns true on succes
        """
        
    def trashThread(self, thread):
        """returns true on succes
        """
        
    def createLabel(self, labelName):
        """returns true on succes
        """
    def deleteLabel(self, labelName):
        """returns true on succes
        """
    def renameLabel(self, oldLabelName, newLabelName):
        """returns true on succes
        """
        
    def storeFile(self, filename, label = None):
        """returns a GmailMessage object ?
        """
        
############################################################
    
    def getMessagesByLabel(self,label,all=None):
        """Get messages labeled with @label.
        Returns a list of email.message.Message objects.
        """
        # lables are treated as folders by Gmail
        # There are issues with Gmail labels
        # When messages are deleted, moved to Trash, with there label attached
        # they still returnt by GM.search(None, '(UNDELETED)')
        # TODO: convert all to criterion
        return self.get_messages_by_folder(label)    
    
    def getLabelNames(self,foo):
        """List all the Gmail labels.
        Return a list with the label names.
        It raises an ServerError when it fails.
        """
##        Reminder: labels returnt looks like this
##        ['(\\HasNoChildren) "/" "label1"',
##        '(\\HasNoChildren) "/" "labeltester"',
##        '(\\HasNoChildren) "/" "labeltester1"',
##        '(\\HasNoChildren) "/" "testlabel"']

        result,data = self.AC.list(directory='',pattern='*')
        if result != 'OK':
            raise ServerError,data 
        labels = []
        # parse the list with strings to get the names
        for item in data:
            if '[Gmail]' in item or 'INBOX' in item:
                continue
            line = item.split('"/"')[1].split('/')
            labels.append(line[0][2:-1])
        return labels
    
    def add_label(self,id,label):
        """Add a label to a message.
        @label is a string.
        @id is a message id string.
        Returns True on succes, None on failure.
        Adding a lable in Gmail is basically copying a message to a folder.
        """
        return self.copy_message(id,label)
            
    def remove_label(self,id,label):
        """Removes a label from a message.
        @label is a string.
        @id is a message id.
        Returns True on succes, None on failure.
        Removing a lable in Gmail is basically removing a message from a folder.
        """
        self.delete_message_from_folder(id,label)

if __name__ == '__main__':
    import pprint
    
    ac = GmailAccount()
    ac.login_ssl()
    
##    print "number of unread messages"
##    print len(ac.get_unread_message_id_list())
##    
##    print "unread messages from inbox"
##    pprint.pprint(ac.get_unread_messages())
    
    print "all INBOX folder messages:"
    # same as get_unread_messages but now also the read messages
    messages = ac.get_all_messages()
    for message in messages:
        print message[1].as_string()
    
    print "folders:",ac.get_folder_names()
    
##    print "deleting message 1"
##    print ac.delete_message('1')

    print "remove label label1"
    print ac.remove_label('label1','1')

##    print "move message 1 to (Gmail)trash"
##    ac.copy_message('1', '[Gmail]/Trash')
        
    
    ac.logout()
