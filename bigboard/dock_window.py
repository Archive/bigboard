import hippo
import gobject
import gtk

_SPACING = 5

class RootWindowWatcher(gtk.Invisible):
    """Class to track properties of the root window.

    The tracking is a distinctly hacky; what we do is set the user data of the root window
    to point to an GtkInvisible and catch the property-notify events there, since we can't use
    gdk_window_add_filter() from Python. If someone else uses the same trick in the
    same process, we'll break.

    """
    
    __gsignals__ = {
        'realize' : 'override',
        'unrealize' : 'override',
        'workarea-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        }

    @staticmethod
    def get_for_screen(screen):
        watcher = screen.get_data("RootWindowWatcher")
        if watcher == None:
            watcher = RootWindowWatcher(screen)
            screen.set_data("RootWindowWatcher", watcher)

        return watcher

    def __init__(self, screen):
        """Don't call this, call get_for_screen() instead. (Might be possible to make
           a singleton with __new__, but the GObject interaction could be tricky.)"""
        
        super(RootWindowWatcher, self).__init__()
        self.set_screen(screen)

    def do_realize(self):
        super(RootWindowWatcher, self).do_realize(self)
        
        screen = self.get_screen()
        rootw = screen.get_root_window()
        self.__old_events = rootw.get_events()

        rootw.set_events(self.__old_events | gtk.gdk.PROPERTY_CHANGE_MASK)
        rootw.set_user_data(self)

        self.__compute_workarea()

    def do_unrealize(self):
        rootw.set_events(self.__old_events)
        rootw.set_user_data(None)
        
        super(RootWindowWatcher, self).do_unrealize(self)

    def do_property_notify_event(self, event):
        if event.atom == "_NET_WORKAREA":
            old_workarea = self.__workarea
            self.__compute_workarea()
            if (self.__workarea != old_workarea):
                self.emit("workarea-changed")

    def __compute_workarea(self):
        screen = self.get_screen()
        rootw = screen.get_root_window()
        prop = rootw.property_get("_NET_WORKAREA")
        (_, _, workarea) = prop
        self.__workarea = (workarea[0], workarea[1], workarea[2], workarea[3])

    def get_workarea(self):
        return self.__workarea

class DockWindow(hippo.CanvasWindow):
    __gsignals__ = {
        'realize' : 'override',
        'screen-changed' : 'override',
        'size-allocate' : 'override',
        'show' : 'override',
        }

    def __init__(self, edge_gravity=gtk.gdk.GRAVITY_WEST):
        hippo.CanvasWindow.__init__(self)

        self.set_type_hint(gtk.gdk.WINDOW_TYPE_HINT_DOCK)
        self.set_decorated(False)
        self.set_resizable(False)
        
        self.composited = False
            
        self.edge_gravity = edge_gravity
        self.__strut_size = -1
        self.__watcher = None

    def set_composited(self, composited):
        if composited == self.composited:
            return

        self.composited = composited

        if composited:
            colormap = self.get_screen().get_rgba_colormap()

        if colormap == None:
            colormap = self.get_screen().get_rgb_colormap()
            
        self.set_colormap(colormap)
        
    def do_show(self):
        self.__update_watcher()
        super(DockWindow, self).do_show(self)

    def do_screen_changed(self, old_screen):
        super(DockWindow, self).do_screen_changed(old_screen)

        self.__update_watcher()

    def __set_strut(self):
        strut_size = self.__strut_size

        if self.__strut_size < 0:
            if (self.edge_gravity == gtk.gdk.GRAVITY_WEST or 
                self.edge_gravity == gtk.gdk.GRAVITY_EAST):
                strut_size = self.allocation.width
            else:
                strut_size = self.allocation.height

        strut_size += _SPACING
                
        if self.edge_gravity == gtk.gdk.GRAVITY_WEST:
            # left, right, top, bottom
            propvals = [strut_size, 0, 0, 0]
        elif self.edge_gravity == gtk.gdk.GRAVITY_EAST:
            propvals = [0, strut_size, 0, 0]
        elif self.edge_gravity == gtk.gdk.GRAVITY_NORTH:
            propvals = [0, 0, strut_size, 0]
        elif self.edge_gravity == gtk.gdk.GRAVITY_SOUTH:
            propvals = [0, 0, 0, strut_size]
        
        self.window.property_change("_NET_WM_STRUT",
                                    "CARDINAL",
                                    32,
                                    gtk.gdk.PROP_MODE_REPLACE,
                                    propvals)
        
    def do_size_allocate(self, allocation):
        super(DockWindow, self).do_size_allocate(self, allocation)
        if self.__strut_size < 0 and self.flags() & gtk.REALIZED:
            self.__set_strut()

    def do_realize(self):
        super(DockWindow, self).do_realize(self)
        self.__set_strut()

    def __update_watcher(self):
        if self.__watcher != None:
            self.__watcher.disconnect(self.__watcher_handler_id)
        self.__watcher = RootWindowWatcher.get_for_screen(self.get_screen())
        self.__watcher_handler_id = self.__watcher.connect('workarea-changed', self.__on_workarea_changed)
        self.__on_workarea_changed(self.__watcher)

    def __on_workarea_changed(self, watcher):
        (workarea_x,workarea_y,workarea_width,workarea_height) = watcher.get_workarea()
        
        if (self.edge_gravity == gtk.gdk.GRAVITY_WEST or
            self.edge_gravity == gtk.gdk.GRAVITY_NORTH):
            monitor = self.get_screen().get_monitor_at_point(workarea_x, workarea_y)
        elif (self.edge_gravity == gtk.gdk.GRAVITY_EAST or
              self.edge_gravity == gtk.gdk.GRAVITY_SOUTH):
            monitor = self.get_screen().get_monitor_at_point(workarea_x + workarea_height - 1,
                                                             workarea_y + workarea_width - 1)

        (monitor_x, monitor_y, monitor_width, monitor_height) = self.get_screen().get_monitor_geometry(monitor)
        request_width, request_height = self.size_request()

        if (self.edge_gravity == gtk.gdk.GRAVITY_WEST or
            self.edge_gravity == gtk.gdk.GRAVITY_EAST):
            y = workarea_y
            height = workarea_height
#            height = monitor_y + monitor_height - y
            
            if self.edge_gravity == gtk.gdk.GRAVITY_WEST: 
                x = 0
            else:
                x = monitor_x + monitor_width - request_width
            
            at_top = y == monitor_y
            at_bottom = y +  height == monitor_y + monitor_height

            if not at_top:
                y += _SPACING
                height -= _SPACING

            if not at_bottom:
                height -= _SPACING
        
            self.set_size_request(-1, height)
            
        elif (self.edge_gravity == gtk.gdk.GRAVITY_NORTH or
              self.edge_gravity == gtk.gdk.GRAVITY_SOUTH):
            x = workarea_x
            width = workarea_width
            
            at_left = x == monitor_x
            at_right = x +  width == monitor_x + monitor_width
            
            if self.edge_gravity == gtk.gdk.GRAVITY_NORTH: 
                y = 0
            else:
                y = monitor_y + monitor_height - request_height
            
            if not at_left:
                x += _SPACING
                width -= _SPACING

            if not at_right:
                width -= _SPACING

            self.set_size_request(width, -1)
        
        self.move(x, y)

    def get_strut_size(self):
        return self.__strut_size

    def set_strut_size(self, size):
        self.__strut_size = size
        if self.flags() & gtk.REALIZED:
            self.__set_strut()
        
    strut_size = gobject.property(type=int, getter=get_strut_size, setter=set_strut_size)
