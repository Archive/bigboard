import logging, dbus, threading, gobject, time, gtk
import urllib
import hippo
import twyt.twitter as twyt
import twyt.data as twyt_data
from bigboard.stock import Stock
from bigboard.slideout import Slideout
import bigboard.libbig.gutil as gutil
from bigboard.libbig.logutil import log_except
import bigboard.libbig as libbig
from bigboard.big_widgets import PrelightingCanvasBox, CanvasVBox, CanvasHBox, Header, CanvasURLImage

_logger = logging.getLogger('bigboard.stocks.TwitterStock')

LOGIN_TO_TWITTER_STRING = "Login to Twitter"
FAILED_TO_LOGIN_STRING = "Failed to login."

TYPE_TWITTER = "twitter"
TWITTER_USER_AGENT = "gnomeonlinedesktop"

TWITTER_STATUS_MAX_LENGTH = 140

FRIENDS_UPDATES_LIMIT = 3

def make_update_string(result, details):
    string = ""
    try:
        if details:
            utc = time.mktime(time.strptime(
                              "%s UTC" % result.created_at, "%a %b %d %H:%M:%S +0000 %Y %Z"))
            stamp = time.strftime("%a %b %d %H:%M:%S %Y", time.localtime(utc))

	    string = "%s (%s via %s)" % (result.text, stamp, result.source)
        else:
            string = "%s" % (result.text)                      
    except AttributeError, e:
	_logger.debug("AttributeError when parsing result %s" % e)

    return string

class UpdateSlideout(Slideout):
    def __init__(self, result):
        super(UpdateSlideout, self).__init__()
        vbox = CanvasVBox(border_color=0x0000000ff, spacing=4, box_width=320)
        self.get_root().append(vbox)
        self.__header = Header(topborder=False)
        # TODO: for some reason, specifying an additional class with  the color did not set the color
        text = hippo.CanvasText(classes='header', color=0x00AABBFF, text=result.user.screen_name, xalign=hippo.ALIGNMENT_START, padding=2)
        self.__header.append(text, hippo.PACK_EXPAND)        
        vbox.append(self.__header)
        
        self.id = result.id        

        hbox = CanvasHBox(spacing=4)
      
        image = CanvasURLImage(result.user.profile_image_url, padding=2) 
        status = hippo.CanvasText(text=make_update_string(result, True), xalign=hippo.ALIGNMENT_START,
                                  size_mode=hippo.CANVAS_SIZE_WRAP_WORD, padding=2)
        hbox.append(image)
        hbox.append(status, hippo.PACK_EXPAND) 
        vbox.append(hbox)

class CheckTwitterTask(libbig.polling.Task):
    def __init__(self, twitter_stock):
        libbig.polling.Task.__init__(self, 1000 * 120, initial_interval=0)
        self.__twitter_stock = twitter_stock  
        
    def do_periodic_task(self):
        self.__twitter_stock.do_periodic_updates()

class TwitterStock(Stock):

    def __init__(self, *args, **kwargs):
        Stock.__init__(self, *args, **kwargs)
        _logger.debug("Hello Twitter")
        self.__box = hippo.CanvasBox(orientation=hippo.ORIENTATION_VERTICAL)

        self.__login_button = hippo.CanvasButton(text=LOGIN_TO_TWITTER_STRING)
        self.__login_button.connect('activated', lambda button: self.__open_login_dialog())
        self.__box.append(self.__login_button)
  
        self.__twitter_status_counter_text = hippo.CanvasText(text=str(TWITTER_STATUS_MAX_LENGTH))
   
        self.__twitter_status_input = hippo.CanvasEntry()
        self.__twitter_status_input.get_property("widget").set_max_length(TWITTER_STATUS_MAX_LENGTH)
        self.__twitter_status_input.connect("notify::text", self.__on_status_edited)
        self.__twitter_status_input.connect("key-press-event", self.__on_status_submitted)

        self._add_more_button(self.__on_more_button)

        self.__friends_updates_box = hippo.CanvasBox(orientation=hippo.ORIENTATION_VERTICAL)

        self.__slideout = None
        self.__last_slideout_event_time = None  

        self.__twitter = twyt.Twitter()
        self.__twitter.set_user_agent("gnome")

        self.__check_twitter_task = CheckTwitterTask(self)

        # even though the account system can theoretically return multiple Twitter accounts, this stock
        # only supports one -- the last one that was added
        self.__twitter_account = None
        self.__twitter_account_changed_signal_match = None

        self.__onlineaccounts_proxy = None

        try:
            self.__onlineaccounts_proxy = dbus.SessionBus().get_object('org.gnome.OnlineAccounts', '/onlineaccounts')
        except dbus.DBusException, e:
            _logger.error("onlineaccounts DBus service not available, can't get twitter accounts")
            return
        
        try: 
            self.__onlineaccounts_proxy.EnsureAccountType(TYPE_TWITTER, "Twitter", "Twitter username", "http://twitter.com")
        except dbus.DBusException, e:
            _logger.error(e.message)
            return

        all_twitter_account_paths = self.__onlineaccounts_proxy.GetEnabledAccountsWithTypes([TYPE_TWITTER])
        for a_path in all_twitter_account_paths:
            self.__on_account_added(a_path)

        self.__connections = gutil.DisconnectSet()

        id = self.__onlineaccounts_proxy.connect_to_signal('AccountEnabled', self.__on_account_added)
        self.__connections.add(self.__onlineaccounts_proxy, id)
        id = self.__onlineaccounts_proxy.connect_to_signal('AccountDisabled', self.__on_account_removed)
        self.__connections.add(self.__onlineaccounts_proxy, id)

    def _on_delisted(self):
        self.__remove_current_account()
        self.__connections.disconnect_all()

    def get_content(self, size):
        return self.__box

    @log_except(_logger)
    def __on_account_added(self, acct_path):
        try:
            _logger.debug("acct_path is %s" % acct_path)
            acct = dbus.SessionBus().get_object('org.gnome.OnlineAccounts', acct_path)
        except dbus.DBusException, e:
            _logger.error("onlineaccount for path %s was not found" % acct_path)
            return

        if acct.GetType() != TYPE_TWITTER:
            return
       
        _logger.debug("in __on_account_added for %s %s" % (str(acct), acct.GetUsername()))    
  
        self.__remove_current_account()
       
        self.__twitter_account = acct
        self.__twitter_account_changed_signal_match = \
            self.__twitter_account.connect_to_signal('Changed', self.__on_twitter_account_changed)
        self.__on_twitter_account_changed()  
    
    @log_except(_logger)
    def __remove_current_account(self):
        if self.__twitter_account:
            _logger.debug("in __remove_current_account for %s", self.__twitter_account.GetObjectPath())   
            self.__twitter_account_changed_signal_match.remove()
            self.__twitter_account_changed_signal_match = None 
            self.__twitter_account = None
            self.__check_twitter_task.stop() 
            self.__box.remove_all() 
            self.__box.append(self.__login_button)

    @log_except(_logger)
    def __on_account_removed(self, acct_path):
        _logger.debug("in __on_account_removed")
        if self.__twitter_account and self.__twitter_account.GetObjectPath() == acct_path: 
            self.__remove_current_account()
            
            # try getting some other Twitter account in case there is another one out there
            # that is enabled  
            all_twitter_account_paths = self.__onlineaccounts_proxy.GetEnabledAccountsWithTypes([TYPE_TWITTER])
            for a_path in all_twitter_account_paths:
                self.__on_account_added(a_path)

    @log_except(_logger)
    def __on_twitter_account_changed(self):
        _logger.debug("will change stuff")
        username = self.__twitter_account.GetUsername()
        password = self.__twitter_account.GetPassword() 
        if password != "":
            self.__box.remove_all()
            checking = hippo.CanvasText(text="Checking credentials for " + username + "...",
                                          size_mode=hippo.CANVAS_SIZE_WRAP_WORD, classes="info")
            self.__box.append(checking)
            self.__get_friends_updates(username, password)
            self.__check_twitter_task.start()
        else:
            self.__add_login_button()  

    def __on_status_edited(self, input, param_spec):
        self.__twitter_status_counter_text.set_property("text", str(TWITTER_STATUS_MAX_LENGTH - len(self.__twitter_status_input.get_property("text"))))

    def __on_status_submitted(self, entry, event):
        if event.key == hippo.KEY_RETURN:
            status = self.__twitter_status_input.get_property("text").strip()
            if status != "":
                _logger.debug("will send status: %s" % status)
                username = self.__twitter_account.GetUsername()
                password =  self.__twitter_account.GetPassword()
                self.__twitter.set_auth(username, password)
                self.__twitter.set_user_agent(TWITTER_USER_AGENT)  
                try:
                    self.__twitter.status_update(status)
                    _logger.debug("Twitter status update went fine")
                    gobject.timeout_add(1000 * 5, self.__get_friends_updates, username, password)
                    self.__twitter_status_input.set_property("text", "")
                    self.__twitter_status_counter_text.set_property("text", "sent...")
                except twyt.TwitterException, e:
                    _logger.debug("caught an exception %s" % e)
                    # most likely this was an authentication failure
                    self.__on_twitter_error(username, password)

    def __on_first_twitter_response(self):
        _logger.debug("Authentication must be good")
        self.__box.remove_all()
        hello_message = hippo.CanvasText(text="Update status for " + self.__twitter_account.GetUsername() + ":",
                                        size_mode=hippo.CANVAS_SIZE_WRAP_WORD)
        self.__box.append(hello_message)
        self.__box.append(self.__twitter_status_counter_text)
        self.__box.append(self.__twitter_status_input)  
       
        # remove everything from the friends updates box in case there are friends updates from the 
        # previous account that are stored there
        self.__friends_updates_box.remove_all()
        self.__box.append(self.__friends_updates_box)

    def do_periodic_updates(self):
        t = threading.Thread(target=self.__get_friends_updates, 
                             kwargs={"username": self.__twitter_account.GetUsername(), "password": self.__twitter_account.GetPassword()},
                             name="Twitter Updates Request")
        t.setDaemon(True)
        t.start()

    def __get_friends_updates(self, username, password):
        self.__twitter.set_auth(username, password)
        self.__twitter.set_user_agent(TWITTER_USER_AGENT)  
        try:
            # _logger.debug("will send request to twitter")
            answer = self.__twitter.status_friends_timeline()
            # _logger.debug("got answer from Twitter") # %s" % answer)
            results = twyt_data.StatusList(answer)
            # _logger.debug("number of results: %s" % str(len(results)))
            gobject.idle_add(self.__process_friends_updates, results, username, password)
        except twyt.TwitterException, e:
            _logger.debug("caught an exception %s" % e)
            # most likely this was an authentication failure
            gobject.idle_add(self.__on_twitter_error, username, password)

    def __process_friends_updates(self, results, username, password):
        # _logger.debug("processing friends' updates")
        if self.__same_credentials(username, password):
            if len(self.__box.get_children()) == 1:
                self.__on_first_twitter_response()
            self.__on_status_edited(None, None)
            self.__friends_updates_box.remove_all()
            i = 0
            for result in results:
                if i >= FRIENDS_UPDATES_LIMIT: break

                box = PrelightingCanvasBox(orientation=hippo.ORIENTATION_VERTICAL)

                box.connect("button-press-event", self.create_update_slideout, result)
                self.__friends_updates_box.append(box)
                name = hippo.CanvasText(text=result.user.screen_name, xalign=hippo.ALIGNMENT_START, classes="friend-name")
                status = hippo.CanvasText(text=make_update_string(result, False), xalign=hippo.ALIGNMENT_START,
                                          size_mode=hippo.CANVAS_SIZE_WRAP_WORD)
                box.append(name) # hippo.PACK_FLOAT_LEFT) 
                box.append(status)
                i += 1

                _logger.debug("%s" % result)
        else:
            _logger.debug("the credentials changed while we were getting friends updates from Twitter")

    def __on_twitter_error(self, username, password):
        _logger.debug("There was a Twitter error.")
        if self.__same_credentials(username, password):
            self.__add_login_button(True)

    def __same_credentials(self, username, password):
        return self.__twitter_account and self.__twitter_account.GetUsername() == username and self.__twitter_account.GetPassword() == password

    def __add_login_button(self, failed_to_login = False):
        self.__check_twitter_task.stop() 
        self.__box.remove_all()
        if failed_to_login:
            error = hippo.CanvasText(text=FAILED_TO_LOGIN_STRING, size_mode=hippo.CANVAS_SIZE_WRAP_WORD, classes="error")
            self.__box.append(error) 
        hello_message = hippo.CanvasText(text="Hello Twitter User " + self.__twitter_account.GetUsername(),
                                         size_mode=hippo.CANVAS_SIZE_WRAP_WORD)
        self.__box.append(hello_message)                           
        self.__box.append(self.__login_button) 
 
    def __open_login_dialog(self):
        if self.__onlineaccounts_proxy:
            self.__onlineaccounts_proxy.OpenAccountsDialogWithTypes([TYPE_TWITTER], gtk.get_current_event_time())
        else: 
            _logger.error("onlineaccounts proxy is not initialized, can't open the dialog for twitter accounts")    

    def __on_more_button(self):
        if self.__twitter_account:
            url = "http://twitter.com/home"
        else:
            url = "http://twitter.com"
        libbig.show_url(url)

    def create_update_slideout(self, widget, hippo_event, result):
        if self.__slideout and self.__slideout.id == result.id and self.__last_slideout_event_time == gtk.get_current_event_time():
            self.__slideout = None
            return 
        self.__slideout = UpdateSlideout(result)
        def on_slideout_close(s, action_taken):
            self.__last_slideout_event_time = gtk.get_current_event_time() 
            if action_taken:
                self._panel.action_taken()
            s.destroy()
            self._set_active(False)
        self._set_active(True)
        self.__slideout.connect('close', on_slideout_close)
        y = widget.get_context().translate_to_screen(widget)[1]
        if not self.__slideout.slideout_from(204, y):
            self.__slideout.destroy()
            self.__slideout = None
            self._set_active(False)
            return

