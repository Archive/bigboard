import logging

import gconf, hippo

import bigboard.globals as globals
import bigboard.libbig as libbig
from bigboard.libbig.gutil import defer_idle_func
from bigboard.big_widgets import CanvasVBox, ActionLink
import bigboard.stock

import apps, appbrowser, apps_widgets

import bigboard.search as search

_logger = logging.getLogger("bigboard.stocks.AppsStock")

GCONF_KEY_APP_SIZE = '/apps/bigboard/application_list_size'

# TODO: with some applications, the name of the file is less predictable, while the official name of the application is more predictable, 
# for example on my system Evolution is redhat-evolution-mail and Open Office Writer is openoffice.org-1.9-writer,
# so we might try to do this selection using the names, such as "Firefox Web Browser", "Terminal", "File Browser", "Email", "Text Editor", etc.
POPULAR_APPS = ["mozilla-firefox", "gnome-terminal", "gnome-nautilus", "evolution", "gnome-gedit", "evince", "mozilla-thunderbird", "rhythmbox", "totem", "gnome-eog", "gnome-file-roller", "epiphany", "openoffice.org-write", "liferia", "xchat", "synaptic", "pup", "gnome-volume-control", "openoffice.org-clac", "gnome-gcalctool", "gnome-system-monitor", "amarok", "gimp", "xchat-gnome", "gnome-bug-buddy", "gnu-emacs", "eclipse", "gaim", "openoffice.org-impress", "vncviewer"]
    
class AppDisplayLauncher(apps_widgets.AppDisplay):
    def __init__(self):
        super(AppDisplayLauncher, self).__init__()
  

class AppsStock(bigboard.stock.AbstractMugshotStock):
    __gsignals__ = {

    }    
    def __init__(self, *args, **kwargs):
        super(AppsStock, self).__init__(*args, **kwargs)

        search.enable_search_provider('apps')

        self.__box = CanvasVBox(spacing=3)
        self.__message = hippo.CanvasText()
        self.__message_link = ActionLink()
        self.__message_link.connect("button-press-event", lambda link, event: self.__on_message_link())
        self.__message_link_url = None
        self.__subtitle = hippo.CanvasText(classes='stock-subtitle')
        self.__applications = CanvasVBox()
        
        self.__box.append(self.__message)
        self.__box.append(self.__message_link)        
        self.__box.append(self.__subtitle)
        self.__box.append(self.__applications)
        
        self.__app_browser = None
        self._add_more_button(self.__on_more_button)
        
        gconf.client_get_default().notify_add(GCONF_KEY_APP_SIZE, self.__on_app_size_changed)
         
        self.__set_message('Loading...')

        self.__repo = apps.get_apps_repo()

        self.__repo.connect('enabled-changed', self.__on_usage_enabled_changed)
        self.__repo.connect('all-apps-loaded', self.__on_all_apps_loaded)
        self.__repo.connect('my-pinned-apps-changed', self.__on_my_pinned_apps_changed)
        self.__repo.connect('my-top-apps-changed', self.__on_my_top_apps_changed)
        self.__repo.connect('global-top-apps-changed', self.__on_global_top_apps_changed)
        self.__repo.connect('app-launched', self.__on_app_launched)

        self.__sync()

    def _on_ready(self):
        # When we disconnect from the server we freeze existing content, then on reconnect
        # we clear everything and start over.
        _logger.debug("Connected to data model")

    def __on_query_error(self, where, error_code, message):
        _logger.warn("Query '" + where + "' failed, code " + str(error_code) + " message: " + str(message))

    def __on_usage_enabled_changed(self, repo):
        _logger.debug("usage enabled changed")
        self.__sync()

    def __on_all_apps_loaded(self, repo):
        _logger.debug("all apps are loaded")
        self.__sync()

    def __on_my_pinned_apps_changed(self, repo, pinned_apps):
        _logger.debug("Pinned apps changed from apps repo: " + str(pinned_apps))
        self.__sync()

    def __on_my_top_apps_changed(self, repo, my_top_apps):
        _logger.debug("My top apps changed from apps repo: " + str(my_top_apps))
        self.__sync()

    def __on_global_top_apps_changed(self, repo, global_top_apps):
        _logger.debug("Global top apps changed from apps repo: " + str(global_top_apps))
        self.__sync()
        
    def __on_app_size_changed(self, *args):
        _logger.debug("app size changed")  
        self.__sync()

    def __on_app_launched(self, repo, app):
        self._panel.action_taken()

    def __on_more_button(self):
        _logger.debug("more!")
        if self.__app_browser is None:
            self.__app_browser = appbrowser.AppBrowser(self.get_path("browser.css"))
        if self.__app_browser.get_property('is-active'):
            self.__app_browser.hide()
        else:
            self.__app_browser.present()
        
    def __on_message_link(self):
        libbig.show_url(self.__message_link_url)
        
    def __set_message(self, text, link=None):
        self.__box.set_child_visible(self.__message, (not text is None) and link is None)
        self.__box.set_child_visible(self.__message_link, not (text is None or link is None))        
        if text:
            self.__message.set_property("text", text)
            self.__message_link.set_property("text", text)
        if link:
            self.__message_link_url = link
            
    def __set_subtitle(self, text):
        self.__box.set_child_visible(self.__subtitle, not text is None)
        if text:
            self.__subtitle.set_property("text", text)        

    def get_authed_content(self, size):
        return self.__box

    def get_unauthed_content(self, size):
        return self.__box
            
    def __set_item_size(self, item, size):
        if size == bigboard.stock.Stock.SIZE_BULL:
            item.set_property('xalign', hippo.ALIGNMENT_FILL)
        else:
            item.set_property('xalign', hippo.ALIGNMENT_CENTER)
        item.set_size(size)            
            
    def set_size(self, size):
        super(AppsStock, self).set_size(size)
        for child in self.__applications.get_children():
            self.__set_item_size(child, size)        

    def __get_popular_local_apps(self):
        local_apps = self.__repo.get_local_apps()

        result = [app for app in local_apps if app.get_app_name_from_file_name() in POPULAR_APPS]
        result.sort(lambda a, b: cmp(POPULAR_APPS.index(a.get_app_name_from_file_name()),
                                     POPULAR_APPS.index(b.get_app_name_from_file_name())))

        return result

    def __cmp_application_name(self, a, b):
        return cmp(a.get_name(), b.get_name())

    def __cmp_application_usage(self, a, b):
        # note the "-" in front of the cmp to sort descending
        return - cmp(a.get_usage_count(), b.get_usage_count())

    def __fill_applications(self):
        self.__set_subtitle(None)
        self.__applications.remove_all()

        apps_in_set = []

        if self.__repo.get_app_usage_enabled():
            # Pinned apps are returned as an unordered set, we need
            # to sort them ourselves.
            pinned_apps = list(self.__repo.get_pinned_apps())
            pinned_apps.sort(self.__cmp_application_name)

            # Server returns a list sorted by the user's usage, keep that
            # ordering unchanged. (Note that the user's usage is not the
            # same as applicatin.get_usage_count() which is a global usage
            # across the user's usage.)
            my_top_apps = self.__repo.get_my_top_apps()
        else:
            pinned_apps = []
            my_top_apps = []

        global_top_apps = self.__repo.get_global_top_apps()
        global_top_apps.sort(self.__cmp_application_usage)

        for app in (pinned_apps + my_top_apps + global_top_apps):
            if app not in apps_in_set:
                apps_in_set.append(app)

        if not apps_in_set:
            apps_in_set = self.__get_popular_local_apps()

        if global_top_apps and not my_top_apps and not pinned_apps:
            self.__set_subtitle("Popular Applications")

        size = gconf.client_get_default().get_int(GCONF_KEY_APP_SIZE) or 7
        count = 0
        for app in apps_in_set:
            if count >= size:
                break

            # don't display apps that are not installed if the user is not logged in
            if not self._model.self_resource and not app.is_installed():
                continue

            display = apps_widgets.AppDisplay(apps_widgets.AppLocation.STOCK, app)
            display.connect("button-press-event", lambda display, event: display.launch()) 
            #_logger.debug("setting static set app: %s", app)
            self.__applications.append(display)
            
            count += 1

    @defer_idle_func(logger=_logger)
    def __sync(self):
        #_logger.debug("doing sync")
        
        self.__set_message(None)        
             
        usage = self.__repo.get_app_usage_enabled()

        #_logger.debug("usage: %s", usage)

        if usage is False and self._model.ready and self._model.global_resource.online:
            self.__set_message("Enable application tracking", 
                               globals.get_baseurl() + "/account")        

        self.__fill_applications()
