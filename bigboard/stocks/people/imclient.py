import cgi
import dbus
import os
import gobject
import gnomevfs

from bigboard import empathy
from pyonlinedesktop.fsutil import VfsMonitor

_client = None

class Pidgin(gobject.GObject):
    def __init__(self):
        gobject.GObject.__init__(self)

        self.__accounts_config = os.path.expanduser('~/.purple/accounts.xml')
        VfsMonitor('file://' + self.__accounts_config, gnomevfs.MONITOR_FILE,
                   lambda: self.notify('configured'))

    def open_chat(self, buddy_id, protocol):
        # FIXME, is there some less hardcoded way to do this?
        os.spawnlp(os.P_NOWAIT, 'purple-remote', 'purple-remote',
                   '%s:goim?screenname=%s' % (protocol, cgi.escape(buddy_id)))

    def get_configured(self):
        return os.path.exists(self.__accounts_config)

    def configure(self):
        bus = dbus.SessionBus()

        try:
            obj = bus.get_object('im.pidgin.purple.PurpleService', '/org/freedesktop/od/im')
            im = dbus.Interface(obj, 'org.freedesktop.od.IMClient')
            im.Configure()
        except dbus.DBusException:
            os.spawnlp(os.P_NOWAIT, 'pidgin', 'pidgin')

    configured = gobject.property(type=object, getter=get_configured)

class Empathy(gobject.GObject):
    def __init__(self):
        gobject.GObject.__init__(self)

    def open_chat(self, buddy_id, protocol):
        empathy.open_chat_with(buddy_id, protocol)

    def get_configured(self):
        return empathy.is_configured()

    def configure(self):
        dialog = empathy.configure_accounts()
        dialog.connect('destroy', self.__on_configure_dialog_destroy)

    def __on_configure_dialog_destroy(self, widget):
        if self.configured:
            empathy.set_online(True)
            self.notify('configured')

    configured = gobject.property(type=object, getter=get_configured)

def get_default():
    global _client

    if _client == None:
        _client = Pidgin()
        #_client = Empathy()

    return _client
    
