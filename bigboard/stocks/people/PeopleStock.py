import os
import logging

import gtk
import hippo

import bigboard
from bigboard.people_tracker import PeopleTracker, sort_people
from bigboard.stock import AbstractMugshotStock, Stock
import bigboard.slideout
import bigboard.search as search
import bigboard.libbig as libbig
import bigboard.scroll_ribbon as scroll_ribbon

import imclient
import peoplebrowser
from peoplewidgets import PersonItem, ProfileItem

_logger = logging.getLogger("bigboard.stocks.PeopleStock")

class PeopleStock(AbstractMugshotStock):
    def __init__(self, *args, **kwargs):
        super(PeopleStock, self).__init__(*args, **kwargs)

        self.__box = hippo.CanvasBox(orientation=hippo.ORIENTATION_VERTICAL, spacing=3)

        self.__accounts_link = hippo.CanvasLink(text="Configure IM Accounts")
        self.__accounts_link.connect('activated', self.__on_accounts_link_activated)
        self.__box.append(self.__accounts_link)

        self.__scroll_box = scroll_ribbon.VerticalScrollArea()
        self.__scroll_box.set_increment(50)
        self.__box.append(self.__scroll_box, hippo.PACK_EXPAND)

        self.__person_box = hippo.CanvasBox(orientation=hippo.ORIENTATION_VERTICAL, spacing=3)
        self.__scroll_box.add(self.__person_box)

        self.__person_items = {}

        self.__slideout = None
        self.__current_slideout = None
        self.__last_slideout_event_time = None
        self.__in_slideout_close_event = False

        self.__people_browser = None
        self._add_more_button(self.__on_more_button)        
        
        self.__tracker = PeopleTracker()
        self.__tracker.people.connect("added", self.__on_person_added)
        self.__tracker.people.connect("removed", self.__on_person_removed)

        for person in self.__tracker.people:
            self.__on_person_added(self.__tracker.people, person)
            
        ## add a new search provider (FIXME never gets disabled)
        search.enable_search_provider('people',
                                      lambda: PeopleSearchProvider(self.__tracker, self))

        imclient.get_default().connect(
                'notify::configured', self.__on_imclient_configured_changed)
        self.__sync_accounts_message()

    def __sync_accounts_message(self):
        visible = not imclient.get_default().configured
        self.__box.set_child_visible(self.__accounts_link, visible)

    def __on_imclient_configured_changed(self, client, pspec):
        self.__sync_accounts_message()        

    def __on_accounts_link_activated(self, link):
        imclient.get_default().configure()

    def get_authed_content(self, size):
        return self.__box

    def __set_item_size(self, item, size):
        if size == bigboard.stock.SIZE_BULL:
            item.set_property('xalign', hippo.ALIGNMENT_FILL)
        else:
            item.set_property('xalign', hippo.ALIGNMENT_CENTER)
        
        item.set_size(size)

    def set_size(self, size):
        super(PeopleStock, self).set_size(size)
        for i in self.__person_items.values():
            self.__set_item_size(i, size)

    def __add_person(self, person, box, map):
        _logger.debug("person added to people stock %s", person.display_name)
        if map.has_key(person):
            return
        
        item = PersonItem(person)
        box.insert_sorted(item, hippo.PACK_IF_FITS, lambda a,b: sort_people(a.person, b.person))

        def resort(*args):
            box.remove(item)
            box.insert_sorted(item, hippo.PACK_IF_FITS, lambda a,b: sort_people(a.person, b.person))

        item.connect('sort-changed', resort)
        # A PersonItem might be pressed, but not activated, if an IM status portion
        # of it got pressed and resulted in an IM chat window being opened. So we have
        # to handle this event in two steps here.
        item.connect('button-press-event', self.__handle_item_pressed)
        item.connect('activated', self.__handle_item_activated)

        map[person] = item
        self.__set_item_size(item, self.get_size())

    def __remove_person(self, person, box, map):
        _logger.debug("person removed from people stock %s", person.display_name)
        try:
            item = map[person]
        except KeyError:
            return
        
        item.destroy()
        del map[person]    

    def __on_person_added(self, list, person):
        self.__add_person(person, self.__person_box, self.__person_items)
        
    def __on_person_removed(self, list, person):
        self.__remove_person(person, self.__person_box, self.__person_items)        
        
    def __close_slideout(self, object=None, action_taken=False):
        self.__last_slideout_event_time = gtk.get_current_event_time()
        if self.__slideout:
            if action_taken:
                self._panel.action_taken()
            self.__slideout.destroy()
            self.__slideout = None
            self._set_active(False)
    
    def __handle_item_pressed(self, item, event):
        self.__in_slideout_close_event = self.__last_slideout_event_time == gtk.get_current_event_time()
        
    def __slideout_item(self, item, y_fixup=0):
        # y_fixup is a number to be added to the the current position of the item
        # to determine where it will be after asynchronous scrolling occurs
        same_item = self.__current_slideout == item
        if same_item and self.__in_slideout_close_event:
            self.__current_slideout = None  
            self.__in_slideout_close_event = False
            return True

        self.__in_slideout_close_event = False

        self._set_active(True)
        self.__slideout = bigboard.slideout.Slideout()
        self.__current_slideout = item
        item_x, item_y = item.get_screen_coords()
        
        coords = item.get_screen_coords()

        p = ProfileItem(item.get_person())
        
        self.__slideout.get_root().append(p)
        p.connect("close", self.__close_slideout)
        self.__slideout.connect("close", self.__close_slideout)
        try:
            success = False
            success = self.__slideout.slideout_from(Stock.SIZE_BULL_CONTENT_PX, item_y + y_fixup)
#            success = self.__slideout.slideout_from(item_x + item.get_allocation()[0] + 4, item_y + y_fixup)
        finally:
            if not success:
                self.__close_slideout()

        return success

    def __handle_item_activated(self, item):
        self.__slideout_item(item)

    def slideout_person(self, person):
        try:
            item = self.__person_items[person]
        except KeyError:
            return

        # Compute the position of the item relative to it's parent item; a bit
        # hacky, but I can't think of an easier way to do it
        _, box_y = self.__person_box.get_context().translate_to_widget(self.__person_box)
        _, item_y = item.get_context().translate_to_widget(item)
        box_relative_y = item_y - box_y

        # Now scroll the scroll ribbon so that the item is visible; since this
        # is asynchronous, we get a "fixup" indicating the number of pixels that
        # the item will eventually move
        _, item_height = item.get_allocation()
        y_fixup = self.__scroll_box.scroll_range_visible(box_relative_y, item_height)
        
        self.__slideout_item(item, y_fixup)

    def __on_more_button(self):
        if self.__people_browser is None:
            self.__people_browser = peoplebrowser.PeopleBrowser(self)
        if self.__people_browser.get_property('is-active'):
            self.__people_browser.hide()
        else:
            self.__people_browser.present()

    def on_popped_out_changed(self, popped_out):
        if not popped_out:
            self.__close_slideout()

class PeopleSearchResult(search.SearchResult):
    def __init__(self, provider, person):
        super(PeopleSearchResult, self).__init__(provider)
        self.__person = person

    def get_title(self):
        return self.__person.display_name

    def get_detail(self):
        return self.__person.display_name

    def get_icon(self):
        """Returns an icon for the result"""
        return None

    def get_icon_url(self):
        return self.__person.icon_url

    def _on_highlighted(self):
        """Action when user has highlighted the result"""
        pass

    def _on_activated(self):
        """Action when user has activated the result"""
        self.get_provider().get_stock().slideout_person(self.__person)

class PeopleSearchProvider(search.SearchProvider):    
    def __init__(self, tracker, stock):
        super(PeopleSearchProvider, self).__init__()
        self.__tracker = tracker
        self.__stock = stock

    def get_heading(self):
        return "People"

    def perform_search(self, query, consumer):
        query = query.lower()
        
        results = []
        
        for p in self.__tracker.people:
            #_logger.debug("person: " + str(p))

            matched = False
            if query in p.display_name.lower():
                matched = True

            for email in p.emails:
                if query in email.lower():
                    matched = True
                    break
                
            for aim in p.aims:
                if query in aim.lower():
                    matched = True
                    break
                
            for xmpp in p.xmpps:
                if query in xmpp.lower():
                    matched = True
                    break

            if matched:
                results.append(PeopleSearchResult(self, p))

        if len(results) > 0:
            consumer.add_results(results)

    def get_stock(self):
        return self.__stock
        
