import os
import time
import logging

import gobject
import gtk
import hippo

import bigboard
from bigboard.databound import DataBoundItem
from bigboard.big_widgets import ActionLink, CanvasMugshotURLImage, Header
from bigboard.big_widgets import PhotoContentItem, CanvasHBox, CanvasVBox
from bigboard.big_widgets import BigWindow
import bigboard.libbig as libbig
from bigboard.libbig.logutil import log_except
import bigboard.globals as globals
from bigboard.libbig.format_escaped import format_escaped

from ddm import DataModel

import imclient

_logger = logging.getLogger("bigboard.stocks.PeopleStock")

STATUS_MUSIC = 0
STATUS_IM = 1

def _open_webdav(url):
    # We pass around WebDAV URL's using the standard http:// scheme, but nautilus wants dav://
    # instead.

    if url.startswith("http:"):
        url = "dav:" + url[5:]
    
    os.spawnlp(os.P_NOWAIT, 'gnome-open', 'gnome-open', url)

class StatusMessage(hippo.CanvasText):
    def __init__(self):
        hippo.CanvasText.__init__(self, classes='subforeground')
        self.__buddies = []

        self.connect('destroy', self.__on_destroy)

    def __on_destroy(self, canvas_item):
        for b in self.__buddies:
            b.disconnect(self.__on_buddy_changed)

    def set_buddies(self, buddies):
        for b in self.__buddies:
            b.disconnect(self.__on_buddy_changed)

        self.__buddies = buddies
        
        for b in self.__buddies:
            b.connect(self.__on_buddy_changed, 'statusMessage')
            self.__on_buddy_changed(b)

    def __on_buddy_changed(self, buddy):
        message = None
        try:
            message = buddy.statusMessage
        except AttributeError:
            pass
        
        if message:
            message = message.strip()
            if message != '':
                self.set_property('text', message)

class PersonItem(PhotoContentItem):
    __gsignals__ = {
        'sort-changed': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        'destroy': 'override',
    }

    @log_except(_logger)
    def __init__(self, person, **kwargs):
        _logger.debug("Initializing %s", self)

        PhotoContentItem.__init__(self, **kwargs)
        self.person = person
        
        model = DataModel(bigboard.globals.server_name)

        self.set_clickable(True)
        
        self.__photo = CanvasMugshotURLImage(scale_width=45,
                                            scale_height=45,
                                            border=1,
                                            border_color=0x000000ff)

        self.set_photo(self.__photo)

        self.__details_box = CanvasVBox()
        self.set_child(self.__details_box)

        self.__name = hippo.CanvasText(xalign=hippo.ALIGNMENT_START, yalign=hippo.ALIGNMENT_START,
                                       size_mode=hippo.CANVAS_SIZE_ELLIPSIZE_END)
        self.__details_box.append(self.__name)

        self.__presence_box = CanvasHBox(spacing=4)
        self.__details_box.append(self.__presence_box)
        
        self.__statuses = []
        self.__status_box = CanvasHBox()
        self.__details_box.append(self.__status_box)

        self.connect('button-press-event', self.__handle_button_press)
        self.connect('button-release-event', self.__handle_button_release)
        self.__pressed = False

        self.__aim_icon = None
        self.__xmpp_icon = None

        self.__current_track = None
        self.__current_track_timeout = None

        user = self.person.user
        if user:
            query = model.query_resource(user, "currentTrack +;currentTrackPlayTime")
            query.execute()

            user.connect(self.__update_current_track, 'currentTrack')
            user.connect(self.__update_current_track, 'currentTrackPlayTime')
            self.__update_current_track(user)

        self.__handlers = []

        self.__handlers.append(self.person.connect('status-changed', self.__emit_sort_changed))
        self.__handlers.append(self.person.connect('online-changed', self.__emit_sort_changed))
        self.__handlers.append(self.person.connect('display-name-changed', self.__emit_sort_changed))
            
        self.__handlers.append(self.person.connect('display-name-changed', self.__update))
        self.__handlers.append(self.person.connect('icon-url-changed', self.__update))
        
        self.__handlers.append(self.person.connect('aim-buddies-changed', self.__update_aim_buddy))
        self.__handlers.append(self.person.connect('xmpp-buddies-changed', self.__update_xmpp_buddy))
        
        self.__update(self.person)
        self.__update_aim_buddy(self.person)
        self.__update_xmpp_buddy(self.person)

        _logger.debug("Initialized %s", self)
        
    def do_destroy(self):
        _logger.debug("Destroying %s", self)
        for handler_id in self.__handlers:
            self.person.disconnect(handler_id)

    def __update_color(self):
        if self.__pressed:
            self.set_property('classes', 'person-item-pressed')
        else:
            self.sync_prelight()

    def __handle_button_press(self, self2, event):
        if event.button != 1 or event.count != 1:
            return False
        
        self.__pressed = True
        self.__update_color()

        return False

    def __handle_button_release(self, self2, event):
        if event.button != 1:
            return False

        self.__pressed = False
        self.__update_color()

        return False

    def get_person(self):
        return self.person

    def set_size(self, size):
        if size == bigboard.stock.SIZE_BULL:
            self.set_child_visible(self.__details_box, True)
            self.__photo.set_property('xalign', hippo.ALIGNMENT_START)
            self.__photo.set_property('yalign', hippo.ALIGNMENT_START)
        else:
            self.set_child_visible(self.__details_box, False)
            self.__photo.set_property('xalign', hippo.ALIGNMENT_CENTER)
            self.__photo.set_property('yalign', hippo.ALIGNMENT_CENTER)

    def __update(self, person):
        self.__name.set_property("text", self.person.display_name) #+ " " + str(self.person._debug_rank))
        if self.person.icon_url:
            self.__photo.set_url(self.person.icon_url)
        else:
            self.__photo.set_property('image-name', 'bigboard-nophoto-45')

    def __reset_im_status(self):
        buddies = self.person.aim_buddies + self.person.xmpp_buddies
        if len(buddies) > 0:
            sm = StatusMessage()
            sm.set_buddies(buddies)
            self.__set_status(STATUS_IM, sm)

    def __update_aim_buddy(self, person):
        if len(person.aim_buddies) > 0:
            if not self.__aim_icon:
                self.__aim_icon = AimIcon(person.aim_buddies[0])
                self.__presence_box.append(self.__aim_icon)
        else:
            if self.__aim_icon:
                self.__aim_icon.destroy()
                self.__aim_icon = None        

        self.__reset_im_status()

    def __update_xmpp_buddy(self, person):
        if len(person.xmpp_buddies) > 0:
            if not self.__xmpp_icon:
                self.__xmpp_icon = XMPPIcon(person.xmpp_buddies[0])
                self.__presence_box.append(self.__xmpp_icon)
        else:
            if self.__xmpp_icon:
                self.__xmpp_icon.destroy()
                self.__xmpp_icon = None

        self.__reset_im_status()

    def __timeout_track(self):
        self.__current_track_timeout = None
        self.__update_current_track(self.person.user)
        return False

    def __update_current_track(self, user):
        try:
            current_track = user.currentTrack
            current_track_play_time = user.currentTrackPlayTime / 1000.
        except AttributeError:
            current_track = None
            current_track_play_time = -1

        # current_track_play_time < 0, current_track != None might indicate stale
        # current_track data
        if current_track_play_time < 0:
            current_track = None

        if current_track != None:
            now = time.time()
            if current_track.duration < 0:
                endTime = current_track_play_time + 30 * 60   # Half hour
            else:
                endTime = current_track_play_time + current_track.duration / 1000. # msec => sec

            if now >= endTime:
               current_track = None

        if current_track != self.__current_track:
            self.__current_track = current_track
            
            if self.__current_track_timeout:
                gobject.source_remove(self.__current_track_timeout)

            if current_track != None:
                # We give 30 seconds of lee-way, so that the track is pretty reliably really stopped
                self.__current_track_timeout = gobject.timeout_add(int((endTime + 30 - now) * 1000), self.__timeout_track)

            if current_track != None:
                self.__set_status(STATUS_MUSIC, TrackItem(current_track))
            else:
                self.__set_status(STATUS_MUSIC, None)
            
    def __set_status(self, type, contents):
        if len(self.__statuses) > 0:
            old_contents = self.__statuses[0][1]
        else:
            old_contents = None
        
        for i in range(0,len(self.__statuses)):
            (i_type,i_contents) = self.__statuses[i]
            if i_type == type:
                i_contents.destroy()
                del self.__statuses[i]
                if i == 0:
                    old_contents = None
                break

        if old_contents != None:
            old_contents.set_visible(False)
        
        if contents != None:
            self.__statuses.insert(0, (type, contents))
            self.__status_box.append(contents)
            
        if len(self.__statuses) > 0:
            new_contents = self.__statuses[0][1]
            new_contents.set_visible(True)

    def __emit_sort_changed(self, *args):
        self.emit('sort-changed')

    def get_screen_coords(self):
        return self.get_context().translate_to_screen(self)

class ExternalAccountIcon(CanvasHBox):
    def __init__(self, acct):
        super(ExternalAccountIcon, self).__init__(box_width=16, box_height=16)
        self.__acct = None
        self.__img = CanvasMugshotURLImage()
        self.append(self.__img)
        self.connect("activated", lambda s2: self.__launch_browser())
        self.set_clickable(True)
        self.set_acct(acct)
        
    def set_acct(self, acct):
        if self.__acct:
            self.__acct.disconnect(self.__sync)
        self.__acct = acct
        self.__acct.connect(self.__sync)
        self.__sync()
         
    def __sync(self, *args):
        self.__img.set_url(self.__acct.iconUrl)
        
    def __launch_browser(self):
        libbig.show_url(self.__acct.link)

class IMIcon(hippo.CanvasLink, DataBoundItem):
    def __init__(self, buddy):
        hippo.CanvasLink.__init__(self)
        DataBoundItem.__init__(self, buddy)
        
        self.connect("activated", self.__on_activated)

        self.connect_resource(self.__update)
        self.__update(self.resource)

    def _get_protocol_name(self):
        raise Exception("implement me")

    def _start_conversation(self):
        raise Exception("implement me")

    def __update(self, buddy):
        text = "%s (%s)" % (self._get_protocol_name(), buddy.status)
        self.set_property("text", text)
        self.set_property("tooltip", "Chat with %s (%s)" % (buddy.name, buddy.status,))
        self.set_clickable(buddy.isOnline)

    def __on_activated(self, object):
        self._start_conversation()


class AimIcon(IMIcon):
    def __init__(self, buddy, **kwargs):
        IMIcon.__init__(self, buddy, **kwargs)
        
    def _get_protocol_name(self):
        return "AIM"

    def _start_conversation(self):
        imclient.get_default().open_chat(self.resource.name, 'aim')
        return True

class XMPPIcon(IMIcon):
    def __init__(self, buddy, **kwargs):
        IMIcon.__init__(self, buddy, **kwargs)
        
    def _get_protocol_name(self):
        if 'gmail.com' in self.resource.name:
            return "GTalk"
        else:
            return "XMPP"

    def _start_conversation(self):
        imclient.get_default().open_chat(self.resource.name, 'jabber')
        return True


#
# The CanvasText item thinks that you always want the full text as the tooltip when
# ellipsized, but we are doing our own "tooltip" that is cooler and contains album art
#
class NoTooltipText(hippo.CanvasText, hippo.CanvasItem):
    def __init__(self, **kwargs):
        hippo.CanvasText.__init__(self, **kwargs)

    def do_get_tooltip(self,x,y,for_area):
        return ""

gobject.type_register(NoTooltipText)
    
class TrackItem(hippo.CanvasBox, DataBoundItem):
    __gsignals__ = {
        'motion-notify-event': 'override'
       }
        
    def __init__(self, track):
        hippo.CanvasBox.__init__(self, orientation=hippo.ORIENTATION_HORIZONTAL)
        DataBoundItem.__init__(self, track)

        image = hippo.CanvasImage(yalign=hippo.ALIGNMENT_START,
                                  image_name="bigboard-music",
                                  border_right=6)
        self.append(image)
        
        details = CanvasVBox()
        self.append(details)

        artist_text = NoTooltipText(text=track.artist,
                                    id="track-artist-text",
                                    classes="subforeground",
                                    xalign=hippo.ALIGNMENT_START,
                                    size_mode=hippo.CANVAS_SIZE_ELLIPSIZE_END)
        details.append(artist_text)

        title_text = NoTooltipText(text=track.name,
                                   id="track-artist-title",
                                   classes="subforeground",
                                   xalign=hippo.ALIGNMENT_START,
                                   size_mode=hippo.CANVAS_SIZE_ELLIPSIZE_END)
        details.append(title_text)

        self.__tooltip_timeout = None
        self.__tooltip_popup = None
        
    def do_motion_notify_event(self, event):
        if event.detail == hippo.MOTION_DETAIL_ENTER:
            if self.__tooltip_timeout == None:
                try:
                    tooltip_time = gtk.settings_get_default().get_property("gtk-tooltip-timeout")
                except TypeError: # Old version of GTK+
                    tooltip_time = 500
                    
                self.__tooltip_timeout = gobject.timeout_add(tooltip_time, self.__on_tooltip_timeout)
        elif event.detail == hippo.MOTION_DETAIL_LEAVE:
            if self.__tooltip_timeout != None:
                gobject.source_remove(self.__tooltip_timeout)
                self.__tooltip_timeout = None
            if self.__tooltip_popup != None:
                self.__tooltip_popup.destroy()
                self.__tooltip_popup = None

    def __on_tooltip_timeout(self):
        self.__tooltip_timeout = None

        if self.__tooltip_popup == None:
            self.__tooltip_popup = TrackPopup(self.resource)

        self.__tooltip_popup.popup()
        
class TrackPopup(BigWindow, DataBoundItem):
    __gsignals__ = {
        'motion-notify-event': 'override'
       }
        
    def __init__(self, track):
        stylesheet = os.path.join(os.path.dirname(__file__), 'trackpopup.css')

        BigWindow.__init__(self, type=gtk.WINDOW_POPUP, stylesheet=stylesheet)
        DataBoundItem.__init__(self, track)

        self.modify_bg(gtk.STATE_NORMAL, gtk.gdk.Color(0xffff,0xffff,0xffff))

        root = CanvasHBox(border=1, border_color=0x000000ff)
        self.set_root(root)
        
        box = CanvasHBox(border=5)
        root.append(box)
        
        image = CanvasMugshotURLImage(box_width=track.imageWidth, box_height=track.imageHeight)
        image.set_url(track.imageUrl)
        box.append(image)
        
        details = CanvasVBox(border_left=6)
        box.append(details)

        artist_text = hippo.CanvasText(text=track.artist,
                                       id="artist-text",
                                       xalign=hippo.ALIGNMENT_START)
        details.append(artist_text)

        title_text = hippo.CanvasText(text=track.name,
                                      id="title-text",
                                      xalign=hippo.ALIGNMENT_START)
        details.append(title_text)

    def popup(self):
        (x,y,_) = gtk.gdk.get_default_root_window().get_pointer()
        self.move(x + 10, y + 10)
        self.show()
        
class LocalFilesLink(hippo.CanvasBox, DataBoundItem):
    def __init__(self, buddy):
        hippo.CanvasBox.__init__(self)
        DataBoundItem.__init__(self, buddy)

        self.__link = ActionLink(text="Shared Files")
        self.__link.connect("activated", self.__open_webdav)
        self.append(self.__link)

        self.connect_resource(self.__update)
        self.__update(self.resource)

    def __get_url(self):
        try:
            return self.resource.webdavUrl
        except AttributeError:
            return None

    def __update(self, buddy):
        self.__link.set_visible(self.__get_url() != None)
        
    def __open_webdav(self, object):
        _open_webdav(self.__get_url())

class ProfileItem(hippo.CanvasBox):
    __gsignals__ = {
        "close": (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (bool,))
       }
        
    def __init__(self, person, **kwargs):
        kwargs['orientation'] = hippo.ORIENTATION_VERTICAL
        hippo.CanvasBox.__init__(self, **kwargs)
        self.person = person

        self.__header = hippo.CanvasBox(orientation=hippo.ORIENTATION_HORIZONTAL, classes="slideout-header")
        self.append(self.__header)

        main_box = hippo.CanvasBox(classes="slideout-main")
        self.append(main_box)
        
        name_vbox = hippo.CanvasBox(padding=6)
        self.__name = hippo.CanvasText(id='profile-name')
        name_vbox.append(self.__name)
        rename_link = ActionLink(text='rename', id="profile-rename-link", xalign=hippo.ALIGNMENT_END)
        name_vbox.append(rename_link)

        rename_link.connect('activated', self.__on_rename_activated)

        self.__header.append(name_vbox)

        if self.person.home_url:
            if self.person.home_url.find("mugshot.org") >= 0:
                web_link = ActionLink(text="Mugshot", padding=6)
            else:
                web_link = ActionLink(text="Web", padding=6)
            self.__header.append(web_link, flags=hippo.PACK_END)
            web_link.connect("activated", self.__on_activate_web)

        self.__top_box = hippo.CanvasBox(orientation=hippo.ORIENTATION_HORIZONTAL)
        main_box.append(self.__top_box)

        self.__photo = CanvasMugshotURLImage(scale_width=60,
                                            scale_height=60,
                                            border=5)

        if self.person.home_url:
            self.__photo.set_clickable(True)
            self.__photo.connect("activated", self.__on_activate_web)

        self.__top_box.append(self.__photo)

        self.__address_box = hippo.CanvasBox(orientation=hippo.ORIENTATION_VERTICAL)
        self.__top_box.append(self.__address_box)

        self.__contact_status_box = hippo.CanvasBox(orientation=hippo.ORIENTATION_HORIZONTAL,
                                                    spacing=4, border=4)
        main_box.append(self.__contact_status_box)

        if person.contact:
            self.__remove_link = ActionLink()
            self.__remove_link.connect('activated', self.__remove_from_network_clicked)
            main_box.append(self.__remove_link)
        else:
            self.__remove_link = None
        
#        self.__online = hippo.CanvasText(text='Offline')
#        main_box.append(self.__online)

        separator = hippo.CanvasBox(box_height=1, background_color=0xAAAAAAFF)
        main_box.append(separator)

        self.__ribbon_bar = hippo.CanvasBox(orientation=hippo.ORIENTATION_HORIZONTAL,
                                           spacing=2, border=4)
        main_box.append(self.__ribbon_bar)

        self.__link_box = hippo.CanvasBox(orientation=hippo.ORIENTATION_HORIZONTAL,
                                          spacing=2, border=4)
        main_box.append(self.__link_box)
        
        self.__local_files_link = None

        self.person.connect('display-name-changed', self.__update)
        self.person.connect('icon-url-changed', self.__update)
        self.person.connect('aims-changed', self.__update)
        self.person.connect('local-buddies-changed', self.__update_local_buddy)
        self.person.connect('xmpps-changed', self.__update)
        self.person.connect('emails-changed', self.__update)
        self.person.connect('status-changed', self.__update_contact_status)

        # FIXME - self.person.user can appear and go away
        if self.person.user:
            self.person.user.connect(self.__update_loved_accounts, "lovedAccounts")

            query = DataModel(bigboard.globals.server_name).query_resource(self.person.user, "lovedAccounts +")
            query.add_handler(self.__update_loved_accounts)
            query.execute()

        self.__update(self.person)
        self.__update_local_buddy(self.person)
        self.__update_contact_status(self.person)

        if self.person.user:
            self.__update_loved_accounts(self.person.user)

    def __add_status_link(self, text, current_status, new_status):
        if current_status == new_status:
            link = hippo.CanvasText(text=text)
        else:
            link = ActionLink(text=text)
            link.connect("activated", self.__set_new_status, new_status)
        
        self.__contact_status_box.append(link)

    def __set_new_status(self, link, new_status):
        def do_set_status(contact):
            model = globals.get_data_model()
            query = model.update(("http://mugshot.org/p/contacts", "setContactStatus"),
                                 contact=contact,
                                 status=new_status)
            query.execute()

        if self.person.contact:
            do_set_status(self.person.contact)
        else:
            self.__add_to_network(do_set_status)
        
    def __remove_from_network_clicked(self, link):

        dialog = gtk.MessageDialog(type=gtk.MESSAGE_QUESTION)
        dialog.set_markup("<b>Remove %s from your network?</b>" % (self.person.display_name))
        dialog.format_secondary_text("This will delete %s's contact information and remove %s from your sidebar" % (self.person.display_name, self.person.display_name))
        dialog.add_buttons("Cancel", gtk.RESPONSE_CANCEL, "Remove", gtk.RESPONSE_ACCEPT)

        def remove_from_network_response(dialog, response_id, person):
            dialog.destroy()

            if response_id == gtk.RESPONSE_ACCEPT:
                _logger.debug("removing from network")

                model = globals.get_data_model()
                query = model.update(("http://mugshot.org/p/contacts", "deleteContact"),
                                     contact=person.contact)
                query.execute()

            else:
                _logger.debug("not removing from network")

        dialog.connect("response", lambda dialog, response_id: remove_from_network_response(dialog, response_id, self.person))                

        # action_taken = False to leave the stock open which seems nicer in this case
        self.emit("close", False)

        dialog.show()

    def __on_rename_activated(self, link):
        dialog = gtk.Dialog(title="Rename a contact")
        
        entry = gtk.Entry()
        entry.set_text(self.person.display_name)
        entry.set_activates_default(True)

        hbox = gtk.HBox(spacing=10)
        hbox.pack_start(gtk.Label('Name:'), False, False)
        hbox.pack_end(entry, True, True)
        
        hbox.show_all()

        dialog.vbox.pack_start(hbox)

        dialog.add_buttons("Cancel", gtk.RESPONSE_CANCEL, "Rename", gtk.RESPONSE_ACCEPT)
        dialog.set_default_response(gtk.RESPONSE_ACCEPT)

        def rename_response(dialog, response_id, person):
            dialog.destroy()

            if response_id == gtk.RESPONSE_ACCEPT:
                _logger.debug("renaming this person")

                name = entry.get_text()

                model = globals.get_data_model()
                query = model.update(("http://mugshot.org/p/contacts", "setContactName"),
                                     contact=person.contact, name=name)
                query.execute()

            else:
                _logger.debug("not renaming")

        dialog.connect("response", lambda dialog, response_id: rename_response(dialog, response_id, self.person))                

        # action_taken = False to leave the stock open which seems nicer in this case
        self.emit("close", False)

        dialog.show()

    def __add_dialog_error_handler(self, query, message):
        def on_error(err_type, err_message):
            dialog = gtk.MessageDialog(type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_OK)
            dialog.set_markup(format_escaped("<b>%s</b>", message))
            dialog.format_secondary_text(format_escaped("%s", err_message))
            dialog.run()
            dialog.destroy()

        query.add_error_handler(on_error)

    def __create_contact(self, addressType, address, after_add, after_add_args):
        def on_success(contact):
            _logger.debug("Succesfully created contact %s", contact.resource_id)
            if after_add:
                after_add(contact, *after_add_args)
        
        _logger.debug("creating contact %s %s" % (addressType, address))
        
        model = globals.get_data_model()
        query = model.update(("http://mugshot.org/p/contacts", "createContact"),
                             fetch="+",
                             single_result=True,
                             addressType=addressType,
                             address=address)
        query.add_handler(on_success)
        self.__add_dialog_error_handler(query, "Couldn't add %s to your contact list" % (address,))
        query.execute()

    def __create_user_contact(self, user_resource, after_add, after_add_args):
        def on_success(contact):
            _logger.debug("Succesfully created contact %s", contact.resource_id)
            if after_add:
                after_add(contact, *after_add_args)
            
        _logger.debug("creating contact %s" % (str(user_resource)))
        
        model = globals.get_data_model()
        query = model.update(("http://mugshot.org/p/contacts", "createUserContact"),
                             fetch="+",
                             single_result=True,
                             user=user_resource);
        query.add_handler(on_success)
        self.__add_dialog_error_handler(query, "Couldn't add %s to your contact list" % (user_resource.name,))
        query.execute()

    def __add_to_network(self, after_add=None, *after_add_args):
        if self.person.aims:
            self.__create_contact('aim', self.person.aims[0], after_add, after_add_args)
        elif self.person.xmpps:
            self.__create_contact('xmpp', self.person.xmpps[0], after_add, after_add_args)
        elif self.person.local_buddies:
            self.__create_user_contact(self.person.local_buddies[0].user, after_add, after_add_args)
        
    def __update_contact_status(self, person):
        self.__contact_status_box.remove_all()
        
        status = person.status
        if status == 0:
            status = 3 

        self.__contact_status_box.append(hippo.CanvasText(text="In sidebar: "))
        
        self.__add_status_link("Top", status, 4)
        self.__add_status_link("Middle", status, 3)
        self.__add_status_link("Bottom", status, 2)

    def __update_loved_accounts(self, person):
        try:
            accounts = self.person.user.lovedAccounts
        except AttributeError:
            accounts = []
        
        self.__ribbon_bar.clear()
        for a in accounts:
            icon = ExternalAccountIcon(a)
            self.__ribbon_bar.append(icon)

    def __update_local_buddy(self, person):
        if self.__local_files_link:
            self.__local_files_link.destroy()
            self.__local_files_link = None

        if len(person.local_buddies) > 0:
            buddy = person.local_buddies[0]
            
            self.__local_files_link = LocalFilesLink(buddy)
            self.__link_box.append(self.__local_files_link)

    def __update(self, person):
        self.__name.set_property('text', self.person.display_name)

        if self.person.icon_url:
            self.__photo.set_url(self.person.icon_url)
        else:
            self.__photo.set_property('image-name', 'bigboard-nophoto-60')

        self.__address_box.remove_all()

        if self.__remove_link:
            self.__remove_link.set_property('text', 
                                            "Remove %s from network" % self.person.display_name)

        for addr in person.emails:
            email = ActionLink(text=addr, xalign=hippo.ALIGNMENT_START)
            email.connect('activated', self.__on_activate_email, addr)
            self.__address_box.append(email)

        for addr in person.aims:
            aim = ActionLink(text=addr, xalign=hippo.ALIGNMENT_START)
            aim.connect('activated', self.__on_activate_aim, addr)
            self.__address_box.append(aim)

        for addr in person.xmpps:
            xmpp = ActionLink(text=addr, xalign=hippo.ALIGNMENT_START)
            xmpp.connect('activated', self.__on_activate_xmpp, addr)
            self.__address_box.append(xmpp)

        add = ActionLink(text='add address', xalign=hippo.ALIGNMENT_END, font_scale=0.8)
        add.connect('activated', self.__on_activate_add_address)
        self.__address_box.append(add)

    def __on_activate_web(self, canvas_item):
        self.emit("close", True)
        if self.person.home_url:
            libbig.show_url(self.person.home_url)

    def __on_activate_email(self, canvas_item, addr):
        self.emit("close", True)
        # email should probably cgi.escape except it breaks if you escape the @
        os.spawnlp(os.P_NOWAIT, 'gnome-open', 'gnome-open', 'mailto:' + addr)

    def __on_activate_aim(self, canvas_item, addr):
        self.emit("close", True)
        imclient.get_default().open_chat(self.resource.name, 'aim')
        
    def __on_activate_xmpp(self, canvas_item, addr):
        self.emit("close", True)
        imclient.get_default().open_chat(self.resource.name, 'jabber')

    def __on_activate_add_address(self, canvas_item):
        dialog = gtk.Dialog(title=("Add an address for %s" % self.person.display_name))
        
        entry = gtk.Entry()
        entry.set_activates_default(True)

        hbox = gtk.HBox(spacing=10)
        hbox.pack_start(gtk.Label('Address:'), False, False)
        hbox.pack_end(entry, True, True)
        
        hbox.show_all()

        dialog.vbox.pack_start(hbox)

        type_combo = gtk.combo_box_new_text()
        type_combo.append_text('AIM')
        type_combo.append_text('Email')
        type_combo.append_text('GTalk/XMPP')
        type_combo.set_active(0)

        hbox = gtk.HBox(spacing=10)
        hbox.pack_start(gtk.Label('Type:'), False, False)
        hbox.pack_end(type_combo, True, True)
        
        hbox.show_all()

        dialog.vbox.pack_start(hbox)

        dialog.add_buttons("Cancel", gtk.RESPONSE_CANCEL, "Add", gtk.RESPONSE_ACCEPT)
        dialog.set_default_response(gtk.RESPONSE_ACCEPT)


        def combo_get_address_type(combo):
            visible_type = type_combo.get_active_text()
            addressType = None
            if visible_type == 'Email':
                addressType = 'email'
            elif visible_type == 'AIM':
                addressType = 'aim'
            elif visible_type == 'GTalk/XMPP':
                addressType = 'xmpp'
            else:
                _logger.warn('Bug: unknown combox box text for address type')
                if '@' in entry.get_text():
                    addressType = 'email'
                else:
                    addressType = 'aim'

            return addressType

        def address_entry_changed(entry):
            address = entry.get_text()
            type = combo_get_address_type(type_combo)
            
            if '@' in address and type == 'aim':
                type_combo.set_active(1) ## set to email if an @ is typed
            elif '@' not in address and type == 'email':
                type_combo.set_active(0) ## set to AIM if no @ is found

            ## remember that @ can mean either email or xmpp

        entry.connect('changed', address_entry_changed)

        def add_address_response(dialog, response_id, person):
            dialog.destroy()

            if response_id == gtk.RESPONSE_ACCEPT:
                _logger.debug("adding address for this person")

                address = entry.get_text()
                address_type = combo_get_address_type(type_combo)

                def do_add_address(contact):
                    model = globals.get_data_model()
                    query = model.update(("http://mugshot.org/p/contacts", "addContactAddress"),
                                         contact=contact, addressType=address_type, address=address)
                    self.__add_dialog_error_handler(query, "Couldn't add address %s to contact" % (address,))
                    query.execute()

                if person.contact:
                    do_add_address(person.contact)
                else:
                    self.__add_to_network(do_add_address)
            else:
                _logger.debug("not adding_address")

        dialog.connect("response", lambda dialog, response_id: add_address_response(dialog, response_id, self.person))

        # action_taken = False to leave the stock open which seems nicer in this case
        self.emit("close", False)

        dialog.show()
