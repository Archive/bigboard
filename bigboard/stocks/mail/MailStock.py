#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging, re, time, urllib, urllib2

import gobject, gtk
import hippo
import xml.dom.minidom
from xml.dom.minidom import Node
from StringIO import StringIO

from bigboard.stock import Stock
from bigboard.slideout import Slideout
import bigboard.google as google
import bigboard.google_stock as google_stock  
from bigboard.big_widgets import ActionLink, CanvasHBox, CanvasVBox, Button, Header, PrelightingCanvasBox, Arrow
import bigboard.libbig as libbig
from bigboard.libbig.struct import AutoStruct, AutoSignallingStruct
from bigboard.libbig.logutil import log_except

#TODO: add a scrollable view for emails
#import bigboard.scroll_ribbon as scroll_ribbon

import bigboard.libgmail.libgmail as libgmail

_logger = logging.getLogger('bigboard.stocks.MailStock')

def replace_chr(m):
    return unichr(int(m.group(1), 16))
UNICHR_REPLACE = re.compile(r"\\u([A-F-a-f0-9]{4})")
def gmail_jshtml_str_parse(s, markup=False):
    # Replace \uxxxx escapes
    parsed_str = UNICHR_REPLACE.sub(replace_chr, s)
    # At this point, we have a Python unicode string which *should* hold
    # an HTML fragment.  Convert that fragment into a document string.
    pystr = "<html>" + parsed_str + "</html>"
    # Now use BeautifulSoup to parse it
    from BeautifulSoup import BeautifulSoup
    soup = BeautifulSoup(pystr, convertEntities=BeautifulSoup.XHTML_ENTITIES)
    textContent = StringIO()    
    def filterBoldOnly(node):       
        if isinstance(node, unicode):
            if markup:
                text = gobject.markup_escape_text(node)
            else:
                text = node
            textContent.write(text)
            return
        if markup and node.name == 'b':
            in_bold = True
            textContent.write('<b>')
        else:
            in_bold = False
        for child in node.childGenerator():
            filterBoldOnly(child)
        if in_bold:
            textContent.write('</b>')
    filterBoldOnly(soup)
    # Return the sanely filtered content
    return textContent.getvalue()

def create_account_url(account):
    account = urllib.unquote(account)
    domain = account[account.find("@") + 1:]
    if domain == "gmail.com":
        return "http://mail.google.com/mail"
    else:
        return "https://mail.google.com/a/" + domain

def on_visit_mail_account(widget, account_name):
    libbig.show_url(create_account_url(account_name))    

class GoogleAccountInfo(AutoStruct):
    def __init__(self):
        super(GoogleAccountInfo, self).__init__({ 'google_account' : None, \
                                                  'google_account_box' : hippo.CanvasBox(orientation=hippo.ORIENTATION_VERTICAL), \
                                                  'current_folder' : 'inbox', \
                                                  'logged_in_flag' : False, \
                                                  'expand_arrow' : hippo.CanvasImage(yalign=hippo.ALIGNMENT_CENTER, padding=2), \
                                                  'expand_arrow_signal_id' : None})
        self.get_expand_arrow().set_clickable(True)
        self.get_expand_arrow().set_property('image-name', 'bigboard-up-arrow-enabled') 

    def __str__(self):
        if self.get_google_account():
            return "Google Account Username: " + str(self.get_google_account().name) + " Domain: " + str(self.get_google_account().domain) + " Current folder: " + self.get_current_folder() + " Logged in: " + str(self.get_logged_in_flag()) + " Arrow state: " + self.get_expand_arrow().get_property("image_name")
        else: 
            return "Google Account: None Current folder: " + self.get_current_folder() + " Logged in: " + str(self.get_logged_in_flag()) + " Arrow state: " + self.get_expand_arrow().get_property("image_name")

class LabelSlideout(Slideout):
    __gsignals__ = {
                    'changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_STRING, )),
                   }
    def __init__(self, ga):
        super(LabelSlideout, self).__init__()
        header = hippo.CanvasBox(orientation=hippo.ORIENTATION_HORIZONTAL, classes="slideout-header")
        self.get_root().append(header)
        if ga.domain and len(ga.domain) > 0:
            account_name = ga.name + "@" + ga.domain
        else:
            account_name = ga.name + "@gmail.com"
        account_text = ActionLink(text=account_name)
        account_text.connect("activated", on_visit_mail_account, account_name)
        header.append(account_text, hippo.PACK_EXPAND)        
        vbox = CanvasVBox(border_color=0x0000000ff, classes="slideout-main")
        self.get_root().append(vbox)
        try:
            folderCounts = ga.getFolderCounts()
            folderCounts["unread"] = ga.getUnreadMsgCount()
        except urllib2.URLError:
            error = hippo.CanvasText(text=google_stock.FAILED_TO_CONNECT_STRING)
            vbox.append(error)    
            return

        for label, number in folderCounts.iteritems():
            box = PrelightingCanvasBox()
            box.connect('button-release-event', self.on_button_release_event, label)
            vbox.append(box)
            hbox = CanvasHBox(spacing=4, padding=4)
            text= ActionLink(text=label, xalign=hippo.ALIGNMENT_START)
            hbox.append(text)
            text= ActionLink(text="(%s)" % number, xalign=hippo.ALIGNMENT_START)
            hbox.append(text, flags=hippo.PACK_END)
            box.append(hbox)
    
    def on_button_release_event (self, hippo_item, event, label_text):
        self.emit('changed', label_text)
        self.emit('close', True)

class EmailSlideout(Slideout):
    def __init__(self, thread):
        super(EmailSlideout, self).__init__()
        header = hippo.CanvasBox(orientation=hippo.ORIENTATION_HORIZONTAL, classes="slideout-header")
        self.get_root().append(header)

        vbox = CanvasVBox(border_color=0x0000000ff, spacing=4, classes="slideout-main")
        self.get_root().append(vbox)
        
        self.id = thread.id        

        subject = gmail_jshtml_str_parse(thread.subject)
        
        subject_box = hippo.CanvasText(classes='header', text=subject)
        header.append(subject_box, hippo.PACK_EXPAND)
        
        for key in ("date", "categories", "snippet"):
            value = getattr(thread, key, None)
            if value:
                if type(value) is list:
                    s = ", ".join(value)
                if type(value) is str:
                    _logger.debug("passing in %s" % value) 
                    s = gmail_jshtml_str_parse(value)

                box = hippo.CanvasText(text=s, xalign=hippo.ALIGNMENT_START)
                vbox.append(box)
        
        #todo: nicify email, strip out junk, and show actual email
        #email_source = thread[len(thread)-1].source
        #we could use a regular expression, but its not so simple.
        #exp = "^\\nReceived:.?\\nMessage-ID:.?\\nDate:.?\\nFrom:.?\\nTo:.?\\nSubject:.?\\n"
        # the following doesn't always work
        #psr =  email.parser.Parser()
        #print psr.parsestr(email_source).get_payload()

class MailStock(Stock, google_stock.GoogleStock):
    """Shows recent emails"""
    def __init__(self, *args, **kwargs):
        _logger.debug("in mail stock init")
        Stock.__init__(self, *args, **kwargs)
        google_stock.GoogleStock.__init__(self, 'gmail', **kwargs)

        self._box = hippo.CanvasBox(orientation=hippo.ORIENTATION_VERTICAL)
        
        self.__slideout = None
        self.__last_slideout_event_time = None       

        self.__google_accounts = {} # gobj -> GoogleAccountInfo  
        self.__last_login_gobj = None
        #self.__current_gobj = None
        #self.__google_account = None
        #self.__folder = 'inbox'
        #self.__logged_in = False  
        
        self.__display_limit = 4

        self.__failed_to_connect_message = hippo.CanvasText(text=google_stock.FAILED_TO_CONNECT_STRING, size_mode=hippo.CANVAS_SIZE_WRAP_WORD)

        self._box.prepend(self._login_button)
        self._box.prepend(self.__failed_to_connect_message)
        self._box.set_child_visible(self.__failed_to_connect_message, False) 
        
        self._add_more_button(self.__on_more_button)
        _logger.debug("done with mail stock init")
        self._post_init()
                                
    def get_content(self, size):
        return self._box
    
    def update_google_data(self, gobj):
        # self.__current_gobj = gobj
        # username = gobj.get_account().GetUsername()
        # password = gobj.get_account().GetPassword()
        self.__update_email_box(gobj)
   
    def remove_google_data(self, gobj):
        if self.__google_accounts.has_key(gobj):
            self._box.remove(self.__google_accounts[gobj].get_google_account_box())
            _logger.debug("will remove key for %s" % gobj.get_account().GetUsername())
            del self.__google_accounts[gobj]
        
        # sometimes we don't even get the self.__google_accounts because the polling task didn't start, so 
        # if all self.googles are removed, we should make sure to have the "Login to Google" button showing 
        if len(self.googles) == 0: 
            self._box.remove_all() # we should have removed all emails individually, but let's do this just in case
            self._login_button.set_property('text', google_stock.LOGIN_TO_GOOGLE_STRING) 
            self._box.prepend(self._login_button)
            self._box.prepend(self.__failed_to_connect_message)
            self._box.set_child_visible(self.__failed_to_connect_message, False) 
            self.__google_accounts = {}

    @log_except(_logger)
    def __update_email_box (self, gobj):       
        _logger.debug("will update mailbox")
        (username, domain) = gobj.get_account().GetUsername().split('@', 1)
        username = str(username)
        domain = str(domain) 
        if domain == "gmail.com":
            domain = None  
        password = gobj.get_account().GetPassword()

        if not self.__google_accounts.has_key(gobj):
            self.__google_accounts[gobj] = GoogleAccountInfo()
            self._box.append(self.__google_accounts[gobj].get_google_account_box())  

        google_account = self.__google_accounts[gobj].get_google_account()
        google_account_box = self.__google_accounts[gobj].get_google_account_box()
        current_folder = self.__google_accounts[gobj].get_current_folder()
        logged_in_flag = self.__google_accounts[gobj].get_logged_in_flag()      
        expand_arrow = self.__google_accounts[gobj].get_expand_arrow()  
        expand_arrow_signal_id = self.__google_accounts[gobj].get_expand_arrow_signal_id()

        try:
            if password == '':
                raise libgmail.GmailLoginFailure, 'No password for a Google account.'

            # creating a new GmailAccount is needed in case self.__last_login_gobj != gobj because
            # otherwise the page content we get says that top.location was the account that we last
            # logged in to, and doesn't contain the information for the GmailAccount we are using;
            # just calling the login() function again doesn't work
            if google_account is None or username != google_account.name or \
               password != google_account.password or domain != google_account.domain or self.__last_login_gobj != gobj:                 
                _logger.debug("username %s domain %s password length %s" % (username, domain, len(password)))
                google_account = libgmail.GmailAccount(username, password, domain = domain)
                google_account.login()
            elif not logged_in_flag:
                google_account.login()                

            labelsDict = google_account.getFolderCounts()
            # this is how we know there was a login problem with GAFYD accounts   
            if not labelsDict:    
                 raise libgmail.GmailLoginFailure, 'Failed to login to Google.'
            labelsDict["unread"] = google_account.getUnreadMsgCount()

            logged_in_flag = True
            self._box.set_child_visible(self.__failed_to_connect_message, False)

            if current_folder == 'inbox':
                threads = google_account.getMessagesByFolder(current_folder)
            elif current_folder == 'unread':
                threads = google_account.getUnreadMessages()
            else:
                _logger.debug("will get messages for label %s for account name %s domain %s" % (current_folder, google_account.name, google_account.domain))
                threads = google_account.getMessagesByLabel(current_folder)

            _logger.debug("done getting threads")
            google_account_box.remove_all()
            account = CanvasHBox(xalign=hippo.ALIGNMENT_START)
 
            account_name = ActionLink(text=gobj.get_account().GetUsername(), size_mode=hippo.CANVAS_SIZE_ELLIPSIZE_END)
            account_name.connect("activated", on_visit_mail_account, gobj.get_account().GetUsername())
            unread_message_count = ActionLink(text=" (%s)" % labelsDict['inbox'], xalign=hippo.ALIGNMENT_START)
            unread_message_count.connect("activated", on_visit_mail_account, gobj.get_account().GetUsername())
            # connecting to this signal once per GoogleAccountInfo did not work in all cases, and connecting multiple
            # times means that the function would be called multiple times, so we need to keep the expand_arrow_signal_id
            # to use it for disconnecting, and then connect again each time
            if expand_arrow_signal_id:
                expand_arrow.disconnect(expand_arrow_signal_id)
                expand_arrow_signal_id = None  
            expand_arrow_signal_id = expand_arrow.connect("activated", self.__on_expand_arrow_clicked, gobj)
            account.append(expand_arrow) 
            account.append(account_name)
            account.append(unread_message_count)
            google_account_box.append(account)
            
            details_box = CanvasVBox()

            box = PrelightingCanvasBox()
            box.connect("button-press-event", self.create_label_slideout, gobj)
            details_box.append(box)

            label_and_arrow = CanvasHBox()
            label = CanvasHBox(xalign=hippo.ALIGNMENT_CENTER)
            label_name = ActionLink(text=current_folder, size_mode=hippo.CANVAS_SIZE_ELLIPSIZE_END, classes="action")
            unread_message_count_for_label = ActionLink(text=" (%s)" % labelsDict[current_folder], classes="action") 
            arrow = Arrow(Arrow.RIGHT, arrow_size = 8, padding = 2)
             
            label.append(label_name)
            label.append(unread_message_count_for_label)
            label_and_arrow.append(label, hippo.PACK_EXPAND)
            label_and_arrow.append(arrow, hippo.PACK_END) 
            box.append(label_and_arrow)
            
            i = 0
            for thread in threads:
                if i >= self.__display_limit: break
                
                subject = gmail_jshtml_str_parse(thread.subject, True)
                
                box = PrelightingCanvasBox()
                box.connect("button-press-event", self.create_email_slideout, thread)
                details_box.append(box)
                email = hippo.CanvasText(markup=subject, xalign=hippo.ALIGNMENT_START,
                                         size_mode=hippo.CANVAS_SIZE_ELLIPSIZE_END)
                box.append(email)
                i += 1

            google_account_box.append(details_box)    
            if expand_arrow.get_property('image-name') == 'bigboard-down-arrow-enabled':
                google_account_box.set_child_visible(details_box, False)         

            print "updated mailbox"
            
        except libgmail.GmailLoginFailure:
            self._box.set_child_visible(self.__failed_to_connect_message, False) 
            google_account_box.remove_all()

            account = ActionLink(text=gobj.get_account().GetUsername(), xalign=hippo.ALIGNMENT_START, size_mode=hippo.CANVAS_SIZE_ELLIPSIZE_END)
            account.connect("activated", on_visit_mail_account, gobj.get_account().GetUsername())
            google_account_box.append(account)
            
            logged_in_flag = False
            #if self._login_button.get_property("text") == google_stock.CHECKING_LOGIN_STRING:
            error = hippo.CanvasText(text=google_stock.FAILED_TO_LOGIN_STRING, size_mode=hippo.CANVAS_SIZE_WRAP_WORD)
            google_account_box.append(error)
        except urllib2.URLError:
            if not logged_in_flag:
                google_account_box.remove_all()

                account = ActionLink(text=gobj.get_account().GetUsername(), xalign=hippo.ALIGNMENT_START, size_mode=hippo.CANVAS_SIZE_ELLIPSIZE_END)
                account.connect("activated", on_visit_mail_account, gobj.get_account().GetUsername())
                google_account_box.append(account) 
 
            self._box.set_child_visible(self.__failed_to_connect_message, True) 

        self.__google_accounts[gobj].update({'google_account' : google_account, 'google_account_box' : google_account_box, 'current_folder' : current_folder, 'logged_in_flag' : logged_in_flag, 'expand_arrow_signal_id' : expand_arrow_signal_id})                

        # set self.__last_login_gobj out here even if the login was unsuccessful, because that still has a way in
        # which it can affect things 
        self.__last_login_gobj = gobj

        all_accounts_logged_in = True 
        at_least_one_account_logged_in = False             
        for google_account_info in self.__google_accounts.values():
            _logger.debug("checking google accounts info %s" % google_account_info)
            if not at_least_one_account_logged_in and google_account_info.get_logged_in_flag():
                at_least_one_account_logged_in = True
            if not google_account_info.get_logged_in_flag():
                all_accounts_logged_in = False
                if at_least_one_account_logged_in:
                    break
 
        if all_accounts_logged_in:           
            _logger.debug("will remove login button")
            self._box.set_child_visible(self._login_button, False)
        elif at_least_one_account_logged_in:
            self._login_button.set_property('text', google_stock.EDIT_GOOGLE_ACCOUNTS_STRING) 
            self._box.set_child_visible(self._login_button, True)
        else:
            self._login_button.set_property('text', google_stock.LOGIN_TO_GOOGLE_STRING) 
            self._box.set_child_visible(self._login_button, True)

    def show_slideout(self, widget):
        def on_slideout_close(s, action_taken):
            self.__last_slideout_event_time = gtk.get_current_event_time() 
            if action_taken:
                self._panel.action_taken()
            s.destroy()
            self._set_active(False)
        self._set_active(True)
        self.__slideout.connect('close', on_slideout_close)
        y = widget.get_context().translate_to_screen(widget)[1]
        if not self.__slideout.slideout_from(204, y):
            self.__slideout.destroy()
            self.__slideout = None
            self._set_active(False)
            return
    
    def create_label_slideout(self, widget, hippo_event, gobj):   
        if type(self.__slideout) is LabelSlideout and \
           self.__last_slideout_event_time == gtk.get_current_event_time():
            self.__slideout = None
            return 
        self.__slideout = LabelSlideout(self.__google_accounts[gobj].get_google_account())
        self.__slideout.connect('changed', self.on_label_changed, gobj)
        self.show_slideout(widget)
    
    def create_email_slideout(self, widget, hippo_event, data):
        if type(self.__slideout) is EmailSlideout and self.__slideout.id == data.id and \
           self.__last_slideout_event_time == gtk.get_current_event_time():
            self.__slideout = None
            return 
        self.__slideout = EmailSlideout(data)
        self.show_slideout(widget)
    
    def on_label_changed(self, slideout, label, gobj):
        _logger.debug("will get new folder for %s" % gobj.get_account().GetUsername())
        self.__google_accounts[gobj].update({'current_folder' : label})
        self.__update_email_box(gobj)
    
    def __on_expand_arrow_clicked(self, widget, gobj):
        _logger.debug("arrow clicked image_name: %s" % self.__google_accounts[gobj].get_expand_arrow().get_property('image-name'))   
        google_account_box = self.__google_accounts[gobj].get_google_account_box()

        if self.__google_accounts[gobj].get_logged_in_flag():
            if self.__google_accounts[gobj].get_expand_arrow().get_property('image-name') == 'bigboard-down-arrow-enabled':
                self.__google_accounts[gobj].get_expand_arrow().set_property('image-name', 'bigboard-up-arrow-enabled')
                google_account_box.set_child_visible(google_account_box.get_children()[-1], True) 
            elif self.__google_accounts[gobj].get_expand_arrow().get_property('image-name') == 'bigboard-up-arrow-enabled':
                self.__google_accounts[gobj].get_expand_arrow().set_property('image-name', 'bigboard-down-arrow-enabled')
                google_account_box.set_child_visible(google_account_box.get_children()[-1], False) 

    # TODO: move generic code to google_stock.py
    def __on_more_button(self):
        done_with_sleep_state = 0
        for google_account in self.googles:
            if done_with_sleep_state == 1:
                # in case the browser is just starting, we should wait a bit, otherwise
                # Firefox produces this for the second link:  
                # "Firefox is already running, but is not responding. To open a new window, 
                #  you must first close the existing Firefox process, or restart your system."
                time.sleep(2)
                done_with_sleep_state = 2  
            libbig.show_url(create_account_url(google_account.get_account().GetUsername()))
            if done_with_sleep_state == 0:
                done_with_sleep_state = 1

