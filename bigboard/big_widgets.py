import logging
import urlparse

import cairo
import pango
import gtk
import gobject
import gconf

import hippo

from libgimmie import DockWindow
from libbig.imagecache import URLImageCache
from bigboard.libbig.logutil import log_except
import libbig, stock, globals, bigboard
from bigboard.libbig.signalobject import SignalObject
from bigboard.libbig.singletonmixin import Singleton
from bigboard.theme_manager import ThemeManager
from table_layout import TableLayout

_logger = logging.getLogger("bigboard.BigWidgets")

class CanvasVBox(hippo.CanvasBox):
    def __init__(self, **kwargs):
        kwargs['orientation'] = hippo.ORIENTATION_VERTICAL
        hippo.CanvasBox.__init__(self, **kwargs)

class CanvasHBox(hippo.CanvasBox):
    def __init__(self, **kwargs):
        kwargs['orientation'] = hippo.ORIENTATION_HORIZONTAL
        hippo.CanvasBox.__init__(self, **kwargs)

class CanvasSpinner(hippo.CanvasWidget):
    def __init__(self):
        super(CanvasSpinner, self).__init__()
        self.spinner = gtk.SpinButton()
        self.set_property('widget', self.spinner)

class BigWindow(hippo.CanvasWindow):
    __gsignals__ = {
        'destroy' : 'override',
        }
    
    def __init__(self, stylesheet=None, themed=True, **kwargs):
        super(BigWindow, self).__init__(**kwargs)

        self.__stylesheet = stylesheet

        theme_mgr = ThemeManager.getInstance()

        colormap = None
        if theme_mgr.composited:
            colormap = self.get_screen().get_rgba_colormap()

        if colormap == None:
            colormap = self.get_screen().get_rgb_colormap()
            
        self.set_colormap(colormap)

        if themed:
            self.__connection = theme_mgr.connect('theme-changed', self.__sync_theme)
            self.__sync_theme()
        else:
            self.__connection = None

            if self.__stylesheet:
                theme = theme_mgr.make_stock_theme(self.__stylesheet, default=True)
            else:
                theme = theme_mgr.get_default_theme()

            self.set_theme(theme)

    def __sync_theme(self, *args):
        theme_mgr = ThemeManager.getInstance()

        if self.__stylesheet:
            theme = theme_mgr.make_stock_theme(self.__stylesheet)
        else:
            theme = theme_mgr.get_theme()

        self.set_theme(theme)

    def do_destroy(self):
        if self.__connection:
            ThemeManager.getInstance().disconnect(self.__connection)
            self.__connection = None
        hippo.CanvasWindow.destroy(self)

class CanvasCheckbox(hippo.CanvasWidget):
    def __init__(self, label):
        super(CanvasCheckbox, self).__init__()
        self.checkbox = gtk.CheckButton(label)
        self.set_property('widget', self.checkbox)
        
class CanvasCombo(hippo.CanvasWidget):
    def __init__(self, model):
        super(CanvasCombo, self).__init__()
        self.combo = gtk.ComboBox(model)
        self.set_property('widget', self.combo)

class CanvasTable(hippo.CanvasBox):
    def __init__(self, column_spacing=0, row_spacing=0, **kwargs):
        hippo.CanvasBox.__init__(self, **kwargs)

        self.__layout = TableLayout(column_spacing=column_spacing, row_spacing=row_spacing)
        self.set_layout(self.__layout)

    def add(self, child, left=None, right=None, top=None, bottom=None, flags=0):
        self.__layout.add(child, left, right, top, bottom, flags)

    def set_column_expand(self, column, expand):
        self.__layout.set_column_expand(column, expand)

    def set_row_expand(self, row, expand):
        self.__layout.set_row_expand(row, expand)        

class Header(hippo.CanvasBox):
    def __init__(self, topborder=True):
        super(Header, self).__init__(orientation=hippo.ORIENTATION_HORIZONTAL,
                                     background_color=0xFF0000FF)
        self.__topborder = topborder

    def do_paint_below_children(self, cr, dmgbox):
        area = self.get_background_area()
        if self.__topborder:
            name = 'header'
        else:
            name = 'header-no-top-border'
        self.get_style().paint(cr, name, area.x, area.y, area.width, area.height)
        
gobject.type_register(Header)

class Arrow(hippo.CanvasBox):
    __gtype_name__ = 'BigboardArrow'

    UP    = 0
    DOWN  = 1
    LEFT  = 2
    RIGHT = 3

    MIN_SIZE = 12

    def __init__(self, arrow_type, arrow_size = MIN_SIZE, **kwargs):
        gobject.GObject.__init__(self, **kwargs)
        self.__arrow_type = arrow_type
        self.__arrow_size = arrow_size     

        self.set_clickable(True)

    def do_get_content_width_request(self):
        return (self.__arrow_size, self.__arrow_size)

    def do_get_content_height_request(self, for_width):
        return (self.__arrow_size, self.__arrow_size)

    def do_paint_below_children(self, cr, dmgbox):
        [w, h] = self.get_allocation()

        color = self.get_style().get_color('color', True)
        hippo.cairo_set_source_rgba32(cr, color)

        x1 = self.props.padding_left
        y1 = self.props.padding_top
        x2 = w - 1 - self.props.padding_right
        y2 = h - 1 - self.props.padding_bottom

        if self.__arrow_type == self.RIGHT:
            cr.move_to(x1, y1)
            cr.line_to(x1, y2)
            cr.line_to(x2, (y1 + y2) / 2)
        elif self.__arrow_type == self.LEFT:
            cr.move_to(x2, y1)
            cr.line_to(x2, y2)
            cr.line_to(x1, (y1 + y2) / 2)
        elif self.__arrow_type == self.UP:
            cr.move_to(x1, y2)
            cr.line_to(x2, y2)
            cr.line_to((x1 + x2) / 2, y1)
        elif self.__arrow_type == self.DOWN:
            cr.move_to(x1, y1)
            cr.line_to(x2, y1)
            cr.line_to((x1 + x2) / 2, y2)

        cr.close_path()
        cr.fill()

    def get_arrow_type(self):
        return self.__arrow_type

class ActionLink(hippo.CanvasLink):
    def __init__(self, underline=pango.UNDERLINE_NONE, **kwargs):
        hippo.CanvasLink.__init__(self, **kwargs)
        self.set_underline(underline)   

    def set_underline(self, underline):
        if self.get_property('text') is None:
            return
        if underline == pango.UNDERLINE_LOW:
            self.set_property("padding-bottom", 2)
        # TODO: need to change the end index of the underline if the text is changed  
        attrs = self.get_property("attributes") and self.get_property("attributes") or pango.AttrList()
        attrs.insert(pango.AttrUnderline(underline, end_index=len(self.get_property('text'))))
        if len(attrs.get_iterator().get_attrs()) == 1: 
            self.set_property("attributes", attrs)
            
class Button(hippo.CanvasButton):
    def __init__(self, label_xpadding=0, label_ypadding=0, label=''):
        super(Button, self).__init__()
        self.__label_xpadding = label_xpadding
        self.__label_ypadding = label_ypadding
        button = self.get_property('widget')
        button.set_property('border-width', 0)
        button.unset_flags(gtk.CAN_DEFAULT)

        # this causes the GtkButton inside the CanvasButton
        # to create a label widget
        self.set_property('text', label)        

        # Avoid some padding
        button.set_name('bigboard-nopad-button')
        child = button.get_child()
        child.set_alignment(0.5, 0)
        child.set_padding(self.__label_xpadding, self.__label_ypadding)

    def get_button(self):
        return self.get_property('widget')
  
    def get_label(self):
	return self.get_button().child

    def set_label_text(self, label=''):
        self.set_property('text', label)
        child = self.get_property('widget').get_child()
        child.set_padding(self.__label_xpadding, self.__label_ypadding) 

class CanvasURLImageMixin:
    """A wrapper for CanvasImage which has a set_url method to retrieve
       images from a URL."""
    def __init__(self, url=None):
        self.__is_button = False        
        if url:
            self.set_url(url)
        
    def set_url(self, url):
        if url:
            #print "fetching %s" % url
            image_cache = URLImageCache()
            image_cache.get(url, self.__handle_image_load, self.__handle_image_error)

    def __handle_image_load(self, url, image):
        #print "got %s: %s" % (url, str(image))
        if self.__is_button:
            self.set_property("normal-image", image)
            self.set_property("prelight-image", image)
        else:
            self.set_property("image", image)
        
    def __handle_image_error(self, url, exc):
        # note exception is automatically added to log
        logging.error("failed to load image for '%s': %s", url, exc)  #FIXME queue retry

    def _set_is_button(self, is_button):
        self.__is_button = is_button
    
class CanvasMugshotURLImageMixin:
    """A canvas image that takes a Mugshot-relative image URL."""
    def __init__(self, url=None):
        self.__rel_url = None
        if url:
            self.set_url(url)
        
    def set_url(self, url):
        if url:
            self.__rel_url = url
            self.__sync()
        
    def __sync(self):
        baseurl = globals.get_baseurl()
        if not (baseurl is None or self.__rel_url is None):
            CanvasURLImageMixin.set_url(self, urlparse.urljoin(baseurl, self.__rel_url))

class CanvasURLImage(hippo.CanvasImage, CanvasURLImageMixin):
    """A wrapper for CanvasImage which has a set_url method to retrieve
       images from a URL."""
    def __init__(self, url=None, **kwargs):
        for k in ['xalign', 'yalign']:
            if k not in kwargs:
                kwargs[k] = hippo.ALIGNMENT_START
        hippo.CanvasImage.__init__(self, **kwargs)
        CanvasURLImageMixin.__init__(self, url)
        
class CanvasMugshotURLImage(CanvasMugshotURLImageMixin, CanvasURLImage):
    """A canvas image that takes a Mugshot-relative image URL."""
    def __init__(self, url=None, **kwargs):
        CanvasURLImage.__init__(self, **kwargs)
        CanvasMugshotURLImageMixin.__init__(self, url)

class CanvasURLImageButton(hippo.CanvasImageButton, CanvasURLImageMixin):
    """A wrapper for CanvasImageButton which has a set_url method to retrieve
       images from a URL."""
    def __init__(self, url=None, **kwargs):
        for k in ['xalign', 'yalign']:
            if k not in kwargs:
                kwargs[k] = hippo.ALIGNMENT_START        
        hippo.CanvasImageButton.__init__(self, **kwargs)
        CanvasURLImageMixin.__init__(self, url)
        self._set_is_button(True)

class Separator(hippo.CanvasBox):
    def __init__(self):
        hippo.CanvasBox.__init__(self, border_top=1, border_color=0x999999FF, padding_left=6, padding_right=6)

class PrelightingCanvasBox(hippo.CanvasBox):
    """A box with a background that changes color on mouse hover."""
    def __init__(self, **kwargs):
        self.__hovered = False
        self.__force_prelight = False
        self._prelighted = False
        hippo.CanvasBox.__init__(self, **kwargs)
        self.connect('motion-notify-event', lambda self, event: self.__handle_motion(event))
        
    def __handle_motion(self, event):
        if event.detail == hippo.MOTION_DETAIL_ENTER:
            self.__hovered = True
        elif event.detail == hippo.MOTION_DETAIL_LEAVE:
            self.__hovered = False

        self.sync_prelight()

    def set_force_prelight(self, force):
        self.__force_prelight = force
        self.sync_prelight()
        
    # protected
    def sync_prelight(self): 
        if self.__force_prelight or (self.__hovered and self.do_prelight()):
            self.set_property('classes', 'prelighted-box')
            self._prelighted = True
        else:
            self.set_property('classes', '')
            self._prelighted = False
            
    # protected
    def do_prelight(self):
        return True
    
class PhotoContentItem(PrelightingCanvasBox):
    """A specialized container that has a photo and some
    corresponding content.  Handles size changes via 
    set_size."""
    def __init__(self, **kwargs):
        if 'spacing' not in kwargs:
            kwargs['spacing'] = 4
        self.__photo = None
        self.__photo_native_width = None
        self.__photo_native_height = None
        self.__child = None
        self.__cb = None            
        PrelightingCanvasBox.__init__(self,
                                      orientation=hippo.ORIENTATION_HORIZONTAL,
                                      **kwargs)

        
    def set_photo(self, photo):
        assert(self.__photo is None)
        self.__photo = photo
        self.__photo_native_width = photo.get_property("scale-width")
        self.__photo_native_height = photo.get_property("scale-height")
        self.append(self.__photo)       
        
    def set_child(self, child):
        assert(self.__child is None)
        self.__child = child
        self.append(self.__child, hippo.PACK_EXPAND)         
        
    def set_size(self, size):
        assert(not None in (self.__photo, self.__child, self.__photo_native_width, self.__photo_native_height))
        if size == stock.Stock.SIZE_BULL:
            self.set_child_visible(self.__child, True)
            if self.__photo:
                self.__photo.set_property('xalign', hippo.ALIGNMENT_START)
                self.__photo.set_property('yalign', hippo.ALIGNMENT_START)
                self.__photo.set_property("scale-width", self.__photo_native_width)
                self.__photo.set_property("scale-height", self.__photo_native_height)   
        else:
            self.set_child_visible(self.__child, False)
            if self.__photo:
                self.__photo.set_property('xalign', hippo.ALIGNMENT_CENTER)
                self.__photo.set_property('yalign', hippo.ALIGNMENT_CENTER)        
                self.__photo.set_property("scale-width", 30)
                self.__photo.set_property("scale-height", 30)            

class IconLink(PrelightingCanvasBox):
    def __init__(self, text="", prelight=True, img_scale_width=20, img_scale_height=20, spacing=4, underline=pango.UNDERLINE_NONE, 
                 **kwargs):
        PrelightingCanvasBox.__init__(self,
                                      orientation=hippo.ORIENTATION_HORIZONTAL,
                                      spacing=spacing, **kwargs)
        self.img = hippo.CanvasImage(scale_width=img_scale_width, scale_height=img_scale_height, xalign=hippo.ALIGNMENT_CENTER, yalign=hippo.ALIGNMENT_CENTER)
        self.append(self.img)
        link_kwargs = {'text': text,
                       'underline': underline,
                       'size_mode': hippo.CANVAS_SIZE_ELLIPSIZE_END}
        self.link = ActionLink(**link_kwargs)
        self.append(self.link)
        self.__prelight = prelight

    # override
    def do_prelight(self):
        return self.__prelight

class RootWindowWatcher(gtk.Invisible):
    """Class to track properties of the root window.

    The tracking is a distinctly hacky; what we do is set the user data of the root window
    to point to an GtkInvisible and catch the property-notify events there, since we can't use
    gdk_window_add_filter() from Python. If someone else uses the same trick in the
    same process, we'll break.

    """
    
    __gsignals__ = {
        'realize' : 'override',
        'unrealize' : 'override',
        'workarea-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        }

    @staticmethod
    def get_for_screen(screen):
        watcher = screen.get_data("RootWindowWatcher")
        if watcher == None:
            watcher = RootWindowWatcher(screen)
            screen.set_data("RootWindowWatcher", watcher)

        return watcher

    def __init__(self, screen):
        """Don't call this, call get_for_screen() instead. (Might be possible to make
           a singleton with __new__, but the GObject interaction could be tricky.)"""
        
        super(RootWindowWatcher, self).__init__()
        self.set_screen(screen)

    def do_realize(self):
        super(RootWindowWatcher, self).do_realize(self)
        
        screen = self.get_screen()
        rootw = screen.get_root_window()
        self.__old_events = rootw.get_events()

        rootw.set_events(self.__old_events | gtk.gdk.PROPERTY_CHANGE_MASK)
        rootw.set_user_data(self)

        self.__compute_workarea()

    def do_unrealize(self):
        rootw.set_events(self.__old_events)
        rootw.set_user_data(None)
        
        super(RootWindowWatcher, self).do_unrealize(self)

    def do_property_notify_event(self, event):
        if event.atom == "_NET_WORKAREA":
            old_workarea = self.__workarea
            self.__compute_workarea()
            if (self.__workarea != old_workarea):
                self.emit("workarea-changed")

    def __compute_workarea(self):
        screen = self.get_screen()
        rootw = screen.get_root_window()
        prop = rootw.property_get("_NET_WORKAREA")
        logging.debug("got _NET_WORKAREA: %s" % (prop,))
        (_, _, workarea) = prop
        self.__workarea = (workarea[0], workarea[1], workarea[2], workarea[3])

    def get_workarea(self):
        return self.__workarea
                
class Sidebar(DockWindow):
    __gsignals__ = {
        'screen-changed' : 'override',
        'show' : 'override',
        }

    def __init__(self, strut_key):
        DockWindow.__init__(self, gtk.gdk.GRAVITY_WEST)
        self.__watcher = None
        self.__strut_key = strut_key

    def do_show(self):
        self.__update_watcher()
        super(Sidebar, self).do_show(self)

    def do_screen_changed(self, old_screen):
        super(Sidebar, self).do_screen_changed(old_screen)

        self.__update_watcher()

    def __update_watcher(self):
        if self.__watcher != None:
            self.__watcher.disconnect(self.__watcher_handler_id)
        self.__watcher = RootWindowWatcher.get_for_screen(self.get_screen())
        self.__watcher_handler_id = self.__watcher.connect('workarea-changed', self.__on_workarea_changed)
        self.__on_workarea_changed(self.__watcher)

    def __on_workarea_changed(self, watcher):
        (x,y,width,height) = watcher.get_workarea()
        self.set_size_request(-1, height)
        if self.edge_gravity == gtk.gdk.GRAVITY_WEST: 
            self.move(0, y)
        else:
            self.move(width, y)
        
    def do_set_wm_strut(self):
        kwargs = {}
        if not gconf.client_get_default().get_bool(self.__strut_key):
            kwargs['remove'] = True
        super(Sidebar, self).do_set_wm_strut(**kwargs)
