/*
 * Copyright (C) 2008, Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __EMPATHY_H__
#define __EMPATHY_H__

#include <gtk/gtkwidget.h>

G_BEGIN_DECLS

gboolean   bb_empathy_is_configured      (void);
GtkWidget *bb_empathy_configure_accounts (void);
void       bb_empathy_set_online         (gboolean    online);
void       bb_empathy_open_chat_with     (const char *buddy_id,
                                          const char *protocol_name);

G_END_DECLS

#endif /* __EMPATHY_H__ */

