/*
 * Copyright (C) 2008, Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <string.h>
#include <libmissioncontrol/mission-control.h>
#include <libmissioncontrol/mc-account.h>
#include <libempathy/empathy-dispatcher.h>
#include <libempathy/empathy-utils.h>

#include "bb-empathy.h"

gboolean
bb_empathy_is_configured(void)
{
    GList *accounts;

    accounts = mc_accounts_list_by_enabled(TRUE);
    if (accounts) {
        mc_accounts_list_free(accounts);
        return TRUE;
    }

    return FALSE;
}

void
bb_empathy_set_online(gboolean online)
{
    MissionControl *mc;

    mc = empathy_mission_control_new();
    mission_control_set_presence(mc, online ? MC_PRESENCE_AVAILABLE : MC_PRESENCE_OFFLINE,
                                 NULL, NULL, NULL);
    g_object_unref(mc);
}

GtkWidget *
bb_empathy_configure_accounts(void)
{
    /* Does not work with latest empathy. See #535129
    return empathy_accounts_dialog_show(NULL); */

    return NULL;
}

void
bb_empathy_open_chat_with(const char *buddy_id,
                          const char *protocol_name)
{
    McAccount *target_account = NULL;
    GList *accounts, *l;

    accounts = mc_accounts_list ();
    for (l = accounts; l != NULL; l = l->next) {
        McAccount *account = MC_ACCOUNT(l->data);
        McProfile *profile;
        McProtocol *protocol;

        profile = mc_account_get_profile(account);
        protocol = mc_profile_get_protocol(profile);

        if (strcmp(mc_protocol_get_name(protocol), protocol_name) == 0) {
            target_account = account;
        }

        g_object_unref(profile);
        g_object_unref(protocol);
    }

    if (target_account != NULL) {
        empathy_dispatcher_chat_with_contact_id(target_account, buddy_id);
    }

    mc_accounts_list_free (accounts);
}
