/* -*- Mode: C; c-basic-offset: 4 -*- */
#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <pygobject.h>

extern PyMethodDef py_empathy_functions[];

DL_EXPORT(void)
init_empathy(void)
{
    PyObject *m, *d;

    /* perform any initialisation required by the library here */
    init_pygobject();
	
    m = Py_InitModule("_empathy", py_empathy_functions);
    d = PyModule_GetDict(m);
    
    if (PyErr_Occurred())
        Py_FatalError("could not initialise module _empathy");
}
